<?php
/**
 * Tax Candidates Fields
 *
 * @package makosi
 */

$prefix = 'tax-candidates-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '2',
		'title'    => 'Tax Candidates 1',
		'fields'   => array(
			array(
				'key'   => $prefix . 'subtitle',
				'label' => 'Sub Title',
				'name'  => $prefix . 'subtitle',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'subtitle-2',
				'label' => 'Sub Title 2',
				'name'  => $prefix . 'subtitle-2',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'content-large',
				'label' => 'Content Large',
				'name'  => $prefix . 'content-large',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-2',
				'label' => 'Content',
				'name'  => $prefix . 'content-2',
				'type'  => 'wysiwyg',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax-candidates.php',
				),
			),
		),
	)
);

acf_add_local_field_group(
	array(
		'key'      => $prefix . '3',
		'title'    => 'Tax Candidates 2',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title 2',
				'name'  => $prefix . 'title-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'icons',
				'label' => 'Icon Columns',
				'name'  => $prefix . 'icons',
				'type'  => 'repeater',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax-candidates.php',
				),
			),
		),
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-image',
		'label'  => 'Image',
		'name'   => $prefix . 'repeater-image',
		'parent' => $prefix . 'icons',
		'type'   => 'image',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-title',
		'label'  => 'Title',
		'name'   => $prefix . 'repeater-title',
		'parent' => $prefix . 'icons',
		'type'   => 'wysiwyg',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-content',
		'label'  => 'Content',
		'name'   => $prefix . 'repeater-content',
		'parent' => $prefix . 'icons',
		'type'   => 'wysiwyg',
	)
);

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Tax Candidates 3',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title 3',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content 3',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax-candidates.php',
				),
			),
		),
	)
);

// acf_add_local_field_group(
// 	array(
// 		'key'      => $prefix . '4',
// 		'title'    => 'Tax Candidates 4',
// 		'fields'   => array(
// 			array(
// 				'key'   => $prefix . 'title-3',
// 				'label' => 'Title 3',
// 				'name'  => $prefix . 'title-3',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'button',
// 				'label' => 'Button',
// 				'name'  => $prefix . 'button',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'url',
// 				'label' => 'URL',
// 				'name'  => $prefix . 'url',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'button-2',
// 				'label' => 'Button 2',
// 				'name'  => $prefix . 'button-2',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'url-2',
// 				'label' => 'URL 2',
// 				'name'  => $prefix . 'url-2',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'link',
// 				'label' => 'Link',
// 				'name'  => $prefix . 'link',
// 				'type'  => 'text',
// 			),
// 		),
// 		'location' => array(
// 			array(
// 				array(
// 					'param'    => 'page_template',
// 					'operator' => '==',
// 					'value'    => 'template-tax-candidates.php',
// 				),
// 			),
// 		),
// 	)
// );
