<?php
/**
 * Case Study Fields
 *
 * @package makosi
 */

$prefix = 'case-study-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Case Study',
		'fields'   => array(
			array(
				'key'   => $prefix . 'testimonial',
				'label' => 'Testimonial',
				'name'  => $prefix . 'testimonial',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button',
				'label' => 'Button',
				'name'  => $prefix . 'button',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-case-study.php',
				),
			),
		),
	)
);
