<?php
/**
 * Home Fields
 *
 * @package makosi
 */

$prefix = 'home-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Home 1',
		'fields'   => array(
			array(
				'key'   => $prefix . 'logo',
				'label' => 'Logo',
				'name'  => $prefix . 'logo',
				'type'  => 'image',
			),
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button',
				'label' => 'Button',
				'name'  => $prefix . 'button',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'link',
				'label' => 'Link',
				'name'  => $prefix . 'link',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'url',
				'label' => 'URL',
				'name'  => $prefix . 'url',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-home.php',
				),
			),
		),
	)
);

acf_add_local_field_group(
	array(
		'key'      => $prefix . '2',
		'title'    => 'Home 2',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title 2',
				'name'  => $prefix . 'title-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-2',
				'label' => 'Content 2',
				'name'  => $prefix . 'content-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button-1',
				'label' => 'Button 1',
				'name'  => $prefix . 'button-1',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'button-2',
				'label' => 'Button 2',
				'name'  => $prefix . 'button-2',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-home.php',
				),
			),
		),
	)
);
