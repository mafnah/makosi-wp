<?php
/**
 * Single Post Fields
 *
 * @package makosi
 */

$prefix = 'single-post-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Post',
		'fields'   => array(
			array(
				'key'           => $prefix . 'related',
				'label'         => 'Related Posts',
				'name'          => $prefix . 'related',
				'post_type'     => 'post',
				'filters'       => array( 'search' ),
				'return_format' => 'id',
				'type'          => 'relationship',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'post',
				),
			),
		),
	)
);
