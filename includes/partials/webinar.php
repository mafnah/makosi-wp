<?php
/**
 * Webinar Fields
 *
 * @package makosi
 */

$prefix = 'webinar-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Webinar 1',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title 2',
				'name'  => $prefix . 'title-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-2',
				'label' => 'Content 2',
				'name'  => $prefix . 'content-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-first',
				'label' => 'Content First',
				'name'  => $prefix . 'content-first',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-large',
				'label' => 'Content Large',
				'name'  => $prefix . 'content-large',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-last',
				'label' => 'Content Last',
				'name'  => $prefix . 'content-last',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'feed',
				'label' => 'Feed',
				'name'  => $prefix . 'feed',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-webinar.php',
				),
			),
		),
	)
);

// acf_add_local_field_group(
// 	array(
// 		'key'      => $prefix . '2',
// 		'title'    => 'Webinar 2',
// 		'fields'   => array(
// 			array(
// 				'key'   => $prefix . 'title-3',
// 				'label' => 'Title 3',
// 				'name'  => $prefix . 'title-3',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'content-3',
// 				'label' => 'Content 3',
// 				'name'  => $prefix . 'content-3',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'button',
// 				'label' => 'Button',
// 				'name'  => $prefix . 'button',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'url',
// 				'label' => 'URL',
// 				'name'  => $prefix . 'url',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'content-small',
// 				'label' => 'Content Small',
// 				'name'  => $prefix . 'content-small',
// 				'type'  => 'wysiwyg',
// 			),
// 		),
// 		'location' => array(
// 			array(
// 				array(
// 					'param'    => 'page_template',
// 					'operator' => '==',
// 					'value'    => 'template-webinar.php',
// 				),
// 			),
// 		),
// 	)
// );
