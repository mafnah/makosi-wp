<?php
/**
 * About Fields
 *
 * @package makosi
 */

$prefix = 'about-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'About',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'title-content',
				'label' => 'Title 2',
				'name'  => $prefix . 'title-content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title 3',
				'name'  => $prefix . 'title-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-2',
				'label' => 'Content 2',
				'name'  => $prefix . 'content-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-large',
				'label' => 'Content Large',
				'name'  => $prefix . 'content-large',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button',
				'label' => 'Button',
				'name'  => $prefix . 'button',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'url',
				'label' => 'URL',
				'name'  => $prefix . 'url',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-about.php',
				),
			),
		),
	)
);
