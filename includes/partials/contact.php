<?php
/**
 * Contact Fields
 *
 * @package makosi
 */

$prefix = 'contact-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Contact',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'form',
				'label' => 'Form',
				'name'  => $prefix . 'form',
				'type'  => 'textarea',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-contact.php',
				),
			),
		),
	)
);
