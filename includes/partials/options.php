<?php
/**
 * Options Page
 *
 * @package makosi
 */

$prefix = 'options-';

acf_add_options_page(
	array(
		'page_title' => __( 'Options' ),
		'menu_title' => __( 'Options' ),
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false,
	)
);

acf_add_local_field_group(
	array(
		'key'        => $prefix . '1',
		'title'      => 'Options',
		'fields'     => array(
			array(
				'key'   => $prefix . 'logo',
				'label' => 'Logo',
				'name'  => $prefix . 'logo',
				'type'  => 'image',
			),
			array(
				'key'   => $prefix . 'logo-light',
				'label' => 'Logo Light',
				'name'  => $prefix . 'logo-light',
				'type'  => 'image',
			),
			array(
				'key'   => $prefix . 'nav-link-title',
				'label' => 'Nav Link Title',
				'name'  => $prefix . 'nav-link-title',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'nav-link-url',
				'label' => 'Nav Link URL',
				'name'  => $prefix . 'nav-link-url',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'blog-title',
				'label' => 'Blog Title',
				'name'  => $prefix . 'blog-title',
				'type'  => 'text',
			),
			// array(
			// 	'key'   => $prefix . 'social-title',
			// 	'label' => 'Social Title',
			// 	'name'  => $prefix . 'social-title',
			// 	'type'  => 'text',
			// ),
			array(
				'key'   => $prefix . 'instagram',
				'label' => 'Instagram',
				'name'  => $prefix . 'instagram',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'facebook',
				'label' => 'Facebook',
				'name'  => $prefix . 'facebook',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'twitter',
				'label' => 'Twitter',
				'name'  => $prefix . 'twitter',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'linkedin',
				'label' => 'LinkedIn',
				'name'  => $prefix . 'linkedin',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'sign-up-form',
				'label' => 'Sign Up Form',
				'name'  => $prefix . 'sign-up-form',
				'type'  => 'textarea',
			),
			array(
				'key'   => $prefix . 'scripts-header',
				'label' => 'Scripts Header',
				'name'  => $prefix . 'scripts-header',
				'type'  => 'textarea',
			),
			array(
				'key'   => $prefix . 'scripts-footer',
				'label' => 'Scripts Footer',
				'name'  => $prefix . 'scripts-footer',
				'type'  => 'textarea',
			),
			// array(
			// 	'key'   => $prefix . 'sidebar-news-title',
			// 	'label' => 'Sidebar News Title',
			// 	'name'  => $prefix . 'sidebar-news-title',
			// 	'type'  => 'text',
			// ),
		),
		'location'   => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'theme-general-settings',
				),
			),
		),
		'menu_order' => 0,
	)
);

acf_add_local_field_group(
	array(
		'key'        => $prefix . 'case-study-modal',
		'title'      => 'Case Study Popup',
		'fields'     => array(
			array(
				'key'   => $prefix . 'title-1',
				'label' => 'Title',
				'name'  => $prefix . 'title-1',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'form',
				'label' => 'Form',
				'name'  => $prefix . 'form',
				'type'  => 'textarea',
			),
		),
		'location'   => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'theme-general-settings',
				),
			),
		),
		'menu_order' => 1,
	)
);

acf_add_local_field_group(
	array(
		'key'        => $prefix . 'request-case-study-modal',
		'title'      => 'Request Popup',
		'fields'     => array(
			array(
				'key'   => $prefix . 'title-3',
				'label' => 'Title',
				'name'  => $prefix . 'title-3',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'content-2',
				'label' => 'Content',
				'name'  => $prefix . 'content-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'form-2',
				'label' => 'Form',
				'name'  => $prefix . 'form-2',
				'type'  => 'textarea',
			),
		),
		'location'   => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'theme-general-settings',
				),
			),
		),
		'menu_order' => 1,
	)
);

acf_add_local_field_group(
	array(
		'key'        => $prefix . 'questions-modal',
		'title'      => 'Questions Popup',
		'fields'     => array(
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title',
				'name'  => $prefix . 'title-2',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'questions',
				'label' => 'Questions',
				'name'  => $prefix . 'questions',
				'type'  => 'repeater',
			),
		),
		'location'   => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'theme-general-settings',
				),
			),
		),
		'menu_order' => 2,
	)
);

acf_add_local_field_group(
	array(
		'key'        => $prefix . 'covid-modal',
		'title'      => 'Covid Banner',
		'fields'     => array(
			array(
				'key'   => $prefix . 'covid-show',
				'label' => 'Show Covid Banner',
				'name'  => $prefix . 'covid-show',
				'type'  => 'true_false',
			),
			array(
				'key'   => $prefix . 'covid-title',
				'label' => 'Title',
				'name'  => $prefix . 'covid-title',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'covid-content',
				'label' => 'Content',
				'name'  => $prefix . 'covid-content',
				'type'  => 'wysiwyg',
			),
		),
		'location'   => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'theme-general-settings',
				),
			),
		),
		'menu_order' => 3,
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-title',
		'label'  => 'Title',
		'name'   => $prefix . 'repeater-title',
		'parent' => $prefix . 'questions',
		'type'   => 'text',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-content',
		'label'  => 'Content',
		'name'   => $prefix . 'repeater-content',
		'parent' => $prefix . 'questions',
		'type'   => 'textarea',
	)
);
