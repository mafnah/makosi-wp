<?php
/**
 * Tax Clients Fields
 *
 * @package makosi
 */

$prefix = 'tax-clients-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Tax Clients 1',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'facts',
				'label' => 'Facts',
				'name'  => $prefix . 'facts',
				'type'  => 'repeater',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax-clients.php',
				),
			),
		),
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-title',
		'label'  => 'Title',
		'name'   => $prefix . 'repeater-title',
		'parent' => $prefix . 'facts',
		'type'   => 'wysiwyg',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-content',
		'label'  => 'Content',
		'name'   => $prefix . 'repeater-content',
		'parent' => $prefix . 'facts',
		'type'   => 'wysiwyg',
	)
);

acf_add_local_field_group(
	array(
		'key'      => $prefix . '2',
		'title'    => 'Tax Clients 2',
		'fields'   => array(
			array(
				'key'   => $prefix . 'side-title-left',
				'label' => 'Side Title Left',
				'name'  => $prefix . 'side-title-left',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'side-title-right',
				'label' => 'Side Title Right',
				'name'  => $prefix . 'side-title-right',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title 2',
				'name'  => $prefix . 'title-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'facts-2',
				'label' => 'Facts',
				'name'  => $prefix . 'facts-2',
				'type'  => 'repeater',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax-clients.php',
				),
			),
		),
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-title-2',
		'label'  => 'Title',
		'name'   => $prefix . 'repeater-title-2',
		'parent' => $prefix . 'facts-2',
		'type'   => 'wysiwyg',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-content-2',
		'label'  => 'Content',
		'name'   => $prefix . 'repeater-content-2',
		'parent' => $prefix . 'facts-2',
		'type'   => 'wysiwyg',
	)
);

// acf_add_local_field_group(
// 	array(
// 		'key'      => $prefix . '3',
// 		'title'    => 'Tax Clients 3',
// 		'fields'   => array(
// 			array(
// 				'key'   => $prefix . 'testimonial',
// 				'label' => 'Testimonial',
// 				'name'  => $prefix . 'testimonial',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'title-3',
// 				'label' => 'Title 3',
// 				'name'  => $prefix . 'title-3',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'button',
// 				'label' => 'Button',
// 				'name'  => $prefix . 'button',
// 				'type'  => 'text',
// 			),
// 		),
// 		'location' => array(
// 			array(
// 				array(
// 					'param'    => 'page_template',
// 					'operator' => '==',
// 					'value'    => 'template-tax-clients.php',
// 				),
// 			),
// 		),
// 	)
// );

// acf_add_local_field_group(
// 	array(
// 		'key'      => $prefix . '4',
// 		'title'    => 'Tax Clients 4',
// 		'fields'   => array(
// 			array(
// 				'key'   => $prefix . 'title-4',
// 				'label' => 'Title 4',
// 				'name'  => $prefix . 'title-4',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'content-2',
// 				'label' => 'Content 2',
// 				'name'  => $prefix . 'content-2',
// 				'type'  => 'wysiwyg',
// 			),
// 			array(
// 				'key'   => $prefix . 'button-2',
// 				'label' => 'Button 2',
// 				'name'  => $prefix . 'button-2',
// 				'type'  => 'text',
// 			),
// 			array(
// 				'key'   => $prefix . 'url',
// 				'label' => 'URL',
// 				'name'  => $prefix . 'url',
// 				'type'  => 'text',
// 			),
// 		),
// 		'location' => array(
// 			array(
// 				array(
// 					'param'    => 'page_template',
// 					'operator' => '==',
// 					'value'    => 'template-tax-clients.php',
// 				),
// 			),
// 		),
// 	)
// );

acf_add_local_field_group(
	array(
		'key'      => $prefix . '5',
		'title'    => 'Tax Clients 5',
		'fields'   => array(
			array(
				'key'   => $prefix . 'content-3',
				'label' => 'Content 3',
				'name'  => $prefix . 'content-3',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button-3',
				'label' => 'Button 3',
				'name'  => $prefix . 'button-3',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'url-3',
				'label' => 'URL',
				'name'  => $prefix . 'url-3',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax-clients.php',
				),
			),
		),
	)
);
