<?php
/**
 * Tax Fields
 *
 * @package makosi
 */

$prefix = 'tax-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Tax',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button-1',
				'label' => 'Button 1',
				'name'  => $prefix . 'button-1',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'button-2',
				'label' => 'Button 2',
				'name'  => $prefix . 'button-2',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-tax.php',
				),
			),
		),
	)
);
