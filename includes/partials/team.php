<?php
/**
 * Team Fields
 *
 * @package makosi
 */

$prefix = 'team-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Team',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title-1',
				'label' => 'Title',
				'name'  => $prefix . 'title-1',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'members',
				'label' => 'Members',
				'name'  => $prefix . 'members',
				'type'  => 'repeater',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-team.php',
				),
			),
		),
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-image',
		'label'  => 'Image',
		'name'   => $prefix . 'repeater-image',
		'parent' => $prefix . 'members',
		'type'   => 'image',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-title',
		'label'  => 'Title',
		'name'   => $prefix . 'repeater-title',
		'parent' => $prefix . 'members',
		'type'   => 'text',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-position',
		'label'  => 'Position',
		'name'   => $prefix . 'repeater-position',
		'parent' => $prefix . 'members',
		'type'   => 'text',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-content',
		'label'  => 'Content',
		'name'   => $prefix . 'repeater-content',
		'parent' => $prefix . 'members',
		'type'   => 'wysiwyg',
	)
);
