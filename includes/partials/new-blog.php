<?php
/**
 * New Blog Fields
 *
 * @package makosi
 */

$prefix = 'new-blog-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'New Blog',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-new-blog.php',
				),
			),
		),
	)
);
