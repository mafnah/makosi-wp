<?php
/**
 * IT Audit Candidates Fields
 *
 * @package makosi
 */

$prefix = 'it-audit-candidates-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '-1',
		'title'    => 'IT Audit Candidates 1',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'icons',
				'label' => 'Icon Columns',
				'name'  => $prefix . 'icons',
				'type'  => 'repeater',
			),
			array(
				'key'   => $prefix . 'title-2',
				'label' => 'Title 2',
				'name'  => $prefix . 'title-2',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-2',
				'label' => 'Content 2',
				'name'  => $prefix . 'content-2',
				'type'  => 'wysiwyg',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-it-audit-candidates.php',
				),
			),
		),
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-image',
		'label'  => 'Image',
		'name'   => $prefix . 'repeater-image',
		'parent' => $prefix . 'icons',
		'type'   => 'image',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-title',
		'label'  => 'Title',
		'name'   => $prefix . 'repeater-title',
		'parent' => $prefix . 'icons',
		'type'   => 'wysiwyg',
	)
);

acf_add_local_field_group(
	array(
		'key'      => $prefix . '2',
		'title'    => 'IT Audit Candidates 2',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title-3',
				'label' => 'Title 3',
				'name'  => $prefix . 'title-3',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content-3',
				'label' => 'Content 3',
				'name'  => $prefix . 'content-3',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'side-title-left',
				'label' => 'Side Title Left',
				'name'  => $prefix . 'side-title-left',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'side-title-right',
				'label' => 'Side Title Right',
				'name'  => $prefix . 'side-title-right',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-it-audit-candidates.php',
				),
			),
		),
	)
);

acf_add_local_field_group(
	array(
		'key'      => $prefix . '3',
		'title'    => 'IT Audit Candidates 3',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title-4',
				'label' => 'Title 4',
				'name'  => $prefix . 'title-4',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button',
				'label' => 'Button',
				'name'  => $prefix . 'button',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'url',
				'label' => 'URL',
				'name'  => $prefix . 'url',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'button-2',
				'label' => 'Button 2',
				'name'  => $prefix . 'button-2',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'url-2',
				'label' => 'URL 2',
				'name'  => $prefix . 'url-2',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'link',
				'label' => 'Link',
				'name'  => $prefix . 'link',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-it-audit-candidates.php',
				),
			),
		),
	)
);
