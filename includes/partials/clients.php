<?php
/**
 * Clients Fields
 *
 * @package makosi
 */

$prefix = 'clients-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Clients',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'logos',
				'label' => 'Logos',
				'name'  => $prefix . 'logos',
				'type'  => 'repeater',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-clients.php',
				),
			),
		),
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-image',
		'label'  => 'Image',
		'name'   => $prefix . 'repeater-image',
		'parent' => $prefix . 'logos',
		'type'   => 'image',
	)
);

acf_add_local_field(
	array(
		'key'    => $prefix . 'repeater-small',
		'label'  => 'Small',
		'name'   => $prefix . 'repeater-small',
		'parent' => $prefix . 'logos',
		'type'   => 'true_false',
		'ui'     => true,
	)
);
