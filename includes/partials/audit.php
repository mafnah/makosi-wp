<?php
/**
 * Audit Fields
 *
 * @package makosi
 */

$prefix = 'audit-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Audit',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button-1',
				'label' => 'Button 1',
				'name'  => $prefix . 'button-1',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'button-2',
				'label' => 'Button 2',
				'name'  => $prefix . 'button-2',
				'type'  => 'text',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-audit.php',
				),
			),
		),
	)
);
