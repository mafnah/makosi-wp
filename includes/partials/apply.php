<?php
/**
 * Apply Fields
 *
 * @package makosi
 */

$prefix = 'apply-';

acf_add_local_field_group(
	array(
		'key'      => $prefix . '1',
		'title'    => 'Apply',
		'fields'   => array(
			array(
				'key'   => $prefix . 'title',
				'label' => 'Title',
				'name'  => $prefix . 'title',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'content',
				'label' => 'Content',
				'name'  => $prefix . 'content',
				'type'  => 'wysiwyg',
			),
			array(
				'key'   => $prefix . 'button',
				'label' => 'Button',
				'name'  => $prefix . 'button',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'url',
				'label' => 'URL',
				'name'  => $prefix . 'url',
				'type'  => 'text',
			),
			array(
				'key'   => $prefix . 'content-small',
				'label' => 'Content Small',
				'name'  => $prefix . 'content-small',
				'type'  => 'wysiwyg',
			),
		),
		'location' => array(
			array(
				array(
					'param'    => 'page_template',
					'operator' => '==',
					'value'    => 'template-apply.php',
				),
			),
		),
	)
);
