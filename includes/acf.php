<?php
/**
 * Registers Advanced Custom Fields.
 *
 * @package makosi
 */

if ( function_exists( 'acf_add_local_field_group' ) ) {
	require_once 'partials/home.php';
	require_once 'partials/audit.php';
	require_once 'partials/audit-clients.php';
	require_once 'partials/audit-candidates.php';
	require_once 'partials/it-audit.php';
	require_once 'partials/it-audit-clients.php';
	require_once 'partials/it-audit-candidates.php';
	require_once 'partials/tax.php';
	require_once 'partials/tax-clients.php';
	require_once 'partials/tax-candidates.php';
	require_once 'partials/about.php';
	require_once 'partials/team.php';
	require_once 'partials/webinar.php';
	require_once 'partials/clients.php';
	require_once 'partials/contact.php';
	require_once 'partials/new-blog.php';
	require_once 'partials/single-post.php';
	require_once 'partials/apply.php';
	require_once 'partials/case-study.php';

	if ( function_exists( 'acf_add_options_page' ) ) {
		require_once 'partials/options.php';
	}
}
