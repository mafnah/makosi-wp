<?php
/**
 * The about template file
 *
 * Template Name: About
 *
 * @package Makosi
 */

get_header();
?>

<main class="main about-main">
	<?php get_template_part( 'template-parts/about' ); ?>
</main>

<?php
get_footer();
