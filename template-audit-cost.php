<?php
/**
 * The cost of audit template file
 *
 * Template Name: Cost of Audit
 *
 * @package Makosi
 */

get_header();
?>

<main class="main audit-cost-main">
	<div class="mobile-hide">
		<div class="cost-blob">
			<?php the_svg( 'images/cost-blob.svg' ); ?>
		</div>
	</div>

	<?php get_template_part( 'template-parts/audit-cost' ); ?>
</main>

<?php
get_footer();
