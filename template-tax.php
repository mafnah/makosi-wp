<?php
/**
 * The tax template file
 *
 * Template Name: Tax
 *
 * @package Makosi
 */

get_header();
?>

<main class="main tax-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/tax' ); ?>
</main>

<?php
get_footer();
