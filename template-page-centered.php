<?php
/**
 * The centered page template file
 *
 * Template Name: Page Centered
 *
 * @package Makosi
 */

get_header();
?>

<main class="main page-single page-centered">
	<?php get_template_part( 'template-parts/content' ); ?>
</main>

<?php
get_footer();

