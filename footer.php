<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'options-';

?>

<?php get_template_part( 'template-parts/modal' ); ?>

<div class="lds-loader">
	<div class="lds-ripple">
		<div></div>
		<div></div>
	</div>
</div>

<div class="go-next">
	<a href="#next" id="go-next"></a>
</div>

<div class="go-back">
	<a href="#back" id="go-back">go back</a>
</div>

<?php wp_footer(); ?>

<?php if ( is_acf( $prefix . 'scripts-footer', 'option' ) ) : ?>
	<?php echo get_field( $prefix . 'scripts-footer', 'option' ); ?>
<?php endif; ?>

</body>
</html>
