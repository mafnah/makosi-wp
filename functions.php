<?php
/**
 * Makosi functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Makosi
 */

/**
 * Register Advanced Custom Fields
 */
require_once 'includes/acf.php';

if ( ! function_exists( 'makosi_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function makosi_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'main-menu' => esc_html__( 'Primary', 'makosi' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'makosi_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function makosi_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'makosi' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'makosi' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'makosi_widgets_init' );

/**
 * Enqueue scripts and styles. https://cdn.jsdelivr.net/npm/animejs@3.1.0/lib/anime.min.js
 */
function makosi_scripts() {
	wp_enqueue_style( 'makosi-css', get_template_directory_uri() . '/css/style.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'makosi-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'makosi-font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_script( 'makosi-anime', 'https://cdn.jsdelivr.net/npm/animejs@3.1.0/lib/anime.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-mobile-detect', 'https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.4.4/mobile-detect.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-moment', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-moment-timezone', 'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.27/moment-timezone-with-data.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-shave', 'https://cdnjs.cloudflare.com/ajax/libs/shave/2.5.7/shave.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-bluebird', 'https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.4/bluebird.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-fetch', 'https://cdn.jsdelivr.net/npm/fetch-polyfill@0.8.2/fetch.min.js', array(), wp_get_theme()->get( 'Version' ), true );
	wp_enqueue_script( 'makosi-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
}
add_action( 'wp_enqueue_scripts', 'makosi_scripts' );

/**
 * Print the svg content
 *
 * @param string $path path of the svg file.
 * @param bool   $full use full path of the svg file.
 */
function the_svg( $path, $full = false ) {
	$file_path = ( $full ) ? $path : get_bloginfo( 'template_directory' ) . '/' . $path;
	$response  = wp_remote_get( $file_path );

	if ( is_array( $response ) && ! is_wp_error( $response ) ) {
		echo $response['body']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
}

/**
 * Add body classes
 *
 * @param array $classes body classes.
 */
function body_class_before_header( $classes ) {
	if ( is_page_template( 'template-audit-cost.php' ) || is_page_template( 'template-page-centered.php' ) || is_page_template( 'template-new-blog.php' ) || is_page( 'blog' ) || is_page( 'blog/single' ) || is_page( 'team' ) || is_single() || ( is_page() && ! is_page_template() ) || is_404() || is_category() || is_archive() ) {
		$classes[] = 'overflow-auto';
	}

	return $classes;
}
add_filter( 'body_class', 'body_class_before_header' );

/**
 * Add svg support
 *
 * @param array $mimes file types.
 */
function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg';

	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/**
 * Check acf
 *
 * @param string $key acf key.
 * @param string $type option or post id.
 */
function is_acf( $key, $type = false ) {
	if ( function_exists( 'get_field' ) ) {
		$field = get_field( $key, $type );
		$value = ( $field || ! empty( $field ) ) ? true : false;

		return $value;
	}
}

/**
 * Handle acf
 *
 * @param string $key acf key.
 * @param string $type option or post id.
 */
function the_acf( $key, $type = false ) {
	if ( function_exists( 'get_field' ) ) {
		$field = get_field( $key, $type );
		$value = ( $field ) ? $field : '';

		if ( $value ) {
			echo wp_kses_post( $value );
		}
	}
}

/**
 * Handle acf image
 *
 * @param string $key acf key.
 * @param string $size image size.
 * @param string $type option or post id.
 */
function the_acf_image( $key, $size = '', $type = false ) {
	if ( function_exists( 'get_field' ) ) {
		$field = get_field( $key, $type );
		$value = ( $field ) ? $field : '';
		$src   = ( $size ) ? $value['sizes'][ $size ] : $value['url'];

		if ( $value ) {
			if ( 'image/svg' === $value['mime_type'] ) {
				the_svg( $src, true );
			} else {
				echo '<img src="' . esc_url( $src ) . '" alt="' . esc_html( $value['title'] ) . '">';
			}
		}
	}
}

/**
 * Check field
 *
 * @param string $field field value.
 */
function is_acf_field( $field ) {
	$value = ( $field || ! empty( $field ) ) ? true : false;

	return $value;
}

/**
 * Handle field
 *
 * @param string $field field value.
 */
function the_acf_field( $field ) {
	$value = ( $field ) ? $field : '';

	if ( $value ) {
		echo wp_kses_post( $value );
	}
}

/**
 * Handle field image
 *
 * @param string $field field value.
 * @param string $size image size.
 * @param bool   $dimensions add image dimensions.
 */
function the_acf_field_image( $field, $size = '', $dimensions = false ) {
	$value = ( $field ) ? $field : '';
	$src   = ( $size ) ? $value['sizes'][ $size ] : $value['url'];

	if ( $value ) {
		if ( 'image/svg' === $value['mime_type'] ) {
			the_svg( $src, true );
		} else {
			if ( $dimensions ) {
				echo '<img src="' . esc_url( $src ) . '" alt="' . esc_html( $value['title'] ) . '" width="' . esc_html( $value['width'] / 2 ) . '" height="' . esc_html( $value['height'] / 2 ) . '">';
			} else {
				echo '<img src="' . esc_url( $src ) . '" alt="' . esc_html( $value['title'] ) . '">';
			}
		}
	}
}

/**
 * Update global post to specified page
 *
 * @param string $path page path.
 */
function set_post( $path ) {
	if ( ! is_page( $path ) ) {
		global $post;
		$new_page = get_page_by_path( $path );
		$post = get_post( $new_page->ID ); // phpcs:ignore
		setup_postdata( $post );
	}
}

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
	return 100;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/**
 * Remove admin bar bump
 */
function my_filter_head() {
	remove_action( 'wp_head', '_admin_bar_bump_cb' );
}
add_action( 'get_header', 'my_filter_head' );
