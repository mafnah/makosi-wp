<?php
/**
 * The vertical align file
 *
 * Template Name: Vertical Align
 *
 * @package Makosi
 */

get_header();
?>

<main class="main page-single">
	<?php get_template_part( 'template-parts/content', 'vertical' ); ?>
</main>

<?php
get_footer();
