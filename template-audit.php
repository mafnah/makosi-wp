<?php
/**
 * The audit template file
 *
 * Template Name: Audit
 *
 * @package Makosi
 */

get_header();
?>

<main class="main audit-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/audit' ); ?>
</main>

<?php
get_footer();
