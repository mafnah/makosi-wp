<?php
/**
 * The audit candidates template file
 *
 * Template Name: Audit Candidates
 *
 * @package Makosi
 */

get_header();
?>

<main class="main audit-candidates-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/audit', 'candidates' ); ?>
</main>

<?php
get_footer();
