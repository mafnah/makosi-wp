<?php
/**
 * The clients template file
 *
 * Template Name: Clients
 *
 * @package Makosi
 */

get_header();
?>

<main class="main clients-main">
	<?php get_template_part( 'template-parts/clients' ); ?>
</main>

<?php
get_footer();
