<?php
/**
 * The blog template file
 *
 * Template Name: Blog
 *
 * @package Makosi
 */

get_header();
?>

<main class="main">
	<?php get_template_part( 'template-parts/blog' ); ?>
</main>

<?php
get_footer();
