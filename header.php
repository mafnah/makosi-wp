<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'options-';

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php if ( is_acf( $prefix . 'scripts-header', 'option' ) ) : ?>
		<?php echo get_field( $prefix . 'scripts-header', 'option' ); ?>
	<?php endif; ?>

	<?php get_template_part( 'template-parts/loader' ); ?>

	<header class="header">
		<?php if ( is_acf( $prefix . 'covid-show', 'option' ) && is_acf( $prefix . 'covid-title', 'option' ) ) : ?>
			<div class="covid-banner alert alert-primary alert-dismissible fade show" role="alert">
				<a href="#" data-toggle="modal" data-target="#covid-update">
					<?php the_acf( $prefix . 'covid-title', 'option' ); ?>
				</a>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php endif; ?>
		<div class="container-fluid">
			<div class="row top-bar">
				<div class="col">
					<?php if ( is_acf( $prefix . 'logo', 'option' ) ) : ?>
						<a class="logo" href="<?php bloginfo( 'url' ); ?>">
							<?php the_acf_image( $prefix . 'logo', '', 'option' ); ?>
						</a>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'logo-light', 'option' ) ) : ?>
						<a class="logo-light" href="<?php bloginfo( 'url' ); ?>">
							<?php the_acf_image( $prefix . 'logo-light', '', 'option' ); ?>
						</a>
					<?php endif; ?>

					<button class="nav-button">
						<span></span>
						<span class="long long-left"></span>
						<span></span>
						<span class="long long-right"></span>
					</button>

					<?php if ( is_acf( $prefix . 'nav-link-title', 'option' ) && is_acf( $prefix . 'nav-link-url', 'option' ) ) : ?>
						<a class="contact-us" href="<?php the_acf( $prefix . 'nav-link-url', 'option' ); ?>">
							<?php the_acf( $prefix . 'nav-link-title', 'option' ); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="row menu-wrapper">
				<div class="col">
					<div class="menu-lines mobile-hide">
						<?php the_svg( 'images/menu.svg' ); ?>
					</div>
					<nav>
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'main-menu',
							)
						);
						?>
					</nav>
				</div>
			</div>
		</div>
	</header>
