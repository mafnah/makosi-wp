<?php
/**
 * The IT audit clients template file
 *
 * Template Name: IT Audit Clients
 *
 * @package Makosi
 */

get_header();
?>

<main class="main it-audit-clients-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/it-audit', 'clients' ); ?>
	<?php get_template_part( 'template-parts/new-blog' ); ?>
</main>

<?php
get_footer();
