<?php
/**
 * The webinar template file
 *
 * Template Name: Webinar
 *
 * @package Makosi
 */

get_header();
?>

<main class="main webinar-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/webinar' ); ?>
	<?php get_template_part( 'template-parts/apply' ); ?>
</main>

<?php
get_footer();
