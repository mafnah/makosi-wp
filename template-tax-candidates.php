<?php
/**
 * The tax candidates template file
 *
 * Template Name: Tax Candidates
 *
 * @package Makosi
 */

get_header();
?>

<main class="main tax-candidates-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/tax', 'candidates' ); ?>
</main>

<?php
get_footer();
