<?php
/**
 * The how our program works template file
 *
 * Template Name: How our program works
 *
 * @package Makosi
 */

get_header();
?>

<main class="main how-works-main">
	<?php get_template_part( 'template-parts/how-works' ); ?>
</main>

<?php
get_footer();
