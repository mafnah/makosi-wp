const isItAuditCandidates = document.querySelector('.it-audit-candidates-main');

if (isItAuditCandidates) {
  // Audit Candidates
  route.current = '/it-audit/applicants';

  let itAuditCandidatesTimeline2 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditCandidatesTimeline3 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditCandidatesTimeline4 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditCandidatesTimeline5 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });

  const itAuditCandidatesButton = document.querySelector('#it-audit-candidates');

  // set audit candidates next timeline
  setNextTimeline(
    [
      [itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
      [itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
      [itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
      [itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
    ],
    [
      [
        [{
          targets: '#it-audit-candidates-1-1',
          translateY: ['0', '-100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#it-audit-candidates-1-2',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#it-audit-candidates-1-3',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000]
      ],
      [
        [{
          targets: '.top-blob',
          top: ['0', '-100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#it-audit-candidates-1',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#it-audit-candidates-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0]
      ],
      [
        [{
          targets: '#it-audit-candidates-2 .slide-title-top',
          translateX: ['-100%', '0'],
          duration: 2000,
        }, 0],
        [{
          targets: '#it-audit-candidates-2 .slide-title-bottom',
          translateX: ['100%', '0'],
          duration: 2000,
        }, 0],
        [{
          targets: '#it-audit-candidates-2 .slide-title',
          background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
          duration: 2000,
        }, 0]
      ],
      [
        [{
          targets: '#it-audit-candidates-2 .slide-title-top',
          translateX: ['0', '100%'],
          duration: 2000,
        }, 0],
        [{
          targets: '#it-audit-candidates-2 .slide-title-bottom',
          translateX: ['0', '-100%'],
          duration: 2000,
        }, 0],
        [{
          targets: '#it-audit-candidates-3',
          opacity: [0, 1],
          zIndex: {
            value: [1, 3],
            round: true
          },
          duration: 1,
        }, 1000],
        [{
          targets: '#it-audit-candidates-3 .before',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 1000],
        [{
          targets: '#it-audit-candidates-3 .after',
          translateX: ['0', '100%'],
          duration: 1000,
        }, 1000],
        [{
          targets: '#it-audit-candidates-3 .call-to-action',
          opacity: ['0', '1'],
          duration: 1000,
        }, 2000]
      ]
    ]
  );

  function itAuditCandidatesScroll5(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      // end audit candidates
      console.log('end audit candidates');
    } else {
      playAnimation(itAuditCandidatesTimeline5, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
    }
  }

  function itAuditCandidatesScroll4(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditCandidatesTimeline5.children.length) {
        itAuditCandidatesTimeline5.add({
          targets: '#it-audit-candidates-2 .slide-title-top',
          translateX: ['0', '100%'],
          duration: 2000,
        }, 0).add({
          targets: '#it-audit-candidates-2 .slide-title-bottom',
          translateX: ['0', '-100%'],
          duration: 2000,
        }, 0).add({
          targets: '#it-audit-candidates-3',
          opacity: [0, 1],
          zIndex: {
            value: [1, 3],
            round: true
          },
          duration: 1,
        }, 1000).add({
          targets: '#it-audit-candidates-3 .before',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 1000).add({
          targets: '#it-audit-candidates-3 .after',
          translateX: ['0', '100%'],
          duration: 1000,
        }, 1000).add({
          targets: '#it-audit-candidates-3 .call-to-action',
          opacity: ['0', '1'],
          duration: 1000,
        }, 2000);
      }

      playAnimation(itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
    } else {
      playAnimation(itAuditCandidatesTimeline4, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
    }
  }

  function itAuditCandidatesScroll3(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditCandidatesTimeline4.children.length) {
        itAuditCandidatesTimeline4.add({
          targets: '#it-audit-candidates-2 .slide-title-top',
          translateX: ['-100%', '0'],
          duration: 2000,
        }, 0).add({
          targets: '#it-audit-candidates-2 .slide-title-bottom',
          translateX: ['100%', '0'],
          duration: 2000,
        }, 0).add({
          targets: '#it-audit-candidates-2 .slide-title',
          background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
          duration: 2000,
        }, 0);
      }

      playAnimation(itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
    } else {
      playAnimation(itAuditCandidatesTimeline3, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
    }
  }

  function itAuditCandidatesScroll2(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditCandidatesTimeline3.children.length) {
        itAuditCandidatesTimeline3.add({
          targets: '.top-blob',
          top: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
    } else {
      playAnimation(itAuditCandidatesTimeline2, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
    }
  }

  function itAuditCandidatesScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditCandidatesTimeline2.children.length) {
        itAuditCandidatesTimeline2.add({
          targets: '#it-audit-candidates-1-1',
          translateY: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1-2',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1-3',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000);
      }

      playAnimation(itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
    }
  }

  window.addEventListener('wheel', itAuditCandidatesScroll);
  window.addEventListener('touchmove', itAuditCandidatesScroll);
}
