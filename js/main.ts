// @ts-nocheck

// Math.sign polyfill
if (!Math.sign) {
  Math.sign = function(x) {
    // If x is NaN, the result is NaN.
    // If x is -0, the result is -0.
    // If x is +0, the result is +0.
    // If x is negative and not -0, the result is -1.
    // If x is positive and not +0, the result is +1.
    return ((x > 0) - (x < 0)) || +x;
    // A more aesthetic pseudo-representation:
    //
    // ( (x > 0) ? 1 : 0 )  // if x is positive, then positive one
    //          +           // else (because you can't be both - and +)
    // ( (x < 0) ? -1 : 0 ) // if x is negative, then negative one
    //         ||           // if x is 0, -0, or NaN, or not a number,
    //         +x           // then the result will be x, (or) if x is
    //                      // not a number, then x converts to number
  };
}

// mobile
const mobileDevice = new MobileDetect(window.navigator.userAgent);

// loader
const loader = document.querySelector('.lds-loader');

// header
const header = document.querySelector('.header');
const headerNav = document.querySelector('.header .nav-button');

// global
const html = document.querySelector('html');
const body = document.querySelector('body');

// detect IE
function msieversion() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    body.classList.add('ie');
  }
}
msieversion();

// handle nav
function navHandler(route) {
  if (!/\d/.test(route) && route !== '/apply') {
    window.history.pushState('', '', route);

    const navLinks = document.querySelectorAll('.header nav a');
    const currentLink = document.querySelector(`.header nav a[href*="${route}"]`);

    for (var item of navLinks) {
      item.classList.remove('active');
    }

    currentLink.classList.add('active');
  }
}

// route
let route = {
  value: '',
  animationStatus: '',
  // previous: 'home',
  changed() {
    console.warn(`The route has changed to ${this.current}`);
    navHandler(this.current);
  },
  statusChanged() {
    console.warn(`Animation is ${this.status}`);
    // loader.classList.toggle('active');
  },
  get current() {
    return this.value;
  },
  set current(value) {
    this.value = value;
    this.changed();
  },
  get status() {
    return this.animationStatus;
  },
  set status(animationStatus) {
    this.animationStatus = animationStatus;
    this.statusChanged();
  }
}

// helper functions
jQuery(window).on('load', function () {
  jQuery('#page-loader').fadeOut();
});

function calcScreenOffset() {
  const intViewportHeight = window.innerHeight;
  const linesHeight = 1337;
  const linesOffset = 100;
  var offset = (intViewportHeight - linesHeight + linesOffset);

  return offset;
}

function isElementInViewport(el) {
  var rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

function homeLogo() {
  const home = document.querySelector('#home-1');
  const logo = document.querySelector('.top-bar .logo');

  if (home && isElementInViewport(home)) {
    logo.classList.add('hide');
  } else {
    logo.classList.remove('hide');
  }
}

// const routes = ['home', 'audit', 'clients', 'clients2', 'clients3', 'clients4', 'clients5', 'clients6'];

// function getPrevRoute(value) {
//   const current = routes.indexOf(value);

//   console.warn(current - 1);

//   if (current) {
//     return current - 1;
//   } else {
//     return 0;
//   }
// }

let xDown = null;
let yDown = null;

function handleTouchStart(event) {
  xDown = event.touches[0].clientX;
  yDown = event.touches[0].clientY;
};

window.addEventListener('touchstart', handleTouchStart, false);

function handleTouchMove(event) {
  let delta = 0;

  let xUp = event.touches[0].clientX;
  let yUp = event.touches[0].clientY;

  let xDiff = xDown - xUp;
  let yDiff = yDown - yUp;

  if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
    if (xDiff > 0) {
      /* left swipe */
      delta = 1;
    } else {
      /* right swipe */
    }
  } else {
    if (yDiff > 0) {
      /* up swipe */
      delta = 1;
    } else {
      /* down swipe */
    }
  }

  return delta;
};

let step = 0;
let timelines = [];
const back = document.querySelector('.go-back');
const next = document.querySelector('.go-next');
const nextLink = document.querySelector('.go-next a');
let scrollsToProceed = 0;

function playAnimation(timeline, reverse, current, remove, add, button, light, fast, duration, speed = -1, arrow = false, overflow) {
  // console.log([timeline, reverse, current, remove, add, button, light, fast, duration, speed, arrow]);
  if (!route.status && !mobileDevice.phone()) {
    if (reverse && speed) {
      scrollsToProceed = scrollsToProceed + 1;

      if (scrollsToProceed < 15) {
        console.log('scroll more ' + scrollsToProceed)
        return;
      }
    }

    route.status = true;

    if (reverse) {
      step = step - 1;
      timeline.reverse();
      body.classList.remove(`animation-${route.current.replace('/', '')}`);

      if (speed) {
        timeline.duration = speed;
        timeline.reversed = false;
        timeline.reverse();
        console.warn('stagger complete');
        scrollsToProceed = 0;
      }
    } else {
      step = step + 1;
      timeline.reversed = false;
      timeline.duration = duration;
    }

    if (fast) {
      if (light) {
        header.classList.add('header-light');
      } else {
        header.classList.remove('header-light');
      }
    }

    timeline.play();

    timeline.begin = function () {
      console.warn('fire begin');
      if (button) {
        button.classList.add('active');
      }
    };

    timeline.complete = function () {
      console.warn('fire complete');
      route.status = false;
      homeLogo();

      if (current) {
        route.current = current;
      }

      if (remove) {
        window.removeEventListener('wheel', remove);
        window.removeEventListener('touchmove', remove);
      }

      if (add) {
        window.addEventListener('wheel', add);
        window.addEventListener('touchmove', add);
      }

      if (button) {
        button.classList.remove('active');
      }

      if (light) {
        header.classList.add('header-light');
      } else {
        header.classList.remove('header-light');
      }

      if (arrow) {
        next.classList.add('active');
      } else {
        next.classList.remove('active');
      }

      if (step) {
        back.classList.add('active');
      } else {
        back.classList.remove('active');
      }

      setNextStep(reverse);

      body.classList.add(`animation-${route.current.replace('/', '')}`);

      // set previous timeline settings for back button
      if (reverse) {
        timelines.pop();
      } else {
        timelines.push([timeline, true, current, add, remove, button, light, fast, duration, speed, arrow]); // switching add/remove events
      }

      setBlogVisibility();
      handleBlogScrollbars();
    }

    if (overflow) {
      body.classList.add('overflow-auto');
      body.classList.add('white-board');
    } else {
      body.classList.remove('overflow-auto');
      body.classList.remove('white-board');
    }
  } else {
    console.log('wait... still plays', mobileDevice.phone());
  }
}

const blog = document.querySelector('.new-blog');
let isBlogVisible = false;

function setBlogVisibility() {
  if (blog) {
    if (parseInt(blog.style.opacity)) {
      isBlogVisible = true;
    } else {
      isBlogVisible = false;
    }
  }
}

function handleBlogScrollbars(click) {
  const overrideBody = document.querySelector('body');
  const overrideMain = document.querySelector('main');

  if (blog) {
    if (isBlogVisible) {
      overrideBody.style.setProperty('overflow', 'auto');
      overrideMain.style.setProperty('position', 'static');
      overrideMain.style.setProperty('top', 'auto');
      // overrideMain.style.setProperty('left', 'auto');
      overrideMain.style.setProperty('height', 'auto');
      overrideMain.style.setProperty('margin-top', 0);
      blog.style.setProperty('margin-top', 0);
    } else {
      overrideBody.style.setProperty('overflow', 'hidden');
      overrideMain.style.setProperty('position', 'fixed');
      overrideMain.style.setProperty('top', 0);
      // overrideMain.style.setProperty('left', 0);
      overrideMain.style.setProperty('width', '100%');
      overrideMain.style.setProperty('height', '100%');
      blog.style.setProperty('margin-top', '76px');
    }

    if (click && isBlogVisible) {
      overrideBody.style.setProperty('overflow', 'visible', 'important');
      window.scrollTo(0, 0);
    }
  }
}

let firstTimelineLight;
let firstTimelineArrow;

back.addEventListener('click', (e) => {
  e.preventDefault();

  setBlogVisibility();
  handleBlogScrollbars(true);

  const ancestorTimeline = timelines[timelines.length - 2];
  const previousTimeline = timelines[timelines.length - 1];
  previousTimeline[6] = (ancestorTimeline) ? ancestorTimeline[6] : firstTimelineLight;
  previousTimeline[10] = (ancestorTimeline) ? ancestorTimeline[10] : firstTimelineArrow;
  console.log(previousTimeline);

  scrollsToProceed = 15;
  playAnimation(...previousTimeline);
});

let nextTimelines;
let nextAnimations;
let nextStep = 0;
let prevNextTimelines = [];
let prevNextAnimations = [];

next.addEventListener('click', (e) => {
  e.preventDefault();

  if (!route.status && !mobileDevice.phone()) {
    if (isMiltiArray(nextTimelines)) {
      applyAnimation(nextAnimations[nextStep], nextTimelines[nextStep][0]);
      playAnimation(...nextTimelines[nextStep]);
    } else {
      playAnimation(...nextTimelines);
    }
  }
});

function isMiltiArray(arr) {
  if (arr) {
    return arr[0].constructor === Array;
  } else {
    return false;
  }
}

function applyAnimation(animation, timeline) {
  if (animation) {
    Array.prototype.forEach.call(animation, e => timeline.add(e[0], e[1]));
  }
}

function setNextTimeline(timelines, animations, step) {
  if (nextTimelines) {
    prevNextTimelines.push(nextTimelines);
  }

  if (nextAnimations) {
    prevNextAnimations.push(nextAnimations);
  }

  next.classList.add('active');
  nextTimelines = timelines;
  nextAnimations = animations;

  if (step) {
    nextStep = -1;
  }
}

function setNextStep(reverse = false) {
  if (isMiltiArray(nextTimelines)) {
    if (reverse && nextStep > -1) {
      nextStep = nextStep - 1;

      if (nextStep == -1) {
        if (prevNextTimelines) {
          nextTimelines = prevNextTimelines.pop();
        }

        if (prevNextAnimations) {
          nextAnimations = prevNextAnimations.pop();
        }
      }
    } else {
      if (nextStep < nextTimelines.length) {
        nextStep = nextStep + 1;

        if (nextStep == nextTimelines.length) {
          next.classList.remove('active');
        }
      }
    }
  }

  console.log(nextStep);
}

// Override browser's back button
const isBlogSingle = document.querySelector('.single-post');

if (!mobileDevice.phone() && !isBlogSingle) {
  history.pushState(null, document.title, location.href);
  window.addEventListener('popstate', function (event) {
    history.pushState(null, document.title, location.href);
  });

  window.onpopstate = function() {
    if (back.classList.contains('active')) {
      back.click();
    }
  }
}

// toggle menu
headerNav.addEventListener('click', () => {
  header.classList.toggle('opened');
});

// scroll to anchor
Array.prototype.forEach.call(document.querySelectorAll('a[href^="#"]'), anchor => {
  anchor.addEventListener('click', function (e) {
    e.preventDefault();

    if (this.getAttribute('href') !== '#back' || this.getAttribute('href') !== '#next') {
      document.querySelector(this.getAttribute('href')).scrollIntoView({
        behavior: 'smooth'
      });
    }
  });
});

// fetch json feed
async function getFeed(url) {
  let response = await fetch(url);
  let data = await response.json()
  return data;
}

jQuery(window).load(function () {
  // set first timeline
  const header = document.querySelector('.header');
  firstTimelineLight = (header.classList.contains('header-light')) ? true : false;
  firstTimelineArrow = (next.classList.contains('active')) ? true : false;
});

jQuery(document).ready(function($) {
  // shave blog content
  function shaveBlogs() {
    const blogPosts = document.querySelectorAll('.blog-post');

    Array.prototype.forEach.call(blogPosts, blogPost => {
      const postHeading = (blogPost.querySelector('h2')) ? blogPost.querySelector('h2').offsetHeight : blogPost.querySelector('h4').offsetHeight;
      const postContent = blogPost.querySelector('p');
      shave(postContent, 100 - postHeading);
    });
  }

  // fetch instagram images
  function loadInstagram() {
    const instagram = document.querySelector('.insta');
    const feed = (instagram.dataset.feed) ? instagram.dataset.feed : 'https://www.instagram.com/makosi_consulting/';

    getFeed(`${feed}?__a=1`).then(data => {
      if (data) {
        const images = data.graphql.user.edge_owner_to_timeline_media.edges;

        if (images) {
          instagram.innerHTML = '';
          images.slice(0, 15).map(image => {
            const url = `https://www.instagram.com/p/${image.node.shortcode}/`;
            const title = (image.node.edge_media_to_caption && image.node.edge_media_to_caption.edges[0] && image.node.edge_media_to_caption.edges[0].node && image.node.edge_media_to_caption.edges[0].node.text) ? image.node.edge_media_to_caption.edges[0].node.text : 'Makosi';
            const thumbnail = (image.node.thumbnail_resources[1].src) ? image.node.thumbnail_resources[1].src : '/wp-content/themes/makosi/images/insta-8@2x.jpg';

            instagram.innerHTML += `
              <a href="${url}" target="_blank">
                <img src="${thumbnail}" alt="${title}">
              </a>
            `;
          });
        }
      }
    });
  }

  // get blog content
  function getBlog(remove) {
    $('#page-loader').show();

    if (remove) {
      window.removeEventListener('wheel', remove);
      window.removeEventListener('touchmove', remove);
    }

    $('main').load('/blog .new-blog', function() {
      $('main').attr('class', 'main main-new-blog');
      $('body').attr('class', 'page-template page-template-template-new-blog page-template-template-new-blog-php page page-parent overflow-auto');
      $('header').addClass('header-light');
      $('#go-back').hide();
      route.current = '/blog';

      $('#page-loader').hide();

      shaveBlogs();
      loadInstagram();
    });
  }

  if (mobileDevice.phone()) {
    html.classList.add('mobile-device');
  }

  const isHome = document.querySelector('#home-1');

  if (isHome) {
    // Home functions
    const moreButton = document.querySelector('#more');
    let homeTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    homeLogo();

    if (!homeTimeline.children.length) {
      homeTimeline.add({
        targets: '.lines svg',
        opacity: [0, 1],
        bottom: [
          {
            value: [-700, calcScreenOffset()],
            duration: 3000,
            delay: 0
          }
        ],
        right: [
          {
            value: [-1150, -1380],
            duration: 3000,
            delay: 0
          }
        ],
        duration: 100,
      }, 0).add({
        targets: '.lines .top-line path',
        strokeDashoffset: [anime.setDashoffset, 0],
        delay: function (el, i) { return i * 250 },
        duration: 3000,
      }, 0).add({
        targets: '.lines .bot-line path',
        strokeDashoffset: [anime.setDashoffset, 0],
        delay: function (el, i) { return i * 250 },
        duration: 3000,
      }, 0).add({
        targets: '.lines .hor-line-1 path',
        strokeDashoffset: [anime.setDashoffset, 0],
        delay: function (el, i) { return i * 250 },
        duration: 3000,
      }, 0).add({
        targets: '.lines .hor-line-2 path',
        strokeDashoffset: [anime.setDashoffset, 0],
        delay: function (el, i) { return i * 250 },
        duration: 3000,
      }, 0).add({
        targets: '.lines .hor-line-3 path',
        strokeDashoffset: [anime.setDashoffset, 0],
        delay: function (el, i) { return i * 250 },
        duration: 3000,
      }, 0).add({
        targets: ['#home-1', '.lines'],
        translateX: ['0', '-100%'],
        duration: 2000,
      }, 1000).add({
        targets: '#home-2',
        translateX: ['100%', '0'],
        duration: 2000,
      }, 1000);
    }

    // set home next timeline
    setNextTimeline([homeTimeline, false, '/', homeScroll, homeScroll2, false, false, false, 3000, -1, false]);

    function homeScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(homeTimeline, false, '/', homeScroll, homeScroll2, false, false, false, 3000, -1, false);
      }
    }

    function homeScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta != '1') {
        playAnimation(homeTimeline, true, '/', homeScroll2, homeScroll, false, false, false, 3000, -1, true);
      }
    }

    function animateHome(event) {
      event.preventDefault();
      playAnimation(homeTimeline, false, '/', homeScroll, homeScroll2, moreButton, false, false, 3000, -1, false);
    }

    window.addEventListener('wheel', homeScroll);
    window.addEventListener('touchmove', homeScroll);

    if (moreButton) {
      moreButton.addEventListener('click', animateHome);
    }

    // Audit
    const auditButton = document.querySelector('#audit');
    let auditTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    function auditScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta != '1') {
        if (isHome) {
          playAnimation(auditTimeline, true, '/', auditScroll, homeScroll2, auditButton, false, false, 1000);
        }
      }
    }

    function animateAudit(event) {
      if (auditButton) {
        event.preventDefault();
      }

      if (!auditTimeline.children.length) {
        auditTimeline.add({
          targets: '.lines .rest',
          opacity: [0, 1],
          duration: 1000,
        }, 0).add({
          targets: '.lines',
          translateX: '-100%',
          translateY: '-50%',
          right: '100px',
          duration: 1000,
        }, 0).add({
          targets: '#home-2',
          translateY: [0, '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-1',
          translateY: ['100%', '-100%'],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditTimeline, false, '/audit', homeScroll2, auditScroll, auditButton, false, false, 1000);
    }

    if (auditButton) {
      auditButton.addEventListener('click', animateAudit);
    }

    // Audit Clients
    const auditClientsButton = document.querySelector('#audit-clients');
    let auditClientsTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline7 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    if (!auditClientsTimeline2.children.length) {
      auditClientsTimeline2.add({
        targets: '.welcome-content',
        translateY: ['0', '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section, .why-content',
        translateY: ['100%', '0'],
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.why-content .section-title',
        translateY: ['100%', '0'],
        rotate: ['-90deg', '-90deg'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section .fact-0',
        opacity: [0, 1],
        duration: 1000,
      }, 1000).add({
        targets: '.why-section .fact-1',
        opacity: [0, 1],
        duration: 1000,
      }, 2000).add({
        targets: '.why-section .fact-2',
        opacity: [0, 1],
        duration: 1000,
      }, 3000);
    }

    function animateAuditClients(event) {
      if (auditClientsButton) {
        event.preventDefault();
      }

      if (!auditClientsTimeline.children.length) {
        auditClientsTimeline.add({
          targets: '.top-blob, #audit-clients-1',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditClientsTimeline, false, '/audit/clients', auditScroll, auditClientsScroll, auditClientsButton, false, false, 1000, -1, true);

      // set audit clients next timeline
      setNextTimeline(
        [
          [auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
          [auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
          [auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
          [auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
          [auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
          [auditClientsTimeline7, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
        ],
        [
          [
            [
              {
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ]
          ],
          [
            [
              {
                targets: '.top-blob',
                top: ['0', '100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-2',
                translateY: ['-100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-2 .side-title',
                top: ['150%', '50%'],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts h2',
                opacity: [0, 0.25],
                borderBottom: '1px solid #fff',
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-0 p',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-1 p',
                opacity: [0, 1],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-2 p',
                opacity: [0, 1],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              6000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-3 p',
                opacity: [0, 1],
                duration: 1000,
              },
              6000
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.left-blob',
                translateX: ['100%', '-50%'],
                duration: 1500,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-3',
                translateX: ['100%', '0'],
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1000,
              },
              500
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-3',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-3',
                translateX: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-4',
                translateX: ['100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-4 .first-title',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-4 .second-title',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#audit-clients-4 .third-title',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#audit-clients-4 p',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#audit-clients-4 .industries',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '.lines-single .top-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .bot-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-1 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-2 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-3 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ]
          ],
          [
            [
              {
                targets: '#audit-clients-5',
                translateY: ['-100%', '0'],
                duration: 1000,
              }, 0
            ]
          ],
          [
            [
              {
                targets: '.new-blog',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#home-1, #audit-1',
                height: ['100vh', 0],
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.new-blog',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ]
          ],
        ],
        true
      );
    }

    function auditClientsScroll7(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end of audit clients
        console.log('end audit clients animations');
      } else {
        console.log('no scrolling events inside blog');
      }
    }

    function auditClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline7.children.length) {
          auditClientsTimeline7.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#home-1, #audit-1',
            height: ['100vh', 0],
            opacity: [1, 0],
            duration: 1,
          },
          0).add({
            targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditClientsTimeline7, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(auditClientsTimeline6, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
      }
    }

    function auditClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline6.children.length) {
          auditClientsTimeline6.add({
            targets: '#audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
      } else {
        playAnimation(auditClientsTimeline5, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
      }
    }

    function auditClientsScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline5.children.length) {
          auditClientsTimeline5.add({
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0);
        }

        playAnimation(auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
      } else {
        playAnimation(auditClientsTimeline4, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
      }
    }

    function auditClientsScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline4.children.length) {
          auditClientsTimeline4.add({
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          }, 0).add({
            targets: '#audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 500);
        }

        playAnimation(auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
      } else {
        playAnimation(auditClientsTimeline3, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
      }
    }

    function auditClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline3.children.length) {
          auditClientsTimeline3.add({
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 3000).add({
            targets: '#audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '#audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000);
        }

        playAnimation(auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
      } else {
        playAnimation(auditClientsTimeline2, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
      }
    }

    function auditClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
      } else {
        playAnimation(auditClientsTimeline, true, '/audit', auditClientsScroll, auditScroll, auditClientsButton, false, false, 1000);
      }
    }

    if (auditClientsButton) {
      auditClientsButton.addEventListener('click', animateAuditClients);
    }

    // Audit Candidates
    let auditCandidatesTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    const auditCandidatesButton = document.querySelector('#audit-candidates');

    function auditCandidatesScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end audit candidates
        console.log('end audit candidates');
      } else {
        playAnimation(auditCandidatesTimeline5, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
      }
    }

    function auditCandidatesScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline5.children.length) {
          auditCandidatesTimeline5.add({
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000).add({
            targets: '#audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000);
        }

        playAnimation(auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline4, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
      }
    }

    function auditCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline4.children.length) {
          auditCandidatesTimeline4.add({
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
            duration: 2000,
          }, 0);
        }

        playAnimation(auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline3, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
      }
    }

  function auditCandidatesScroll2(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditCandidatesTimeline3.children.length) {
        auditCandidatesTimeline3.add({
          targets: '.top-blob',
          top: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
    } else {
      playAnimation(auditCandidatesTimeline2, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
    }
  }

  function auditCandidatesScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditCandidatesTimeline2.children.length) {
        auditCandidatesTimeline2.add({
          targets: '#audit-candidates-1-1',
          translateY: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1-2',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1-3',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000);
      }

      playAnimation(auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
    } else {
      playAnimation(auditCandidatesTimeline, true, '/audit', auditCandidatesScroll, auditScroll, auditCandidatesButton, false, false, 2000);
    }
  }

  function animateCandidates(event) {
    if (auditCandidatesButton) {
      event.preventDefault();
    }

    if (!auditCandidatesTimeline.children.length) {
      auditCandidatesTimeline.add({
        targets: '.top-blob, #audit-candidates-1',
        translateY: ['-100%', '0'],
        duration: 1000,
      }, 0).add({
        targets: '#audit-candidates-1-2',
        translateX: ['-100%', '0'],
        duration: 1000,
      }, 1000);
    }

    playAnimation(auditCandidatesTimeline, false, '/audit/applicants', auditScroll, auditCandidatesScroll, auditCandidatesButton, false, false, 2000, -1, true);

    // set audit candidates next timeline
    setNextTimeline(
      [
        [auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
        [auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
        [auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
        [auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
      ],
      [
        [
          [{
            targets: '#audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000]
        ],
        [
          [{
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
            duration: 2000,
          }, 0]
        ],
        [
          [{
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000],
          [{
            targets: '#audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000]
        ]
      ],
      true
    );
  }

  if (auditCandidatesButton) {
    auditCandidatesButton.addEventListener('click', animateCandidates);
  }

  // new start
    // itAudit
    const itAuditButton = document.querySelector('#it-audit');
    let itAuditTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    function itAuditScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta != '1') {
        if (isHome) {
          playAnimation(itAuditTimeline, true, '/', itAuditScroll, homeScroll2, itAuditButton, false, false, 1000);
        }
      }
    }

    function animateItAudit(event) {
      if (itAuditButton) {
        event.preventDefault();
      }

      if (!itAuditTimeline.children.length) {
        itAuditTimeline.add({
          targets: '.lines .rest',
          opacity: [0, 1],
          duration: 1000,
        }, 0).add({
          targets: '.lines',
          translateX: '-100%',
          translateY: '-50%',
          right: '100px',
          duration: 1000,
        }, 0).add({
          targets: '#home-2',
          translateY: [0, '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-1',
          translateY: ['100%', '-100%'],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditTimeline, false, '/it-audit', homeScroll2, itAuditScroll, itAuditButton, false, false, 1000);
    }

    if (itAuditButton) {
      itAuditButton.addEventListener('click', animateItAudit);
    }

    // itAudit Clients
    const itAuditClientsButton = document.querySelector('#it-audit-clients');
    let itAuditClientsTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline7 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    if (!itAuditClientsTimeline2.children.length) {
      itAuditClientsTimeline2.add({
        targets: '.welcome-content',
        translateY: ['0', '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section, .why-content',
        translateY: ['100%', '0'],
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.why-content .section-title',
        translateY: ['100%', '0'],
        rotate: ['-90deg', '-90deg'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section .fact-0',
        opacity: [0, 1],
        duration: 1000,
      }, 1000).add({
        targets: '.why-section .fact-1',
        opacity: [0, 1],
        duration: 1000,
      }, 2000).add({
        targets: '.why-section .fact-2',
        opacity: [0, 1],
        duration: 1000,
      }, 3000);
    }

    function animateItAuditClients(event) {
      if (itAuditClientsButton) {
        event.preventDefault();
      }

      if (!itAuditClientsTimeline.children.length) {
        itAuditClientsTimeline.add({
          targets: '.top-blob, #it-audit-clients-1',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditClientsTimeline, false, '/it-audit/clients', itAuditScroll, itAuditClientsScroll, itAuditClientsButton, false, false, 1000, -1, true);

      // set itAudit clients next timeline
      setNextTimeline(
        [
          [itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
          [itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
          [itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
          [itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
          [itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
          [itAuditClientsTimeline7, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
        ],
        [
          [
            [
              {
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ]
          ],
          [
            [
              {
                targets: '.top-blob',
                top: ['0', '100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-2',
                translateY: ['-100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-2 .side-title',
                top: ['150%', '50%'],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts h2',
                opacity: [0, 0.25],
                borderBottom: '1px solid #fff',
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                opacity: [0, 1],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                opacity: [0, 1],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              6000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                opacity: [0, 1],
                duration: 1000,
              },
              6000
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.left-blob',
                translateX: ['100%', '-50%'],
                duration: 1500,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-3',
                translateX: ['100%', '0'],
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1000,
              },
              500
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-3',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-3',
                translateX: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-4',
                translateX: ['100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-4 .first-title',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-4 .second-title',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#it-audit-clients-4 .third-title',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#it-audit-clients-4 p',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#it-audit-clients-4 .industries',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '.lines-single .top-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .bot-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-1 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-2 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-3 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ]
          ],
          [
            [
              {
                targets: '#it-audit-clients-5',
                translateY: ['-100%', '0'],
                duration: 1000,
              }, 0
            ]
          ],
          [
            [
              {
                targets: '.new-blog',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#home-1, #it-audit-1',
                height: ['100vh', 0],
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.new-blog',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ]
          ],
        ],
        true
      );
    }

    function itAuditClientsScroll7(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end of itAudit clients
        console.log('end itAudit clients animations');
      } else {
        console.log('no scrolling events inside blog');
      }
    }

    function itAuditClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline7.children.length) {
          itAuditClientsTimeline7.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#home-1, #it-audit-1, #audit-1',
            height: ['100vh', 0],
            opacity: [1, 0],
            duration: 1,
          },
          0).add({
            targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline7, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(itAuditClientsTimeline6, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
      }
    }

    function itAuditClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline6.children.length) {
          itAuditClientsTimeline6.add({
            targets: '#it-audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline5, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
      }
    }

    function itAuditClientsScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline5.children.length) {
          itAuditClientsTimeline5.add({
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline4, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
      }
    }

    function itAuditClientsScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline4.children.length) {
          itAuditClientsTimeline4.add({
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          }, 0).add({
            targets: '#it-audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 500);
        }

        playAnimation(itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline3, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
      }
    }

    function itAuditClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline3.children.length) {
          itAuditClientsTimeline3.add({
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 3000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000);
        }

        playAnimation(itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline2, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
      }
    }

    function itAuditClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline, true, '/it-audit', itAuditClientsScroll, itAuditScroll, itAuditClientsButton, false, false, 1000);
      }
    }

    if (itAuditClientsButton) {
      itAuditClientsButton.addEventListener('click', animateItAuditClients);
    }

    // itAudit Candidates
    let itAuditCandidatesTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    const itAuditCandidatesButton = document.querySelector('#it-audit-candidates');

    function itAuditCandidatesScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end itAudit candidates
        console.log('end itAudit candidates');
      } else {
        playAnimation(itAuditCandidatesTimeline5, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
      }
    }

    function itAuditCandidatesScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline5.children.length) {
          itAuditCandidatesTimeline5.add({
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000);
        }

        playAnimation(itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline4, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
      }
    }

    function itAuditCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline4.children.length) {
          itAuditCandidatesTimeline4.add({
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
            duration: 2000,
          }, 0);
        }

        playAnimation(itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline3, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
      }
    }

  function itAuditCandidatesScroll2(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditCandidatesTimeline3.children.length) {
        itAuditCandidatesTimeline3.add({
          targets: '.top-blob',
          top: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
    } else {
      playAnimation(itAuditCandidatesTimeline2, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
    }
  }

  function itAuditCandidatesScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditCandidatesTimeline2.children.length) {
        itAuditCandidatesTimeline2.add({
          targets: '#it-audit-candidates-1-1',
          translateY: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1-2',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1-3',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000);
      }

      playAnimation(itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
    } else {
      playAnimation(itAuditCandidatesTimeline, true, '/it-audit', itAuditCandidatesScroll, itAuditScroll, itAuditCandidatesButton, false, false, 2000);
    }
  }

  function animateItCandidates(event) {
    if (itAuditCandidatesButton) {
      event.preventDefault();
    }

    if (!itAuditCandidatesTimeline.children.length) {
      itAuditCandidatesTimeline.add({
        targets: '.top-blob, #it-audit-candidates-1',
        translateY: ['-100%', '0'],
        duration: 1000,
      }, 0).add({
        targets: '#it-audit-candidates-1-2',
        translateX: ['-100%', '0'],
        duration: 1000,
      }, 1000);
    }

    playAnimation(itAuditCandidatesTimeline, false, '/it-audit/applicants', itAuditScroll, itAuditCandidatesScroll, itAuditCandidatesButton, false, false, 2000, -1, true);

    // set itAudit candidates next timeline
    setNextTimeline(
      [
        [itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
        [itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
        [itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
        [itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
      ],
      [
        [
          [{
            targets: '#it-audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000]
        ],
        [
          [{
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
            duration: 2000,
          }, 0]
        ],
        [
          [{
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000],
          [{
            targets: '#it-audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#it-audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#it-audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000]
        ]
      ],
      true
    );
  }

  if (itAuditCandidatesButton) {
    itAuditCandidatesButton.addEventListener('click', animateItCandidates);
  }

  // new end

  // Tax
  const taxButton = document.querySelector('#tax');
  let taxTimeline = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });

  function taxScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta != '1') {
      if (isHome) {
        playAnimation(taxTimeline, true, '/', taxScroll, homeScroll2, taxButton, false, false, 1000);
      }
    }
  }

  function animateTax(event) {
    if (taxButton) {
      event.preventDefault();
    }

    if (!taxTimeline.children.length) {
      taxTimeline.add({
        targets: '.lines .rest',
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.lines',
        translateX: '-225%',
        translateY: '-75%',
        scale: 2,
        duration: 1000,
      }, 0).add({
        targets: '#home-2',
        translateX: [0, '-100%'],
        translateY: [0, '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '#tax-1',
        translateX: ['100%', '0'],
        duration: 1000,
      }, 0);
    }

    playAnimation(taxTimeline, false, '/tax', homeScroll2, taxScroll, taxButton, false, false, 1000);
  }

  if (taxButton) {
    taxButton.addEventListener('click', animateTax);
  }

  // Tax Clients
  const taxClientsButton = document.querySelector('#tax-clients');
  let taxClientsTimeline = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let taxClientsTimeline2 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
    // let taxClientsTimeline3 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });
    // let taxClientsTimeline4 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });

    let taxClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    let taxClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    function taxClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end tax clients
        console.log('end tax clients');
      } else {
        console.log('no scrolling inside blog');
      }
    }

    function taxClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxClientsTimeline6.children.length) {
          taxClientsTimeline6.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#home-1, #audit-1, #tax-1',
            height: ['100vh', 0],
            opacity: [1, 0],
            duration: 1,
          },
          0).add({
            targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(taxClientsTimeline6, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(taxClientsTimeline5, true, 'tax/clients2', taxClientsScroll5, taxClientsScroll2, false, false, false, 1600, -1, true);
      }
    }

    // function taxClientsScroll4(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // end tax clients
    //    console.log('end tax clients');
    //  } else {
    //    playAnimation(taxClientsTimeline4, true, 'tax/clients3', taxClientsScroll4, taxClientsScroll3, false, true);
    //  }
    // }

    // function taxClientsScroll3(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // taxClientsTimeline4.add({
    //    //  targets: '.left-blob',
    //    //  opacity: 0,
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-3',
    //    //  zIndex: {
    //    //    value: [3, 2],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '.left-blob-blue',
    //    //  zIndex: {
    //    //    value: [2, 3],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-4',
    //    //  zIndex: {
    //    //    value: [2, 3],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '.left-blob-blue',
    //    //  translateX: ['100%', '-75%'],
    //    //  duration: 1500,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-4',
    //    //  translateX: ['100%', '0'],
    //    //  duration: 1000,
    //    //  offset: 600
    //    // });

    //     // playAnimation(taxClientsTimeline4, false, 'tax/clients4', taxClientsScroll3, taxClientsScroll4);

    //     // end tax clients
    //    console.log('end tax clients');
    //  } else {
    //    playAnimation(taxClientsTimeline3, true, 'tax/clients2', taxClientsScroll3, taxClientsScroll2);
    //  }
    // }

    function taxClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxClientsTimeline3.add({
        //  targets: '.bottom-blob',
        //  opacity: 0,
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '.left-blob',
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '#tax-clients-2',
        //  zIndex: {
        //    value: [3, 2],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '.left-blob',
        //  translateX: ['100%', '-50%'],
        //  duration: 1500,
        //  offset: 0
        // }).add({
        //  targets: '#tax-clients-3',
        //  translateX: ['100%', '0'],
        //  duration: 1000,
        //  offset: 500,
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        // });

        // playAnimation(taxClientsTimeline3, false, 'tax/clients3', taxClientsScroll2, taxClientsScroll3, false, true);

        // end tax clients
        // console.log('end tax clients');

        if (!taxClientsTimeline5.children.length) {
          taxClientsTimeline5.add({
            targets: '.bottom-blob',
            opacity: 0,
            duration: 1,
          }, 0).add({
            targets: '#tax-clients-2',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#tax-clients-2',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 600).add({
            targets: '#tax-clients-5',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#tax-clients-5',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 600);
        }

        playAnimation(taxClientsTimeline5, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true);
      } else {
        playAnimation(taxClientsTimeline2, true, '/tax/clients', taxClientsScroll2, taxClientsScroll, false, true, false, 8000, -1, true);
      }
    }

    function taxClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxClientsTimeline2.children.length) {
          taxClientsTimeline2.add({
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#tax-clients-2',
            translateY: ['100%', '0'],
            duration: 1000,
            zIndex: {
              value: [2, 3],
              round: true
            },
          }, 1000).add({
            targets: '#tax-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 2000).add({
            targets: '#tax-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 3000).add({
            targets: '#tax-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#tax-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#tax-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#tax-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#tax-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#tax-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000).add({
            targets: '#tax-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 7000).add({
            targets: '#tax-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 7000);
        }

        playAnimation(taxClientsTimeline2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true);
      } else {
        playAnimation(taxClientsTimeline, true, '/tax', taxClientsScroll, taxScroll, taxClientsButton, false, true, 4500);
      }
    }

    function animateTaxClients(event) {
      if (taxClientsButton) {
        event.preventDefault();
      }

      if (!taxClientsTimeline.children.length) {
        taxClientsTimeline.add({
          targets: '.lines',
          opacity: [1, 0],
          duration: 1000,
        }, 0).add({
          targets: '#tax-1',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '.right-blob',
          translateX: ['100%', '0'],
          left: ['0', '-100%'],
          duration: 2000,
        }, 0).add({
          targets: '.right-blob',
          opacity: 0,
          duration: 100,
        }, 2000).add({
          targets: '#tax-clients-1',
          translateX: ['100%', '0'],
          duration: 1000,
        }, 500).add({
          targets: '#tax-clients-1 #tax-item-0',
          opacity: [0, 1],
          duration: 1000,
        }, 1500).add({
          targets: '#tax-clients-1 #tax-item-1',
          opacity: [0, 1],
          duration: 1000,
        }, 2500).add({
          targets: '#tax-clients-1 #tax-item-2',
          opacity: [0, 1],
          duration: 1000,
        }, 3500);
      }

      playAnimation(taxClientsTimeline, false, '/tax/clients', taxScroll, taxClientsScroll, taxClientsButton, true, false, 4500, -1, true);

      // set taxt clients next timeline
      setNextTimeline(
        [
          [taxClientsTimeline2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true],
          [taxClientsTimeline5, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true],
          [taxClientsTimeline6, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true]
        ],
        [
          [
            [{
              targets: '.bottom-blob',
              translateY: ['100%', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#tax-clients-2',
              translateY: ['100%', '0'],
              duration: 1000,
              zIndex: {
                value: [2, 3],
                round: true
              },
            }, 1000],
            [{
              targets: '#tax-clients-2 .side-title',
              top: ['150%', '50%'],
              duration: 1000,
            }, 2000],
            [{
              targets: '#tax-clients-2 .audit-facts h2',
              opacity: [0, 0.25],
              borderBottom: '1px solid #fff',
              duration: 1000,
            }, 3000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-0 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 4000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-0 p',
              opacity: [0, 1],
              duration: 1000,
            }, 4000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-1 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 5000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-1 p',
              opacity: [0, 1],
              duration: 1000,
            }, 5000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-2 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 6000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-2 p',
              opacity: [0, 1],
              duration: 1000,
            }, 6000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-3 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 7000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-3 p',
              opacity: [0, 1],
              duration: 1000,
            }, 7000]
          ],
          [
            [{
              targets: '.bottom-blob',
              opacity: 0,
              duration: 1,
            }, 0],
            [{
              targets: '#tax-clients-2',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '#tax-clients-2',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 600],
            [{
              targets: '#tax-clients-5',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '#tax-clients-5',
              translateX: ['100%', '0'],
              duration: 1000,
            }, 600]
          ],
          [
            [{
              targets: '.new-blog',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
                targets: '#home-1, #audit-1, #tax-1',
                height: ['100vh', 0],
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [{
              targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
              opacity: [1, 0],
              duration: 1,
            }, 0],
            [{
              targets: '.new-blog',
              opacity: [0, 1],
              duration: 1000,
            }, 0]
          ]
        ],
        true
      );
    }

    if (taxClientsButton) {
      taxClientsButton.addEventListener('click', animateTaxClients);
    }

    // Tax Candidates
    const taxCandidatesButton = document.querySelector('#tax-candidates');
    let taxCandidatesTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let taxCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let taxCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    // let taxCandidatesTimeline4 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });

    // function taxCandidatesScroll4(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // end tax candidates
    //    console.log('end tax candidates');
    //  } else {
    //    playAnimation(taxCandidatesTimeline4, true, 'tax/candidates3', taxCandidatesScroll4, taxCandidatesScroll3);
    //  }
    // }

    function taxCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxCandidatesTimeline4.add({
        //  targets: '#col-0, #col-1',
        //  translateX: ['0', '-100vw'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#col-2, #col-3',
        //  translateX: ['0', '100vw'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#mid-arrow',
        //  opacity: 0,
        //  duration: 100,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3 h1',
        //  translateY: ['0', '-100vh'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3',
        //  zIndex: {
        //    value: [3, 2],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 1000,
        // }).add({
        //  targets: '#tax-candidates-4',
        //  zIndex: {
        //    value: [1, 3],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 1000
        // }).add({
        //  targets: '#tax-candidates-4',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 1000
        // }).add({
        //  targets: '#tax-candidates-4 .call-to-action',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 2000
        // });

        // playAnimation(taxCandidatesTimeline4, false, 'tax/candidates4', taxCandidatesScroll3, taxCandidatesScroll4);

        // end tax candidates
        console.log('end tax candidates');
      } else {
        playAnimation(taxCandidatesTimeline3, true, 'tax/applicants2', taxCandidatesScroll3, taxCandidatesScroll2, false, false, false, 2100, -1, true);
      }
    }

    function taxCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxCandidatesTimeline3.add({
        //  targets: '.bottom-blob',
        //  translateY: ['100%', '-100%'],
        //  duration: 2000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3',
        //  translateY: ['100%', '0'],
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        //  duration: 1000,
        //  offset: 1000,
        // }).add({
        //  targets: '#col-0 svg, #col-0 h2, #col-0 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 2000
        // }).add({
        //  targets: '#col-1 svg, #col-1 h2, #col-1 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 3000
        // }).add({
        //  targets: '#col-2 svg, #col-2 h2, #col-2 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 4000
        // }).add({
        //  targets: '#col-3 svg, #col-3 h2, #col-3 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 5000
        // });

        if (!taxCandidatesTimeline3.children.length) {
          taxCandidatesTimeline3.add({
            targets: '.right-blob',
            left: ['-100%', '0'],
            translateX: ['0', '-100%'],
            opacity: [0, 1],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#tax-candidates-2',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '#tax-candidates-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1000,
          }, 0).add({
            targets: '#tax-candidates-3 .container-fluid',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#tax-candidates-1',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.lines',
            opacity: [1, 0],
            duration: 1000,
          }, 0).add({
            targets: '#tax-1',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '.right-blob',
            translateX: ['100%', '0'],
            left: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '.right-blob',
            opacity: 0,
            duration: 100,
          }, 2000).add({
            targets: '#tax-candidates-1',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 500);
        }

        playAnimation(taxCandidatesTimeline3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true);
      } else {
        playAnimation(taxCandidatesTimeline2, true, '/tax/applicants', taxCandidatesScroll2, taxCandidatesScroll, false, true, true, 6000, -1, true);
      }
    }

    function taxCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxCandidatesTimeline2.children.length) {
          taxCandidatesTimeline2.add({
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#tax-candidates-3',
            translateY: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 1000).add({
            targets: '#col-0 svg, #col-0 h2, #col-0 p',
            opacity: 1,
            duration: 1000,
          }, 2000).add({
            targets: '#col-1 svg, #col-1 h2, #col-1 p',
            opacity: 1,
            duration: 1000,
          }, 3000).add({
            targets: '#col-2 svg, #col-2 h2, #col-2 p',
            opacity: 1,
            duration: 1000,
          }, 4000).add({
            targets: '#col-3 svg, #col-3 h2, #col-3 p',
            opacity: 1,
            duration: 1000,
          }, 5000);
        }

        playAnimation(taxCandidatesTimeline2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true);
      } else {
        playAnimation(taxCandidatesTimeline, true, '/tax', taxCandidatesScroll, taxScroll, taxCandidatesButton, false, true, 3000);
      }

      // if (delta == '1') {
        // taxCandidatesTimeline2.add({
        //  targets: '#tax-candidates-1',
        //  translateX: ['0', '-100%'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2',
        //  marginLeft: [0, -1],
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2',
        //  translateX: ['100%', '0'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2 #love',
        //  translateX: ['200%', '0'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2 #tax',
        //  translateX: ['-200vw', '0'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2 .content-wrap',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 1000
        // });

        // playAnimation(taxCandidatesTimeline, false, 'tax/candidates2', taxCandidatesScroll, taxCandidatesScroll2, false, true, true);

      // }
    }

    function animateTaxCandidates(event) {
      if (taxCandidatesButton) {
        event.preventDefault();
      }

      if (!taxCandidatesTimeline.children.length) {
        taxCandidatesTimeline.add({
          targets: '.lines',
          opacity: [1, 0],
          duration: 1000,
        }, 0).add({
          targets: '.right-blob',
          translateX: ['100%', '0'],
          left: ['0', '-100%'],
          duration: 2000,
        }, 0).add({
          targets: '.right-blob',
          opacity: [1, 0],
          duration: 100,
        }, 2500).add({
          targets: '#tax-candidates-2',
          marginLeft: [0, -1],
          duration: 1,
        }, 0).add({
          targets: '#tax-candidates-2',
          translateX: ['100%', '0'],
          duration: 1000,
        }, 500).add({
          targets: '#tax-candidates-2 #love',
          translateX: ['200%', '0'],
          duration: 1000,
        }, 1000).add({
          targets: '#tax-candidates-2 #tax',
          translateX: ['-200vw', '0'],
          duration: 1000,
        }, 1000).add({
          targets: '#tax-candidates-2 .content-wrap',
          opacity: [0, 1],
          duration: 1000,
        }, 2000);
      }

      playAnimation(taxCandidatesTimeline, false, '/tax/applicants', taxScroll, taxCandidatesScroll, taxCandidatesButton, true, false, 3000, -1, true);

      // set tat candidates next timeline
      setNextTimeline(
        [
          [taxCandidatesTimeline2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true],
          [taxCandidatesTimeline3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true]
        ],
        [
          [
            [{
              targets: '.bottom-blob',
              translateY: ['100%', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#tax-candidates-3',
              translateY: ['100%', '0'],
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1000,
            }, 1000],
            [{
              targets: '#col-0 svg, #col-0 h2, #col-0 p',
              opacity: 1,
              duration: 1000,
            }, 2000],
            [{
              targets: '#col-1 svg, #col-1 h2, #col-1 p',
              opacity: 1,
              duration: 1000,
            }, 3000],
            [{
              targets: '#col-2 svg, #col-2 h2, #col-2 p',
              opacity: 1,
              duration: 1000,
            }, 4000],
            [{
              targets: '#col-3 svg, #col-3 h2, #col-3 p',
              opacity: 1,
              duration: 1000,
            }, 5000]
          ],
          [
            [{
              targets: '.right-blob',
              left: ['-100%', '0'],
              translateX: ['0', '-100%'],
              opacity: [0, 1],
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '#tax-candidates-2',
              opacity: [1, 0],
              duration: 1,
            }, 0],
            [{
              targets: '#tax-candidates-3',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1000,
            }, 0],
            [{
              targets: '#tax-candidates-3 .container-fluid',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#tax-candidates-1',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '.lines',
              opacity: [1, 0],
              duration: 1000,
            }, 0],
            [{
              targets: '#tax-1',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '.right-blob',
              translateX: ['100%', '0'],
              left: ['0', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '.right-blob',
              opacity: 0,
              duration: 100,
            }, 2000],
            [{
              targets: '#tax-candidates-1',
              translateX: ['100%', '0'],
              duration: 1000,
            }, 500]
          ]
        ],
        true
      );
    }

    if (taxCandidatesButton) {
      taxCandidatesButton.addEventListener('click', animateTaxCandidates);
    }
  }

  const isAbout = document.querySelector('#about-1');

  if (isAbout) {
    // About functions
    route.current = '/about';

    let aboutTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let aboutTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let aboutTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    // add about next timeline
    setNextTimeline(
      [
        [aboutTimeline, false, 'about-2', aboutScroll, aboutScroll2, false, false, false, 1000, -1, true],
        [aboutTimeline2, false, 'about-3', aboutScroll2, aboutScroll3, false, false, false, 2000, -1, true],
        [aboutTimeline3, false, 'about-4', aboutScroll3, aboutScroll4, false, false, false, 3000, -1, true]
      ],
      [
        [
          [{
            targets: '#about-1 h1 strong',
            opacity: [0, 1],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg',
            scale: [0.125, 0.2],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path',
            strokeWidth: [20, 15],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path.middle-ring',
            strokeWidth: [40, 30],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#about-1',
            opacity: [1, 0],
            duration: 1000,
          }, 0],
          [{
            targets: '#about-2, #about-2 p, #about-2 h2',
            opacity: [0, 1],
            duration: 1000,
          }, 1000],
          [{
            targets: '.about-circle svg',
            scale: [0.2, 0.4],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path',
            strokeWidth: [15, 10],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path.middle-ring',
            strokeWidth: [30, 20],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path.user-icon-fill',
            fill: '#071738',
            easing: 'linear',
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#about-2',
            translateY: ['-50%', '-50%'],
            translateX: ['0', '-100vw'],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle',
            translateX: ['0', '-50vw'],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg',
            scale: [0.4, 1],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path',
            strokeWidth: [10, 3],
            duration: 1000,
          }, 0],
          [{
            targets: '.about-circle svg path.middle-ring',
            strokeWidth: [20, 6],
            duration: 1000,
          }, 0],
          [{
            targets: '#about-3, #about-3 .first p',
            opacity: [0, 1],
            duration: 1000,
          }, 1000],
          [{
            targets: '#about-3 .large p, #about-3 .btn',
            opacity: [0, 1],
            duration: 1000,
          }, 2000]
        ]
      ]
    );

    function aboutScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end about
        console.log('end about');
      } else {
        playAnimation(aboutTimeline3, true, 'about-3', aboutScroll4, aboutScroll3, false, false, false, 3000, -1, true);
      }
    }

    function aboutScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!aboutTimeline3.children.length) {
          aboutTimeline3.add({
            targets: '#about-2',
            translateY: ['-50%', '-50%'],
            translateX: ['0', '-100vw'],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle',
            translateX: ['0', '-50vw'],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg',
            scale: [0.4, 1],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path',
            strokeWidth: [10, 3],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path.middle-ring',
            strokeWidth: [20, 6],
            duration: 1000,
          }, 0).add({
            targets: '#about-3, #about-3 .first p',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#about-3 .large p, #about-3 .btn',
            opacity: [0, 1],
            duration: 1000,
          }, 2000);
        }

        playAnimation(aboutTimeline3, false, 'about-4', aboutScroll3, aboutScroll4, false, false, false, 3000, -1, true);
      } else {
        playAnimation(aboutTimeline2, true, 'about-2', aboutScroll3, aboutScroll2, false, false, false, 2000, -1, true);
      }
    }

    function aboutScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!aboutTimeline2.children.length) {
          aboutTimeline2.add({
            targets: '#about-1',
            opacity: [1, 0],
            duration: 1000,
          }, 0).add({
            targets: '#about-2, #about-2 p, #about-2 h2',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '.about-circle svg',
            scale: [0.2, 0.4],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path',
            strokeWidth: [15, 10],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path.middle-ring',
            strokeWidth: [30, 20],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path.user-icon-fill',
            fill: '#071738',
            easing: 'linear',
            duration: 1000,
          }, 0);
        }

        playAnimation(aboutTimeline2, false, 'about-3', aboutScroll2, aboutScroll3, false, false, false, 2000, -1, true);
      } else {
        playAnimation(aboutTimeline, true, 'about-1', aboutScroll2, aboutScroll, false, false, false, 1000, -1, true);
      }
    }

    function aboutScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!aboutTimeline.children.length) {
          aboutTimeline.add({
            targets: '#about-1 h1 strong',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg',
            scale: [0.125, 0.2],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path',
            strokeWidth: [20, 15],
            duration: 1000,
          }, 0).add({
            targets: '.about-circle svg path.middle-ring',
            strokeWidth: [40, 30],
            duration: 1000,
          }, 0);
        }

        playAnimation(aboutTimeline, false, 'about-2', aboutScroll, aboutScroll2, false, false, false, 1000, -1, true);
      }
    }

    window.addEventListener('wheel', aboutScroll);
    window.addEventListener('touchmove', aboutScroll);
  }

  const isWebinar = document.querySelector('#webinar-1');

  if (isWebinar) {
    // Webinar functions
    route.current = '/webinar';

    // fetch webinars
    const table = document.querySelector('.webinar-table');
    const localFeed = '/wp-content/themes/makosi/json/webinars.json';
    const feed = (table.dataset.feed) ? table.dataset.feed : localFeed;
    const webinarsUrl = (location.hostname === 'makosi.local' || location.hostname === 'localhost' || location.hostname === 'makosi.savyclients.com') ? localFeed : feed;

    getFeed(webinarsUrl).then(data => {
      if (data) {
        const table = document.querySelector('.webinar-table table tbody');
        table.innerHTML = '';

        data.slice(0, 5).map(webinar => {
          const date = moment(webinar.start_time);
          const dateFormatted = date.format('MM.DD.YYYY');
          const tz = (Intl.DateTimeFormat().resolvedOptions().timeZone) ? Intl.DateTimeFormat().resolvedOptions().timeZone : webinar.timezone;
          const timezone = date.tz(tz).format('h:mm a z');

          table.innerHTML += `
              <tr>
                <td><strong>${dateFormatted}</strong> | ${timezone}</td>
                <td>${webinar.topic}</td>
                <td><a href="${webinar.join_url}" target="_blank">sign up</a></td>
              </tr>
            `;
        });
      }
    });

    let webinarTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let webinarTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let webinarTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    // add webinar next timeline
    setNextTimeline(
      [
        [webinarTimeline, false, 'webinar-2', webinarScroll, webinarScroll2, false, false, false, 1000, -1, true],
        [webinarTimeline2, false, 'webinar-3', webinarScroll2, webinarScroll3, false, false, false, 1000, -1, true],
        [webinarTimeline3, false, '/apply', webinarScroll3, webinarScroll4, false, false, false, 1000, -1, true]
      ],
      [
        [
          [{
            targets: '#webinar-1',
            translateX: ['0', '100vw'],
            duration: 1000,
          }, 0],
          [{
            targets: '.stroke-blob',
            translateX: ['0', '125vw'],
            duration: 1000,
          }, 0],
          [{
            targets: '#webinar-2',
            translateX: ['-100vw', '0'],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#webinar-3',
            translateX: ['0', '100vw'],
            duration: 1000,
          }, 0],
          [{
            targets: '.stroke-blob',
            top: [-830, -1280],
            duration: 1000,
          }, 0],
          [{
            targets: '#webinar-4',
            translateX: ['-100vw', '0'],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#webinar-main',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '.stroke-blob',
            opacity: [1, 0],
            duration: 1000,
          }, 0],
          [{
            targets: '.top-blob',
            translateY: ['100vh', '0'],
            duration: 1000,
          }, 0],
          [{
            targets: '#application-1',
            translateY: ['100%', '0'],
            duration: 1000,
          }, 0]
        ]
      ]
    );

    function webinarScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end webinar
        console.log('end webinar');
      } else {
        playAnimation(webinarTimeline3, true, 'webinar-3', webinarScroll4, webinarScroll3, false, false, false, 1000, -1, true);
      }
    }

    function webinarScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!webinarTimeline3.children.length) {
          webinarTimeline3.add({
            targets: '#webinar-main',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '.stroke-blob',
            opacity: [1, 0],
            duration: 1000,
          }, 0).add({
            targets: '.top-blob',
            translateY: ['100vh', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#application-1',
            translateY: ['100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(webinarTimeline3, false, '/apply', webinarScroll3, webinarScroll4, false, false, false, 1000, -1, true);
      } else {
        playAnimation(webinarTimeline2, true, 'webinar-2', webinarScroll3, webinarScroll2, false, false, false, 1000, -1, true);
      }
    }

    function webinarScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!webinarTimeline2.children.length) {
          webinarTimeline2.add({
            targets: '#webinar-3',
            translateX: ['0', '100vw'],
            duration: 1000,
          }, 0).add({
            targets: '.stroke-blob',
            top: [-830, -1280],
            duration: 1000,
          }, 0).add({
            targets: '#webinar-4',
            translateX: ['-100vw', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(webinarTimeline2, false, 'webinar-3', webinarScroll2, webinarScroll3, false, false, false, 1000, -1, true);
      } else {
        playAnimation(webinarTimeline, true, 'webinar-1', webinarScroll2, webinarScroll, false, false, false, 1000, -1, true);
      }
    }

    function webinarScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!webinarTimeline.children.length) {
          webinarTimeline.add({
            targets: '#webinar-1',
            translateX: ['0', '100vw'],
            duration: 1000,
          }, 0).add({
            targets: '.stroke-blob',
            translateX: ['0', '125vw'],
            duration: 1000,
          }, 0).add({
            targets: '#webinar-2',
            translateX: ['-100vw', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(webinarTimeline, false, 'webinar-2', webinarScroll, webinarScroll2, false, false, false, 1000, -1, true);
      }
    }

    window.addEventListener('wheel', webinarScroll);
    window.addEventListener('touchmove', webinarScroll);
  }

  const isAudit = document.querySelector('.audit-main');

  if (isAudit) {
    // Audit functions
    route.current = '/audit';

    // Audit Clients
    const auditClientsButton = document.querySelector('#audit-clients');
    let auditClientsTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline7 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    if (!auditClientsTimeline2.children.length) {
      auditClientsTimeline2.add({
        targets: '.welcome-content',
        translateY: ['0', '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section, .why-content',
        translateY: ['100%', '0'],
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.why-content .section-title',
        translateY: ['100%', '0'],
        rotate: ['-90deg', '-90deg'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section .fact-0',
        opacity: [0, 1],
        duration: 1000,
      }, 1000).add({
        targets: '.why-section .fact-1',
        opacity: [0, 1],
        duration: 1000,
      }, 2000).add({
        targets: '.why-section .fact-2',
        opacity: [0, 1],
        duration: 1000,
      }, 3000);
    }

    function animateAuditClients(event) {
      if (auditClientsButton) {
        event.preventDefault();
      }

      if (!auditClientsTimeline.children.length) {
        auditClientsTimeline.add({
          targets: '.top-blob, #audit-clients-1',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditClientsTimeline, false, '/audit/clients', false, auditClientsScroll, auditClientsButton, false, false, 1000, -1, true);

      // set audit clients next timeline
      setNextTimeline(
        [
          [auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
          [auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
          [auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
          [auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
          [auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
          [auditClientsTimeline7, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
        ],
        [
          [
            [
              {
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ]
          ],
          [
            [
              {
                targets: '.top-blob',
                top: ['0', '100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-2',
                translateY: ['-100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-2 .side-title',
                top: ['150%', '50%'],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts h2',
                opacity: [0, 0.25],
                borderBottom: '1px solid #fff',
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-0 p',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-1 p',
                opacity: [0, 1],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-2 p',
                opacity: [0, 1],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              6000
            ],
            [
              {
                targets: '#audit-clients-2 .audit-facts .fact-3 p',
                opacity: [0, 1],
                duration: 1000,
              },
              6000
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.left-blob',
                translateX: ['100%', '-50%'],
                duration: 1500,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-3',
                translateX: ['100%', '0'],
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1000,
              },
              500
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-3',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-3',
                translateX: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-4',
                translateX: ['100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-4 .first-title',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-4 .second-title',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#audit-clients-4 .third-title',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#audit-clients-4 p',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#audit-clients-4 .industries',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '.lines-single .top-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .bot-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-1 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-2 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-3 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ]
          ],
          [
            [
              {
                targets: '#audit-clients-5',
                translateY: ['-100%', '0'],
                duration: 1000,
              }, 0
            ]
          ],
          [
            [
              {
                targets: '.new-blog',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.new-blog',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ]
          ],
        ],
        true
      );
    }

    function auditClientsScroll7(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end of audit clients
        console.log('end audit clients animations');
      } else {
        console.log('no scrolling events inside blog');
      }
    }

    function auditClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline7.children.length) {
          auditClientsTimeline7.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditClientsTimeline7, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(auditClientsTimeline6, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
      }
    }

    function auditClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline6.children.length) {
          auditClientsTimeline6.add({
            targets: '#audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
      } else {
        playAnimation(auditClientsTimeline5, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
      }
    }

    function auditClientsScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline5.children.length) {
          auditClientsTimeline5.add({
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0);
        }

        playAnimation(auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
      } else {
        playAnimation(auditClientsTimeline4, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
      }
    }

    function auditClientsScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline4.children.length) {
          auditClientsTimeline4.add({
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          }, 0).add({
            targets: '#audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 500);
        }

        playAnimation(auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
      } else {
        playAnimation(auditClientsTimeline3, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
      }
    }

    function auditClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline3.children.length) {
          auditClientsTimeline3.add({
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 3000).add({
            targets: '#audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '#audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000);
        }

        playAnimation(auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
      } else {
        playAnimation(auditClientsTimeline2, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
      }
    }

    function auditClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
      } else {
        playAnimation(auditClientsTimeline, true, '/audit', auditClientsScroll, false, auditClientsButton, false, false, 1000);
      }
    }

    if (auditClientsButton) {
      auditClientsButton.addEventListener('click', animateAuditClients);
    }

    // Audit Candidates
    let auditCandidatesTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    const auditCandidatesButton = document.querySelector('#audit-candidates');

    function auditCandidatesScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end audit candidates
        console.log('end audit candidates');
      } else {
        playAnimation(auditCandidatesTimeline5, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
      }
    }

    function auditCandidatesScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline5.children.length) {
          auditCandidatesTimeline5.add({
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000).add({
            targets: '#audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000);
        }

        playAnimation(auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline4, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
      }
    }

    function auditCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline4.children.length) {
          auditCandidatesTimeline4.add({
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
            duration: 2000,
          }, 0);
        }

        playAnimation(auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline3, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
      }
    }

    function auditCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline3.children.length) {
          auditCandidatesTimeline3.add({
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline2, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
      }
    }

    function auditCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline2.children.length) {
          auditCandidatesTimeline2.add({
            targets: '#audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000);
        }

        playAnimation(auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline, true, '/audit', auditCandidatesScroll, false, auditCandidatesButton, false, false, 2000);
      }
    }

    function animateCandidates(event) {
      if (auditCandidatesButton) {
        event.preventDefault();
      }

      if (!auditCandidatesTimeline.children.length) {
        auditCandidatesTimeline.add({
          targets: '.top-blob, #audit-candidates-1',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1-2',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000);
      }

      playAnimation(auditCandidatesTimeline, false, '/audit/applicants', false, auditCandidatesScroll, auditCandidatesButton, false, false, 2000, -1, true);

      // set audit candidates next timeline
      setNextTimeline(
        [
          [auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
          [auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
          [auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
          [auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
        ],
        [
          [
            [{
              targets: '#audit-candidates-1-1',
              translateY: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#audit-candidates-1-2',
              translateY: ['0', '100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#audit-candidates-1-3',
              translateX: ['-100%', '0'],
              duration: 1000,
            }, 1000]
          ],
          [
            [{
              targets: '.top-blob',
              top: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#audit-candidates-1',
              translateY: ['0', '100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#audit-candidates-2',
              translateY: ['-100%', '0'],
              duration: 1000,
            }, 0]
          ],
          [
            [{
              targets: '#audit-candidates-2 .slide-title-top',
              translateX: ['-100%', '0'],
              duration: 2000,
            }, 0],
            [{
              targets: '#audit-candidates-2 .slide-title-bottom',
              translateX: ['100%', '0'],
              duration: 2000,
            }, 0],
            [{
              targets: '#audit-candidates-2 .slide-title',
              background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
              duration: 2000,
            }, 0]
          ],
          [
            [{
              targets: '#audit-candidates-2 .slide-title-top',
              translateX: ['0', '100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#audit-candidates-2 .slide-title-bottom',
              translateX: ['0', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#audit-candidates-3',
              opacity: [0, 1],
              zIndex: {
                value: [1, 3],
                round: true
              },
              duration: 1,
            }, 1000],
            [{
              targets: '#audit-candidates-3 .before',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 1000],
            [{
              targets: '#audit-candidates-3 .after',
              translateX: ['0', '100%'],
              duration: 1000,
            }, 1000],
            [{
              targets: '#audit-candidates-3 .call-to-action',
              opacity: ['0', '1'],
              duration: 1000,
            }, 2000]
          ]
        ],
        true
      );
    }

    if (auditCandidatesButton) {
      auditCandidatesButton.addEventListener('click', animateCandidates);
    }
  }

  // new main
  const isItAudit = document.querySelector('.it-audit-main');

  if (isItAudit) {
    // itAudit functions
    route.current = '/it-audit';

    // itAudit Clients
    const itAuditClientsButton = document.querySelector('#it-audit-clients');
    let itAuditClientsTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline7 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    if (!itAuditClientsTimeline2.children.length) {
      itAuditClientsTimeline2.add({
        targets: '.welcome-content',
        translateY: ['0', '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section, .why-content',
        translateY: ['100%', '0'],
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.why-content .section-title',
        translateY: ['100%', '0'],
        rotate: ['-90deg', '-90deg'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section .fact-0',
        opacity: [0, 1],
        duration: 1000,
      }, 1000).add({
        targets: '.why-section .fact-1',
        opacity: [0, 1],
        duration: 1000,
      }, 2000).add({
        targets: '.why-section .fact-2',
        opacity: [0, 1],
        duration: 1000,
      }, 3000);
    }

    function animateItAuditClients(event) {
      if (itAuditClientsButton) {
        event.preventDefault();
      }

      if (!itAuditClientsTimeline.children.length) {
        itAuditClientsTimeline.add({
          targets: '.top-blob, #it-audit-clients-1',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditClientsTimeline, false, '/it-audit/clients', false, itAuditClientsScroll, itAuditClientsButton, false, false, 1000, -1, true);

      // set audit clients next timeline
      setNextTimeline(
        [
          [itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
          [itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
          [itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
          [itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
          [itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
          [itAuditClientsTimeline7, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
        ],
        [
          [
            [
              {
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ]
          ],
          [
            [
              {
                targets: '.top-blob',
                top: ['0', '100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-2',
                translateY: ['-100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-2 .side-title',
                top: ['150%', '50%'],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts h2',
                opacity: [0, 0.25],
                borderBottom: '1px solid #fff',
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                opacity: [0, 1],
                duration: 1000,
              },
              4000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                opacity: [0, 1],
                duration: 1000,
              },
              5000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                opacity: [0.25, 1],
                borderColor: ['#fff', '#16e7cf'],
                borderWidth: [1, 3],
                duration: 1000,
              },
              6000
            ],
            [
              {
                targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                opacity: [0, 1],
                duration: 1000,
              },
              6000
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.left-blob',
                translateX: ['100%', '-50%'],
                duration: 1500,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-3',
                translateX: ['100%', '0'],
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1000,
              },
              500
            ]
          ],
          [
            [
              {
                targets: '.left-blob',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-3',
                zIndex: {
                  value: [3, 2],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-3',
                translateX: ['0', '-100%'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-4',
                translateX: ['100%', '0'],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-4 .first-title',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-4 .second-title',
                opacity: [0, 1],
                duration: 1000,
              },
              1000
            ],
            [
              {
                targets: '#it-audit-clients-4 .third-title',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#it-audit-clients-4 p',
                opacity: [0, 1],
                duration: 1000,
              },
              2000
            ],
            [
              {
                targets: '#it-audit-clients-4 .industries',
                opacity: [0, 1],
                duration: 1000,
              },
              3000
            ],
            [
              {
                targets: '.lines-single .top-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .bot-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-1 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-2 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ],
            [
              {
                targets: '.lines-single .hor-line-3 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250 },
                duration: 3000,
              },
              0
            ]
          ],
          [
            [
              {
                targets: '#it-audit-clients-5',
                translateY: ['-100%', '0'],
                duration: 1000,
              }, 0
            ]
          ],
          [
            [
              {
                targets: '.new-blog',
                zIndex: {
                  value: [2, 3],
                  round: true
                },
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [
              {
                targets: '.new-blog',
                opacity: [0, 1],
                duration: 1000,
              },
              0
            ]
          ],
        ],
        true
      );
    }

    function itAuditClientsScroll7(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end of audit clients
        console.log('end audit clients animations');
      } else {
        console.log('no scrolling events inside blog');
      }
    }

    function itAuditClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline7.children.length) {
          itAuditClientsTimeline7.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline7, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(itAuditClientsTimeline6, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
      }
    }

    function itAuditClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline6.children.length) {
          itAuditClientsTimeline6.add({
            targets: '#it-audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline5, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
      }
    }

    function itAuditClientsScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline5.children.length) {
          itAuditClientsTimeline5.add({
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline4, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
      }
    }

    function itAuditClientsScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline4.children.length) {
          itAuditClientsTimeline4.add({
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          }, 0).add({
            targets: '#it-audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 500);
        }

        playAnimation(itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline3, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
      }
    }

    function itAuditClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline3.children.length) {
          itAuditClientsTimeline3.add({
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 3000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000);
        }

        playAnimation(itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline2, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
      }
    }

    function itAuditClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline, true, '/it-audit', itAuditClientsScroll, false, itAuditClientsButton, false, false, 1000);
      }
    }

    if (itAuditClientsButton) {
      itAuditClientsButton.addEventListener('click', animateItAuditClients);
    }

    // itAudit Candidates
    let itAuditCandidatesTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    const itAuditCandidatesButton = document.querySelector('#it-audit-candidates');

    function itAuditCandidatesScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end audit candidates
        console.log('end audit candidates');
      } else {
        playAnimation(itAuditCandidatesTimeline5, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
      }
    }

    function itAuditCandidatesScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline5.children.length) {
          itAuditCandidatesTimeline5.add({
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000);
        }

        playAnimation(itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline4, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
      }
    }

    function itAuditCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline4.children.length) {
          itAuditCandidatesTimeline4.add({
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
            duration: 2000,
          }, 0);
        }

        playAnimation(itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline3, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
      }
    }

    function itAuditCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline3.children.length) {
          itAuditCandidatesTimeline3.add({
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline2, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
      }
    }

    function itAuditCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline2.children.length) {
          itAuditCandidatesTimeline2.add({
            targets: '#it-audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000);
        }

        playAnimation(itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline, true, '/it-audit', itAuditCandidatesScroll, false, itAuditCandidatesButton, false, false, 2000);
      }
    }

    function animateItCandidates(event) {
      if (itAuditCandidatesButton) {
        event.preventDefault();
      }

      if (!itAuditCandidatesTimeline.children.length) {
        itAuditCandidatesTimeline.add({
          targets: '.top-blob, #it-audit-candidates-1',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-candidates-1-2',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000);
      }

      playAnimation(itAuditCandidatesTimeline, false, '/it-audit/applicants', false, itAuditCandidatesScroll, itAuditCandidatesButton, false, false, 2000, -1, true);

      // set audit candidates next timeline
      setNextTimeline(
        [
          [itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
          [itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
          [itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
          [itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
        ],
        [
          [
            [{
              targets: '#it-audit-candidates-1-1',
              translateY: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#it-audit-candidates-1-2',
              translateY: ['0', '100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#it-audit-candidates-1-3',
              translateX: ['-100%', '0'],
              duration: 1000,
            }, 1000]
          ],
          [
            [{
              targets: '.top-blob',
              top: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#it-audit-candidates-1',
              translateY: ['0', '100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#it-audit-candidates-2',
              translateY: ['-100%', '0'],
              duration: 1000,
            }, 0]
          ],
          [
            [{
              targets: '#it-audit-candidates-2 .slide-title-top',
              translateX: ['-100%', '0'],
              duration: 2000,
            }, 0],
            [{
              targets: '#it-audit-candidates-2 .slide-title-bottom',
              translateX: ['100%', '0'],
              duration: 2000,
            }, 0],
            [{
              targets: '#it-audit-candidates-2 .slide-title',
              background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
              duration: 2000,
            }, 0]
          ],
          [
            [{
              targets: '#it-audit-candidates-2 .slide-title-top',
              translateX: ['0', '100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#it-audit-candidates-2 .slide-title-bottom',
              translateX: ['0', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#it-audit-candidates-3',
              opacity: [0, 1],
              zIndex: {
                value: [1, 3],
                round: true
              },
              duration: 1,
            }, 1000],
            [{
              targets: '#it-audit-candidates-3 .before',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 1000],
            [{
              targets: '#it-audit-candidates-3 .after',
              translateX: ['0', '100%'],
              duration: 1000,
            }, 1000],
            [{
              targets: '#it-audit-candidates-3 .call-to-action',
              opacity: ['0', '1'],
              duration: 1000,
            }, 2000]
          ]
        ],
        true
      );
    }

    if (itAuditCandidatesButton) {
      itAuditCandidatesButton.addEventListener('click', animateItCandidates);
    }
  }
  // end new main

  const isAuditClients = document.querySelector('.audit-clients-main');

  if (isAuditClients) {
    // Audit Clients
    route.current = '/audit/clients';

    let auditClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditClientsTimeline7 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    if (!auditClientsTimeline2.children.length) {
      auditClientsTimeline2.add({
        targets: '.welcome-content',
        translateY: ['0', '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section, .why-content',
        translateY: ['100%', '0'],
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.why-content .section-title',
        translateY: ['100%', '0'],
        rotate: ['-90deg', '-90deg'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section .fact-0',
        opacity: [0, 1],
        duration: 1000,
      }, 1000).add({
          targets: '.why-section .fact-1',
        opacity: [0, 1],
        duration: 1000,
      }, 2000).add({
          targets: '.why-section .fact-2',
        opacity: [0, 1],
        duration: 1000,
      }, 3000);
    }

    // set audit clients next timeline
    setNextTimeline(
      [
        [auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
        [auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
        [auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
        [auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
        [auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
        [auditClientsTimeline7, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
      ],
      [
        [
          [
            {
              targets: '.welcome-content',
              translateY: ['0', '-100%'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-section, .why-content',
              translateY: ['100%', '0'],
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-content .section-title',
              translateY: ['100%', '0'],
              rotate: ['-90deg', '-90deg'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-section, .why-content',
              translateY: ['100%', '0'],
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-content .section-title',
              translateY: ['100%', '0'],
              rotate: ['-90deg', '-90deg'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-section .fact-0',
              opacity: [0, 1],
              duration: 1000,
            },
            1000
          ],
          [
            {
              targets: '.why-section .fact-1',
              opacity: [0, 1],
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '.why-section .fact-2',
              opacity: [0, 1],
              duration: 1000,
            },
            3000
          ]
        ],
        [
          [
            {
              targets: '.top-blob',
              top: ['0', '100%'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-2',
              translateY: ['-100%', '0'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-2 .side-title',
              top: ['150%', '50%'],
              duration: 1000,
            },
            1000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts h2',
              opacity: [0, 0.25],
              borderBottom: '1px solid #fff',
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-0 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            3000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-0 p',
              opacity: [0, 1],
              duration: 1000,
            },
            3000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-1 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            4000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-1 p',
              opacity: [0, 1],
              duration: 1000,
            },
            4000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-2 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            5000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-2 p',
              opacity: [0, 1],
              duration: 1000,
            },
            5000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-3 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            6000
          ],
          [
            {
              targets: '#audit-clients-2 .audit-facts .fact-3 p',
              opacity: [0, 1],
              duration: 1000,
            },
            6000
          ]
        ],
        [
          [
            {
              targets: '.left-blob',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '.left-blob',
              translateX: ['100%', '-50%'],
              duration: 1500,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-3',
              translateX: ['100%', '0'],
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1000,
            },
            500
          ]
        ],
        [
          [
            {
              targets: '.left-blob',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-3',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-3',
              translateX: ['0', '-100%'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-4',
              translateX: ['100%', '0'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-4 .first-title',
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-4 .second-title',
              opacity: [0, 1],
              duration: 1000,
            },
            1000
          ],
          [
            {
              targets: '#audit-clients-4 .third-title',
              opacity: [0, 1],
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '#audit-clients-4 p',
              opacity: [0, 1],
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '#audit-clients-4 .industries',
              opacity: [0, 1],
              duration: 1000,
            },
            3000
          ],
          [
            {
              targets: '.lines-single .top-line path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .bot-line path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .hor-line-1 path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .hor-line-2 path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .hor-line-3 path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ]
        ],
        [
          [
            {
              targets: '#audit-clients-5',
              translateY: ['-100%', '0'],
              duration: 1000,
            }, 0
          ]
        ],
        [
          [
            {
              targets: '.new-blog',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
              opacity: [1, 0],
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '.new-blog',
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ]
        ],
      ]
    );

    function auditClientsScroll7(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end of audit clients
        console.log('end audit clients animations');
      } else {
        console.log('no scrolling events inside blog');
      }
    }

    function auditClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline7.children.length) {
          auditClientsTimeline7.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditClientsTimeline7, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(auditClientsTimeline6, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
      }
    }

    function auditClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline6.children.length) {
          auditClientsTimeline6.add({
            targets: '#audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
      } else {
        playAnimation(auditClientsTimeline5, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
      }
    }

    function auditClientsScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline5.children.length) {
          auditClientsTimeline5.add({
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0);
        }

        playAnimation(auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
      } else {
        playAnimation(auditClientsTimeline4, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
      }
    }

    function auditClientsScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline4.children.length) {
          auditClientsTimeline4.add({
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          }, 0).add({
            targets: '#audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 500);
        }

        playAnimation(auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
      } else {
        playAnimation(auditClientsTimeline3, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
      }
    }

    function auditClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditClientsTimeline3.children.length) {
          auditClientsTimeline3.add({
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 2000).add({
            targets: '#audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 3000).add({
            targets: '#audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '#audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000);
        }

        playAnimation(auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
      } else {
        playAnimation(auditClientsTimeline2, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
      }
    }

    function auditClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
      }
    }

    window.addEventListener('wheel', auditClientsScroll);
    window.addEventListener('touchmove', auditClientsScroll);
  }

  // new clients
  const isItAuditClients = document.querySelector('.it-audit-clients-main');

  if (isItAuditClients) {
    // Audit Clients
    route.current = '/it-audit/clients';

    let itAuditClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditClientsTimeline7 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    if (!itAuditClientsTimeline2.children.length) {
      itAuditClientsTimeline2.add({
        targets: '.welcome-content',
        translateY: ['0', '-100%'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section, .why-content',
        translateY: ['100%', '0'],
        opacity: [0, 1],
        duration: 1000,
      }, 0).add({
        targets: '.why-content .section-title',
        translateY: ['100%', '0'],
        rotate: ['-90deg', '-90deg'],
        duration: 1000,
      }, 0).add({
        targets: '.why-section .fact-0',
        opacity: [0, 1],
        duration: 1000,
      }, 1000).add({
          targets: '.why-section .fact-1',
        opacity: [0, 1],
        duration: 1000,
      }, 2000).add({
          targets: '.why-section .fact-2',
        opacity: [0, 1],
        duration: 1000,
      }, 3000);
    }

    // set audit clients next timeline
    setNextTimeline(
      [
        [itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
        [itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
        [itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
        [itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
        [itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
        [itAuditClientsTimeline7, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
      ],
      [
        [
          [
            {
              targets: '.welcome-content',
              translateY: ['0', '-100%'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-section, .why-content',
              translateY: ['100%', '0'],
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-content .section-title',
              translateY: ['100%', '0'],
              rotate: ['-90deg', '-90deg'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-section, .why-content',
              translateY: ['100%', '0'],
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-content .section-title',
              translateY: ['100%', '0'],
              rotate: ['-90deg', '-90deg'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '.why-section .fact-0',
              opacity: [0, 1],
              duration: 1000,
            },
            1000
          ],
          [
            {
              targets: '.why-section .fact-1',
              opacity: [0, 1],
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '.why-section .fact-2',
              opacity: [0, 1],
              duration: 1000,
            },
            3000
          ]
        ],
        [
          [
            {
              targets: '.top-blob',
              top: ['0', '100%'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-2',
              translateY: ['-100%', '0'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-2 .side-title',
              top: ['150%', '50%'],
              duration: 1000,
            },
            1000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts h2',
              opacity: [0, 0.25],
              borderBottom: '1px solid #fff',
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            3000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
              opacity: [0, 1],
              duration: 1000,
            },
            3000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            4000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
              opacity: [0, 1],
              duration: 1000,
            },
            4000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            5000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
              opacity: [0, 1],
              duration: 1000,
            },
            5000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            },
            6000
          ],
          [
            {
              targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
              opacity: [0, 1],
              duration: 1000,
            },
            6000
          ]
        ],
        [
          [
            {
              targets: '.left-blob',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '.left-blob',
              translateX: ['100%', '-50%'],
              duration: 1500,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-3',
              translateX: ['100%', '0'],
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1000,
            },
            500
          ]
        ],
        [
          [
            {
              targets: '.left-blob',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-3',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-3',
              translateX: ['0', '-100%'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-4',
              translateX: ['100%', '0'],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-4 .first-title',
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-4 .second-title',
              opacity: [0, 1],
              duration: 1000,
            },
            1000
          ],
          [
            {
              targets: '#it-audit-clients-4 .third-title',
              opacity: [0, 1],
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '#it-audit-clients-4 p',
              opacity: [0, 1],
              duration: 1000,
            },
            2000
          ],
          [
            {
              targets: '#it-audit-clients-4 .industries',
              opacity: [0, 1],
              duration: 1000,
            },
            3000
          ],
          [
            {
              targets: '.lines-single .top-line path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .bot-line path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .hor-line-1 path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .hor-line-2 path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ],
          [
            {
              targets: '.lines-single .hor-line-3 path',
              strokeDashoffset: [anime.setDashoffset, 0],
              delay: function (el, i) { return i * 250 },
              duration: 3000,
            },
            0
          ]
        ],
        [
          [
            {
              targets: '#it-audit-clients-5',
              translateY: ['-100%', '0'],
              duration: 1000,
            }, 0
          ]
        ],
        [
          [
            {
              targets: '.new-blog',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
              opacity: [1, 0],
              duration: 1,
            },
            0
          ],
          [
            {
              targets: '.new-blog',
              opacity: [0, 1],
              duration: 1000,
            },
            0
          ]
        ],
      ]
    );

    function itAuditClientsScroll7(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end of audit clients
        console.log('end audit clients animations');
      } else {
        console.log('no scrolling events inside blog');
      }
    }

    function itAuditClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline7.children.length) {
          itAuditClientsTimeline7.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline7, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(itAuditClientsTimeline6, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
      }
    }

    function itAuditClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline6.children.length) {
          itAuditClientsTimeline6.add({
            targets: '#it-audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline5, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
      }
    }

    function itAuditClientsScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline5.children.length) {
          itAuditClientsTimeline5.add({
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#it-audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0).add({
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          }, 0);
        }

        playAnimation(itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline4, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
      }
    }

    function itAuditClientsScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline4.children.length) {
          itAuditClientsTimeline4.add({
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          }, 0).add({
            targets: '#it-audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 500);
        }

        playAnimation(itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline3, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
      }
    }

    function itAuditClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditClientsTimeline3.children.length) {
          itAuditClientsTimeline3.add({
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 2000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 3000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 3000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000);
        }

        playAnimation(itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
      } else {
        playAnimation(itAuditClientsTimeline2, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
      }
    }

    function itAuditClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        playAnimation(itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
      }
    }

    window.addEventListener('wheel', itAuditClientsScroll);
    window.addEventListener('touchmove', itAuditClientsScroll);
  }

  // end new clients

  const isAuditCandidates = document.querySelector('.audit-candidates-main');

  if (isAuditCandidates) {
    // Audit Candidates
    route.current = '/audit/applicants';

    let auditCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let auditCandidatesTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    const auditCandidatesButton = document.querySelector('#audit-candidates');

    // set audit candidates next timeline
    setNextTimeline(
      [
        [auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
        [auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
        [auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
        [auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
      ],
      [
        [
          [{
            targets: '#audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000]
        ],
        [
          [{
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
            duration: 2000,
          }, 0]
        ],
        [
          [{
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000],
          [{
            targets: '#audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000]
        ]
      ]
    );

    function auditCandidatesScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end audit candidates
        console.log('end audit candidates');
      } else {
        playAnimation(auditCandidatesTimeline5, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
      }
    }

    function auditCandidatesScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline5.children.length) {
          auditCandidatesTimeline5.add({
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000).add({
            targets: '#audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000);
        }

        playAnimation(auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline4, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
      }
    }

    function auditCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline4.children.length) {
          auditCandidatesTimeline4.add({
            targets: '#audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
            duration: 2000,
          }, 0);
        }

        playAnimation(auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline3, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
      }
    }

    function auditCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline3.children.length) {
          auditCandidatesTimeline3.add({
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
      } else {
        playAnimation(auditCandidatesTimeline2, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
      }
    }

    function auditCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!auditCandidatesTimeline2.children.length) {
          auditCandidatesTimeline2.add({
            targets: '#audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000);
        }

        playAnimation(auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
      }
    }

    window.addEventListener('wheel', auditCandidatesScroll);
    window.addEventListener('touchmove', auditCandidatesScroll);
  }

  // new candidates
  const isItAuditCandidates = document.querySelector('.it-audit-candidates-main');

  if (isItAuditCandidates) {
    // Audit Candidates
    route.current = '/it-audit/applicants';

    let itAuditCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline4 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let itAuditCandidatesTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    const itAuditCandidatesButton = document.querySelector('#it-audit-candidates');

    // set audit candidates next timeline
    setNextTimeline(
      [
        [itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
        [itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
        [itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
        [itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
      ],
      [
        [
          [{
            targets: '#it-audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000]
        ],
        [
          [{
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0]
        ],
        [
          [{
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
            duration: 2000,
          }, 0]
        ],
        [
          [{
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#it-audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000],
          [{
            targets: '#it-audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#it-audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000],
          [{
            targets: '#it-audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000]
        ]
      ]
    );

    function itAuditCandidatesScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end audit candidates
        console.log('end audit candidates');
      } else {
        playAnimation(itAuditCandidatesTimeline5, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
      }
    }

    function itAuditCandidatesScroll4(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline5.children.length) {
          itAuditCandidatesTimeline5.add({
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['0', '100%'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-3',
            opacity: [0, 1],
            zIndex: {
              value: [1, 3],
              round: true
            },
            duration: 1,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .before',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .after',
            translateX: ['0', '100%'],
            duration: 1000,
          }, 1000).add({
            targets: '#it-audit-candidates-3 .call-to-action',
            opacity: ['0', '1'],
            duration: 1000,
          }, 2000);
        }

        playAnimation(itAuditCandidatesTimeline5, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline4, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
      }
    }

    function itAuditCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline4.children.length) {
          itAuditCandidatesTimeline4.add({
            targets: '#it-audit-candidates-2 .slide-title-top',
            translateX: ['-100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title-bottom',
            translateX: ['100%', '0'],
            duration: 2000,
          }, 0).add({
            targets: '#it-audit-candidates-2 .slide-title',
            background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
            duration: 2000,
          }, 0);
        }

        playAnimation(itAuditCandidatesTimeline4, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline3, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
      }
    }

    function itAuditCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline3.children.length) {
          itAuditCandidatesTimeline3.add({
            targets: '.top-blob',
            top: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-1',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0);
        }

        playAnimation(itAuditCandidatesTimeline3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
      } else {
        playAnimation(itAuditCandidatesTimeline2, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
      }
    }

    function itAuditCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!itAuditCandidatesTimeline2.children.length) {
          itAuditCandidatesTimeline2.add({
            targets: '#it-audit-candidates-1-1',
            translateY: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-1-2',
            translateY: ['0', '100%'],
            duration: 1000,
          }, 0).add({
            targets: '#it-audit-candidates-1-3',
            translateX: ['-100%', '0'],
            duration: 1000,
          }, 1000);
        }

        playAnimation(itAuditCandidatesTimeline2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
      }
    }

    window.addEventListener('wheel', itAuditCandidatesScroll);
    window.addEventListener('touchmove', itAuditCandidatesScroll);
  }
  //end new candidates

  const isTax = document.querySelector('.tax-main');

  if (isTax) {
    // Tax
    route.current = '/tax';

    const taxButton = document.querySelector('#tax');
    let taxTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    function taxScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta != '1') {
        if (isHome) {
          playAnimation(taxTimeline, true, '/', taxScroll, homeScroll, taxButton, false, false, 1000);
        }
      }
    }

    function animateTax(event) {
      if (taxButton) {
        event.preventDefault();
      }

      if (!taxTimeline.children.length) {
        taxTimeline.add({
          targets: '.lines .rest',
          opacity: [0, 1],
          duration: 1000,
        }, 0).add({
          targets: '.lines',
          translateX: '-225%',
          translateY: '-75%',
          scale: 2,
          duration: 1000,
        }, 0).add({
          targets: '#home-2',
          translateX: [0, '-100%'],
          translateY: [0, '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#tax-1',
          translateX: ['100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(taxTimeline, false, '/tax', homeScroll, taxScroll, taxButton, false, false, 1000);
    }

    if (taxButton) {
      taxButton.addEventListener('click', animateTax);
    }

    // Tax Clients
    const taxClientsButton = document.querySelector('#tax-clients');
    let taxClientsTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let taxClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    // let taxClientsTimeline3 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });
    // let taxClientsTimeline4 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });
    let taxClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    let taxClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    function taxClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end tax clients
        console.log('end tax clients');
      } else {
        console.log('no scrolling inside blog');
      }
    }

    function taxClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxClientsTimeline6.children.length) {
          taxClientsTimeline6.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#home-1, #audit-1, #tax-1',
            height: ['100vh', 0],
            opacity: [1, 0],
            duration: 1,
          },
          0).add({
            targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(taxClientsTimeline6, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(taxClientsTimeline5, true, 'tax/clients2', taxClientsScroll5, taxClientsScroll2, false, false, false, 1600, -1, true);
      }
    }

    // function taxClientsScroll4(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // end tax clients
    //    console.log('end tax clients');
    //  } else {
    //    playAnimation(taxClientsTimeline4, true, 'tax/clients3', taxClientsScroll4, taxClientsScroll3, false, true);
    //  }
    // }

    // function taxClientsScroll3(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // taxClientsTimeline4.add({
    //    //  targets: '.left-blob',
    //    //  opacity: 0,
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-3',
    //    //  zIndex: {
    //    //    value: [3, 2],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '.left-blob-blue',
    //    //  zIndex: {
    //    //    value: [2, 3],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-4',
    //    //  zIndex: {
    //    //    value: [2, 3],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '.left-blob-blue',
    //    //  translateX: ['100%', '-75%'],
    //    //  duration: 1500,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-4',
    //    //  translateX: ['100%', '0'],
    //    //  duration: 1000,
    //    //  offset: 600
    //    // });

    //     // playAnimation(taxClientsTimeline4, false, 'tax/clients4', taxClientsScroll3, taxClientsScroll4);

    //     // end tax clients
    //    console.log('end tax clients');
    //  } else {
    //    playAnimation(taxClientsTimeline3, true, 'tax/clients2', taxClientsScroll3, taxClientsScroll2);
    //  }
    // }

    function taxClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxClientsTimeline3.add({
        //  targets: '.bottom-blob',
        //  opacity: 0,
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '.left-blob',
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '#tax-clients-2',
        //  zIndex: {
        //    value: [3, 2],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '.left-blob',
        //  translateX: ['100%', '-50%'],
        //  duration: 1500,
        //  offset: 0
        // }).add({
        //  targets: '#tax-clients-3',
        //  translateX: ['100%', '0'],
        //  duration: 1000,
        //  offset: 500,
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        // });

        // playAnimation(taxClientsTimeline3, false, 'tax/clients3', taxClientsScroll2, taxClientsScroll3, false, true);

        // end tax clients
        // console.log('end tax clients');

        if (!taxClientsTimeline5.children.length) {
          taxClientsTimeline5.add({
            targets: '.bottom-blob',
            opacity: 0,
            duration: 1,
          }, 0).add({
            targets: '#tax-clients-2',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#tax-clients-2',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 600).add({
            targets: '#tax-clients-5',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#tax-clients-5',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 600);
        }

        playAnimation(taxClientsTimeline5, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true);
      } else {
        playAnimation(taxClientsTimeline2, true, '/tax/clients', taxClientsScroll2, taxClientsScroll, false, true, false, 8000, -1, true);
      }
    }

    function taxClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxClientsTimeline2.children.length) {
          taxClientsTimeline2.add({
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#tax-clients-2',
            translateY: ['100%', '0'],
            duration: 1000,
            zIndex: {
              value: [2, 3],
              round: true
            },
          }, 1000).add({
            targets: '#tax-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 2000).add({
            targets: '#tax-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 3000).add({
            targets: '#tax-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#tax-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#tax-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#tax-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#tax-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#tax-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000).add({
            targets: '#tax-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 7000).add({
            targets: '#tax-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 7000);
        }

        playAnimation(taxClientsTimeline2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true);
      } else {
        playAnimation(taxClientsTimeline, true, '/tax', taxClientsScroll, taxScroll, taxClientsButton, false, true, 4500);
      }
    }

    function animateTaxClients(event) {
      if (taxClientsButton) {
        event.preventDefault();
      }

      if (!taxClientsTimeline.children.length) {
        taxClientsTimeline.add({
          targets: '.lines',
          opacity: [1, 0],
          duration: 1000,
        }, 0).add({
          targets: '#tax-1',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '.right-blob',
          translateX: ['100%', '0'],
          left: ['0', '-100%'],
          duration: 2000,
        }, 0).add({
          targets: '.right-blob',
          opacity: 0,
          duration: 100,
        }, 2000).add({
          targets: '#tax-clients-1',
          translateX: ['100%', '0'],
          duration: 1000,
        }, 500).add({
          targets: '#tax-clients-1 #tax-item-0',
          opacity: [0, 1],
          duration: 1000,
        }, 1500).add({
          targets: '#tax-clients-1 #tax-item-1',
          opacity: [0, 1],
          duration: 1000,
        }, 2500).add({
          targets: '#tax-clients-1 #tax-item-2',
          opacity: [0, 1],
          duration: 1000,
        }, 3500);
      }

      playAnimation(taxClientsTimeline, false, '/tax/clients', taxScroll, taxClientsScroll, taxClientsButton, true, false, 4500, -1, true);

      // set taxt clients next timeline
      setNextTimeline(
        [
          [taxClientsTimeline2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true],
          [taxClientsTimeline5, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true],
          [taxClientsTimeline6, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true]
        ],
        [
          [
            [{
              targets: '.bottom-blob',
              translateY: ['100%', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#tax-clients-2',
              translateY: ['100%', '0'],
              duration: 1000,
              zIndex: {
                value: [2, 3],
                round: true
              },
            }, 1000],
            [{
              targets: '#tax-clients-2 .side-title',
              top: ['150%', '50%'],
              duration: 1000,
            }, 2000],
            [{
              targets: '#tax-clients-2 .audit-facts h2',
              opacity: [0, 0.25],
              borderBottom: '1px solid #fff',
              duration: 1000,
            }, 3000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-0 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 4000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-0 p',
              opacity: [0, 1],
              duration: 1000,
            }, 4000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-1 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 5000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-1 p',
              opacity: [0, 1],
              duration: 1000,
            }, 5000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-2 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 6000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-2 p',
              opacity: [0, 1],
              duration: 1000,
            }, 6000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-3 h2',
              opacity: [0.25, 1],
              borderColor: ['#fff', '#16e7cf'],
              borderWidth: [1, 3],
              duration: 1000,
            }, 7000],
            [{
              targets: '#tax-clients-2 .audit-facts .fact-3 p',
              opacity: [0, 1],
              duration: 1000,
            }, 7000]
          ],
          [
            [{
              targets: '.bottom-blob',
              opacity: 0,
              duration: 1,
            }, 0],
            [{
              targets: '#tax-clients-2',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '#tax-clients-2',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 600],
            [{
              targets: '#tax-clients-5',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '#tax-clients-5',
              translateX: ['100%', '0'],
              duration: 1000,
            }, 600]
          ],
          [
            [{
              targets: '.new-blog',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
                targets: '#home-1, #audit-1, #tax-1',
                height: ['100vh', 0],
                opacity: [1, 0],
                duration: 1,
              },
              0
            ],
            [{
              targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
              opacity: [1, 0],
              duration: 1,
            }, 0],
            [{
              targets: '.new-blog',
              opacity: [0, 1],
              duration: 1000,
            }, 0]
          ]
        ],
        true
      );
    }

    if (taxClientsButton) {
      taxClientsButton.addEventListener('click', animateTaxClients);
    }

    // Tax Candidates
    const taxCandidatesButton = document.querySelector('#tax-candidates');
    let taxCandidatesTimeline = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let taxCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let taxCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    // let taxCandidatesTimeline4 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });

    // function taxCandidatesScroll4(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // end tax candidates
    //    console.log('end tax candidates');
    //  } else {
    //    playAnimation(taxCandidatesTimeline4, true, 'tax/candidates3', taxCandidatesScroll4, taxCandidatesScroll3);
    //  }
    // }

    function taxCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxCandidatesTimeline4.add({
        //  targets: '#col-0, #col-1',
        //  translateX: ['0', '-100vw'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#col-2, #col-3',
        //  translateX: ['0', '100vw'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#mid-arrow',
        //  opacity: 0,
        //  duration: 100,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3 h1',
        //  translateY: ['0', '-100vh'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3',
        //  zIndex: {
        //    value: [3, 2],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 1000,
        // }).add({
        //  targets: '#tax-candidates-4',
        //  zIndex: {
        //    value: [1, 3],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 1000
        // }).add({
        //  targets: '#tax-candidates-4',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 1000
        // }).add({
        //  targets: '#tax-candidates-4 .call-to-action',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 2000
        // });

        // playAnimation(taxCandidatesTimeline4, false, 'tax/candidates4', taxCandidatesScroll3, taxCandidatesScroll4);

        // end tax candidates
        console.log('end tax candidates');
      } else {
        playAnimation(taxCandidatesTimeline3, true, 'tax/applicants2', taxCandidatesScroll3, taxCandidatesScroll2, false, false, false, 2100, -1, true);
      }
    }

    function taxCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxCandidatesTimeline3.add({
        //  targets: '.bottom-blob',
        //  translateY: ['100%', '-100%'],
        //  duration: 2000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3',
        //  translateY: ['100%', '0'],
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        //  duration: 1000,
        //  offset: 1000,
        // }).add({
        //  targets: '#col-0 svg, #col-0 h2, #col-0 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 2000
        // }).add({
        //  targets: '#col-1 svg, #col-1 h2, #col-1 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 3000
        // }).add({
        //  targets: '#col-2 svg, #col-2 h2, #col-2 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 4000
        // }).add({
        //  targets: '#col-3 svg, #col-3 h2, #col-3 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 5000
        // });

        if (!taxCandidatesTimeline3.children.length) {
          taxCandidatesTimeline3.add({
            targets: '.right-blob',
            left: ['-100%', '0'],
            translateX: ['0', '-100%'],
            opacity: [0, 1],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#tax-candidates-2',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '#tax-candidates-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1000,
          }, 0).add({
            targets: '#tax-candidates-3 .container-fluid',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#tax-candidates-1',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.lines',
            opacity: [1, 0],
            duration: 1000,
          }, 0).add({
            targets: '#tax-1',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '.right-blob',
            translateX: ['100%', '0'],
            left: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '.right-blob',
            opacity: 0,
            duration: 100,
          }, 2000).add({
            targets: '#tax-candidates-1',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 500);
        }

        playAnimation(taxCandidatesTimeline3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true);
      } else {
        playAnimation(taxCandidatesTimeline2, true, '/tax/applicants', taxCandidatesScroll2, taxCandidatesScroll, false, true, false, 6000, -1, true);
      }
    }

    function taxCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxCandidatesTimeline2.children.length) {
          taxCandidatesTimeline2.add({
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#tax-candidates-3',
            translateY: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 1000).add({
            targets: '#col-0 svg, #col-0 h2, #col-0 p',
            opacity: 1,
            duration: 1000,
          }, 2000).add({
            targets: '#col-1 svg, #col-1 h2, #col-1 p',
            opacity: 1,
            duration: 1000,
          }, 3000).add({
            targets: '#col-2 svg, #col-2 h2, #col-2 p',
            opacity: 1,
            duration: 1000,
          }, 4000).add({
            targets: '#col-3 svg, #col-3 h2, #col-3 p',
            opacity: 1,
            duration: 1000,
          }, 5000);
        }

        playAnimation(taxCandidatesTimeline2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true);
      } else {
        playAnimation(taxCandidatesTimeline, true, '/tax', taxScroll, taxCandidatesScroll, taxCandidatesButton, false, true, 3000);
      }

      // if (delta == '1') {
      // taxCandidatesTimeline2.add({
      //  targets: '#tax-candidates-1',
      //  translateX: ['0', '-100%'],
      //  duration: 1000,
      //  offset: 0
      // }).add({
      //  targets: '#tax-candidates-2',
      //  marginLeft: [0, -1],
      //  duration: 1,
      //  offset: 0
      // }).add({
      //  targets: '#tax-candidates-2',
      //  translateX: ['100%', '0'],
      //  duration: 1000,
      //  offset: 0
      // }).add({
      //  targets: '#tax-candidates-2 #love',
      //  translateX: ['200%', '0'],
      //  duration: 1000,
      //  offset: 0
      // }).add({
      //  targets: '#tax-candidates-2 #tax',
      //  translateX: ['-200vw', '0'],
      //  duration: 1000,
      //  offset: 0
      // }).add({
      //  targets: '#tax-candidates-2 .content-wrap',
      //  opacity: [0, 1],
      //  duration: 1000,
      //  offset: 1000
      // });

      // playAnimation(taxCandidatesTimeline, false, 'tax/candidates2', taxCandidatesScroll, taxCandidatesScroll2, false, true, true);

      // }
    }

    function animateTaxCandidates(event) {
      if (taxCandidatesButton) {
        event.preventDefault();
      }

    if (!taxCandidatesTimeline.children.length) {
      taxCandidatesTimeline.add({
        targets: '.lines',
        opacity: [1, 0],
        duration: 1000,
      }, 0).add({
        targets: '.right-blob',
        translateX: ['100%', '0'],
        left: ['0', '-100%'],
        duration: 2000,
      }, 0).add({
        targets: '.right-blob',
        opacity: [1, 0],
        duration: 100,
      }, 2000).add({
        targets: '#tax-candidates-2',
        marginLeft: [0, -1],
        duration: 1,
      }, 0).add({
        targets: '#tax-candidates-2',
        translateX: ['100%', '0'],
        duration: 1000,
      }, 500).add({
        targets: '#tax-candidates-2 #love',
        translateX: ['200%', '0'],
        duration: 1000,
      }, 1000).add({
        targets: '#tax-candidates-2 #tax',
        translateX: ['-200vw', '0'],
        duration: 1000,
      }, 1000).add({
        targets: '#tax-candidates-2 .content-wrap',
        opacity: [0, 1],
        duration: 1000,
      }, 2000);
    }

      playAnimation(taxCandidatesTimeline, false, '/tax/applicants', taxScroll, taxCandidatesScroll, taxCandidatesButton, true, false, 3000, -1, true);

      // set tat candidates next timeline
      setNextTimeline(
        [
          [taxCandidatesTimeline2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true],
          [taxCandidatesTimeline3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true]
        ],
        [
          [
            [{
              targets: '.bottom-blob',
              translateY: ['100%', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '#tax-candidates-3',
              translateY: ['100%', '0'],
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1000,
            }, 1000],
            [{
              targets: '#col-0 svg, #col-0 h2, #col-0 p',
              opacity: 1,
              duration: 1000,
            }, 2000],
            [{
              targets: '#col-1 svg, #col-1 h2, #col-1 p',
              opacity: 1,
              duration: 1000,
            }, 3000],
            [{
              targets: '#col-2 svg, #col-2 h2, #col-2 p',
              opacity: 1,
              duration: 1000,
            }, 4000],
            [{
              targets: '#col-3 svg, #col-3 h2, #col-3 p',
              opacity: 1,
              duration: 1000,
            }, 5000]
          ],
          [
            [{
              targets: '.right-blob',
              left: ['-100%', '0'],
              translateX: ['0', '-100%'],
              opacity: [0, 1],
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '#tax-candidates-2',
              opacity: [1, 0],
              duration: 1,
            }, 0],
            [{
              targets: '#tax-candidates-3',
              zIndex: {
                value: [3, 2],
                round: true
              },
              duration: 1000,
            }, 0],
            [{
              targets: '#tax-candidates-3 .container-fluid',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '#tax-candidates-1',
              zIndex: {
                value: [2, 3],
                round: true
              },
              duration: 1,
            }, 0],
            [{
              targets: '.lines',
              opacity: [1, 0],
              duration: 1000,
            }, 0],
            [{
              targets: '#tax-1',
              translateX: ['0', '-100%'],
              duration: 1000,
            }, 0],
            [{
              targets: '.right-blob',
              translateX: ['100%', '0'],
              left: ['0', '-100%'],
              duration: 2000,
            }, 0],
            [{
              targets: '.right-blob',
              opacity: 0,
              duration: 100,
            }, 2000],
            [{
              targets: '#tax-candidates-1',
              translateX: ['100%', '0'],
              duration: 1000,
            }, 500]
          ]
        ],
        true
      );
    }

    if (taxCandidatesButton) {
      taxCandidatesButton.addEventListener('click', animateTaxCandidates);
    }
  }

  const isTaxClients = document.querySelector('.tax-clients-main');

  if (isTaxClients) {
    // Tax Clients
    route.current = '/tax/clients';

    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }

    let taxClientsTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    // let taxClientsTimeline3 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });
    // let taxClientsTimeline4 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });

    let taxClientsTimeline5 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    let taxClientsTimeline6 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });

    // set taxt clients next timeline
    setNextTimeline(
      [
        [taxClientsTimeline2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true],
        [taxClientsTimeline5, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true],
        [taxClientsTimeline6, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true]
      ],
      [
        [
          [{
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#tax-clients-2',
            translateY: ['100%', '0'],
            duration: 1000,
            zIndex: {
              value: [2, 3],
              round: true
            },
          }, 1000],
          [{
            targets: '#tax-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 2000],
          [{
            targets: '#tax-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 3000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 7000],
          [{
            targets: '#tax-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 7000]
        ],
        [
          [{
            targets: '.bottom-blob',
            opacity: 0,
            duration: 1,
          }, 0],
          [{
            targets: '#tax-clients-2',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          }, 0],
          [{
            targets: '#tax-clients-2',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 600],
          [{
            targets: '#tax-clients-5',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0],
          [{
            targets: '#tax-clients-5',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 600]
        ],
        [
          [{
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0],
          [{
              targets: '#home-1, #audit-1, #tax-1',
              height: ['100vh', 0],
              opacity: [1, 0],
              duration: 1,
            },
            0
          ],
          [{
            targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0],
          [{
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0]
        ]
      ]
    );

    function taxClientsScroll6(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // end tax clients
        console.log('end tax clients');
      } else {
        console.log('no scrolling inside blog');
      }
    }

    function taxClientsScroll5(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxClientsTimeline6.children.length) {
          taxClientsTimeline6.add({
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '#home-1, #audit-1, #tax-1',
            height: ['100vh', 0],
            opacity: [1, 0],
            duration: 1,
          },
          0).add({
            targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          }, 0);
        }

        playAnimation(taxClientsTimeline6, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true);
      } else {
        playAnimation(taxClientsTimeline5, true, 'tax/clients2', taxClientsScroll5, taxClientsScroll2, false, false, false, 1600, -1, true);
      }
    }

    // function taxClientsScroll4(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // end tax clients
    //    console.log('end tax clients');
    //  } else {
    //    playAnimation(taxClientsTimeline4, true, 'tax/clients3', taxClientsScroll4, taxClientsScroll3, false, true);
    //  }
    // }

    // function taxClientsScroll3(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // taxClientsTimeline4.add({
    //    //  targets: '.left-blob',
    //    //  opacity: 0,
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-3',
    //    //  zIndex: {
    //    //    value: [3, 2],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '.left-blob-blue',
    //    //  zIndex: {
    //    //    value: [2, 3],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-4',
    //    //  zIndex: {
    //    //    value: [2, 3],
    //    //    round: true
    //    //  },
    //    //  duration: 1,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '.left-blob-blue',
    //    //  translateX: ['100%', '-75%'],
    //    //  duration: 1500,
    //    //  offset: 0
    //    // }).add({
    //    //  targets: '#tax-clients-4',
    //    //  translateX: ['100%', '0'],
    //    //  duration: 1000,
    //    //  offset: 600
    //    // });

    //     // playAnimation(taxClientsTimeline4, false, 'tax/clients4', taxClientsScroll3, taxClientsScroll4);

    //     // end tax clients
    //    console.log('end tax clients');
    //  } else {
    //    playAnimation(taxClientsTimeline3, true, 'tax/clients2', taxClientsScroll3, taxClientsScroll2);
    //  }
    // }

    function taxClientsScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxClientsTimeline3.add({
        //  targets: '.bottom-blob',
        //  opacity: 0,
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '.left-blob',
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '#tax-clients-2',
        //  zIndex: {
        //    value: [3, 2],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '.left-blob',
        //  translateX: ['100%', '-50%'],
        //  duration: 1500,
        //  offset: 0
        // }).add({
        //  targets: '#tax-clients-3',
        //  translateX: ['100%', '0'],
        //  duration: 1000,
        //  offset: 500,
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        // });

        // playAnimation(taxClientsTimeline3, false, 'tax/clients3', taxClientsScroll2, taxClientsScroll3, false, true);

        // end tax clients
        // console.log('end tax clients');

      if (!taxClientsTimeline5.children.length) {
        taxClientsTimeline5.add({
        targets: '.bottom-blob',
        opacity: 0,
        duration: 1,
        }, 0).add({
        targets: '#tax-clients-2',
        zIndex: {
          value: [3, 2],
          round: true
        },
        duration: 1,
        }, 0).add({
        targets: '#tax-clients-2',
        translateX: ['0', '-100%'],
        duration: 1000,
        }, 600).add({
        targets: '#tax-clients-5',
        zIndex: {
          value: [2, 3],
          round: true
        },
        duration: 1,
        }, 0).add({
        targets: '#tax-clients-5',
        translateX: ['100%', '0'],
        duration: 1000,
        }, 600);
      }

        playAnimation(taxClientsTimeline5, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true);
      } else {
        playAnimation(taxClientsTimeline2, true, '/tax/clients', taxClientsScroll2, taxClientsScroll, false, true, false, 8000, -1, true);
        // staggerAnimation(taxClientsTimeline2);
      }
    }

    function taxClientsScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxClientsTimeline2.children.length) {
          taxClientsTimeline2.add({
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#tax-clients-2',
            translateY: ['100%', '0'],
            duration: 1000,
            zIndex: {
              value: [2, 3],
              round: true
            },
          }, 1000).add({
            targets: '#tax-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          }, 2000).add({
            targets: '#tax-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          }, 3000).add({
            targets: '#tax-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 4000).add({
            targets: '#tax-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          }, 4000).add({
            targets: '#tax-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 5000).add({
            targets: '#tax-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          }, 5000).add({
            targets: '#tax-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 6000).add({
            targets: '#tax-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          }, 6000).add({
            targets: '#tax-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          }, 7000).add({
            targets: '#tax-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          }, 7000);
        }

        playAnimation(taxClientsTimeline2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true );
      }
    }

    window.addEventListener('wheel', taxClientsScroll);
    window.addEventListener('touchmove', taxClientsScroll);
  }

  const isTaxCandidates = document.querySelector('.tax-candidates-main');

  if (isTaxCandidates) {
    // Tax Candidates
    route.current = '/tax/applicants';

    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }

    let taxCandidatesTimeline2 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    let taxCandidatesTimeline3 = anime.timeline({
      easing: 'easeInOutSine',
      autoplay: false
    });
    // let taxCandidatesTimeline4 = anime.timeline({
    //  easing: 'easeInOutSine',
    //  autoplay: false
    // });

    // function taxCandidatesScroll4(event) {
    //  let delta = Math.sign(event.deltaY);

    //  if (event.touches) {
    //    delta = handleTouchMove(event);
    //  }

    //  if (delta == '1') {
    //    // end tax candidates
    //    console.log('end tax candidates');
    //  } else {
    //    playAnimation(taxCandidatesTimeline4, true, 'tax/candidates3', taxCandidatesScroll4, taxCandidatesScroll3);
    //  }
    // }

    // set tat candidates next timeline
    setNextTimeline(
      [
        [taxCandidatesTimeline2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true],
        [taxCandidatesTimeline3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true]
      ],
      [
        [
          [{
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '#tax-candidates-3',
            translateY: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 1000],
          [{
            targets: '#col-0 svg, #col-0 h2, #col-0 p',
            opacity: 1,
            duration: 1000,
          }, 2000],
          [{
            targets: '#col-1 svg, #col-1 h2, #col-1 p',
            opacity: 1,
            duration: 1000,
          }, 3000],
          [{
            targets: '#col-2 svg, #col-2 h2, #col-2 p',
            opacity: 1,
            duration: 1000,
          }, 4000],
          [{
            targets: '#col-3 svg, #col-3 h2, #col-3 p',
            opacity: 1,
            duration: 1000,
          }, 5000]
        ],
        [
          [{
            targets: '.right-blob',
            left: ['-100%', '0'],
            translateX: ['0', '-100%'],
            opacity: [0, 1],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0],
          [{
            targets: '#tax-candidates-2',
            opacity: [1, 0],
            duration: 1,
          }, 0],
          [{
            targets: '#tax-candidates-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1000,
          }, 0],
          [{
            targets: '#tax-candidates-3 .container-fluid',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '#tax-candidates-1',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0],
          [{
            targets: '.lines',
            opacity: [1, 0],
            duration: 1000,
          }, 0],
          [{
            targets: '#tax-1',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0],
          [{
            targets: '.right-blob',
            translateX: ['100%', '0'],
            left: ['0', '-100%'],
            duration: 2000,
          }, 0],
          [{
            targets: '.right-blob',
            opacity: 0,
            duration: 100,
          }, 2000],
          [{
            targets: '#tax-candidates-1',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 500]
        ]
      ]
    );

    function taxCandidatesScroll3(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxCandidatesTimeline4.add({
        //  targets: '#col-0, #col-1',
        //  translateX: ['0', '-100vw'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#col-2, #col-3',
        //  translateX: ['0', '100vw'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#mid-arrow',
        //  opacity: 0,
        //  duration: 100,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3 h1',
        //  translateY: ['0', '-100vh'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3',
        //  zIndex: {
        //    value: [3, 2],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 1000,
        // }).add({
        //  targets: '#tax-candidates-4',
        //  zIndex: {
        //    value: [1, 3],
        //    round: true
        //  },
        //  duration: 1,
        //  offset: 1000
        // }).add({
        //  targets: '#tax-candidates-4',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 1000
        // }).add({
        //  targets: '#tax-candidates-4 .call-to-action',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 2000
        // });

        // playAnimation(taxCandidatesTimeline4, false, 'tax/candidates4', taxCandidatesScroll3, taxCandidatesScroll4);

        // end tax candidates
        console.log('end tax candidates');
      } else {
        playAnimation(taxCandidatesTimeline3, true, 'tax/applicants2', taxCandidatesScroll3, taxCandidatesScroll2, false, false, false, 2100, -1, true);
      }
    }

    function taxCandidatesScroll2(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        // taxCandidatesTimeline3.add({
        //  targets: '.bottom-blob',
        //  translateY: ['100%', '-100%'],
        //  duration: 2000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-3',
        //  translateY: ['100%', '0'],
        //  zIndex: {
        //    value: [2, 3],
        //    round: true
        //  },
        //  duration: 1000,
        //  offset: 1000,
        // }).add({
        //  targets: '#col-0 svg, #col-0 h2, #col-0 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 2000
        // }).add({
        //  targets: '#col-1 svg, #col-1 h2, #col-1 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 3000
        // }).add({
        //  targets: '#col-2 svg, #col-2 h2, #col-2 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 4000
        // }).add({
        //  targets: '#col-3 svg, #col-3 h2, #col-3 p',
        //  opacity: 1,
        //  duration: 1000,
        //  offset: 5000
        // });

        if (!taxCandidatesTimeline3.children.length) {
          taxCandidatesTimeline3.add({
            targets: '#tax-candidates-2',
            opacity: [1, 0],
            duration: 1,
          }, 0).add({
            targets: '#tax-candidates-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1000,
          }, 0).add({
            targets: '#tax-candidates-3 .container-fluid',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '#tax-candidates-1',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          }, 0).add({
            targets: '.lines',
            opacity: [1, 0],
            duration: 1000,
          }, 0).add({
            targets: '#tax-1',
            translateX: ['0', '-100%'],
            duration: 1000,
          }, 0).add({
            targets: '.right-blob',
            translateX: ['100%', '0'],
            left: ['0', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '.right-blob',
            opacity: [1, 0],
            duration: 100,
          }, 2000).add({
            targets: '#tax-candidates-1',
            translateX: ['100%', '0'],
            duration: 1000,
          }, 500);
        }

        playAnimation(taxCandidatesTimeline3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true);
      } else {
        playAnimation(taxCandidatesTimeline2, true, '/tax/applicants', taxCandidatesScroll2, taxCandidatesScroll, false, true, true, 6000, -1, true);
      }
    }

    function taxCandidatesScroll(event) {
      let delta = Math.sign(event.deltaY);

      if (event.touches) {
        delta = handleTouchMove(event);
      }

      if (delta == '1') {
        if (!taxCandidatesTimeline2.children.length) {
          taxCandidatesTimeline2.add({
            targets: '.bottom-blob',
            translateY: ['100%', '-100%'],
            duration: 2000,
          }, 0).add({
            targets: '#tax-candidates-3',
            translateY: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          }, 1000).add({
            targets: '#col-0 svg, #col-0 h2, #col-0 p',
            opacity: 1,
            duration: 1000,
          }, 2000).add({
            targets: '#col-1 svg, #col-1 h2, #col-1 p',
            opacity: 1,
            duration: 1000,
          }, 3000).add({
            targets: '#col-2 svg, #col-2 h2, #col-2 p',
            opacity: 1,
            duration: 1000,
          }, 4000).add({
            targets: '#col-3 svg, #col-3 h2, #col-3 p',
            opacity: 1,
            duration: 1000,
          }, 5000);
        }

        playAnimation(taxCandidatesTimeline2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true);
      }

      // if (delta == '1') {
        // taxCandidatesTimeline2.add({
        //  targets: '#tax-candidates-1',
        //  translateX: ['0', '-100%'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2',
        //  marginLeft: [0, -1],
        //  duration: 1,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2',
        //  translateX: ['100%', '0'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2 #love',
        //  translateX: ['200%', '0'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2 #tax',
        //  translateX: ['-200vw', '0'],
        //  duration: 1000,
        //  offset: 0
        // }).add({
        //  targets: '#tax-candidates-2 .content-wrap',
        //  opacity: [0, 1],
        //  duration: 1000,
        //  offset: 1000
        // });

        // playAnimation(taxCandidatesTimeline, false, 'tax/candidates2', taxCandidatesScroll, taxCandidatesScroll2, false, true, true);

      // }
    }

    window.addEventListener('wheel', taxCandidatesScroll);
    window.addEventListener('touchmove', taxCandidatesScroll);
  }

  const isBlog = document.querySelector('.new-blog');
  const isMainBlog = document.querySelector('.main-new-blog');
  const isBlogSingle = document.querySelector('.single-post');

  if (isBlogSingle || isBlog) {
    shaveBlogs();
  }

  if (isMainBlog) {
    route.current = '/blog';
    header.classList.add('header-light');
  }

  if (isBlog) {
    // truncate
    function truncate(content) {
      Array.prototype.forEach.call(content, post => {
        setTimeout(function () {
          post.style.height = post.offsetWidth + 'px';
          const heading = post.querySelector('h2');
          const headingLink = post.querySelector('h2 a');
          let headingHeight = heading.offsetHeight;
          const dateHeight = post.querySelector('time').offsetHeight;
          const maxHeight = post.offsetHeight - 32; // padding
          const paragraphHeight = maxHeight - headingHeight - dateHeight;
          const paragraph = post.querySelector('p');

          if (heading.offsetHeight >= 72) {
            headingHeight = 72;
          }

          shave(headingLink, headingHeight);
          shave(paragraph, paragraphHeight);
        }, 100);
      });
    }

    const postContent = document.querySelectorAll('.new-blog .slider .slide .blog-post .post-content');
    truncate(postContent);

    window.addEventListener('resize', function () {
      truncate(postContent);
    });

    if (!mobileDevice.phone()) {
      // slider
      var slider = new Swiper('.blog-slider', {
        spaceBetween: 40,
        mousewheel: true,
        navigation: {
          nextEl: '.slider-nav-wrap .next',
          prevEl: '.slider-nav-wrap .prev',
        },
      });
    }
  }

  const isClients = document.querySelector('.clients-main');

  if (isClients) {
    route.current = '/clients';
  }

  const isContact = document.querySelector('.contact-main');

  if (isContact) {
    route.current = '/contact';
  }

  const isTeam = document.querySelector('.main-team');

  if (isTeam) {
    route.current = '/team';
  }

  const isBlogSingle = document.querySelector('.single-post');

  if (isBlogSingle) {
    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }
  }

  const instagram = document.querySelector('.insta');

  if (instagram) {
    loadInstagram();
  }

  const isApply = document.querySelector('.apply-main');

  if (isApply) {
    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }
  }

  const isAuditCost = document.querySelector('.audit-cost-main');

  if (isAuditCost) {
    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }
  }

  const isHowWorks = document.querySelector('.how-works-main');

  if (isHowWorks) {
    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }
  }

  const isThanks = document.querySelector('.thanks-main');

  if (isThanks) {
    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }
  }

  const isCaseStudy = document.querySelector('.case-study-main');

  if (isCaseStudy) {
    if (!mobileDevice.phone()) {
      const header = document.querySelector('.header');
      header.classList.add('header-light');
    }
  }

  // $('.covid-banner').on('closed.bs.alert', function () {
  //   body.style.paddingTop = 0;
  // });
});
