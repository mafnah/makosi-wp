const isAuditClients = document.querySelector('.audit-clients-main');

if (isAuditClients) {
  // Audit Clients
  route.current = '/audit/clients';

  let auditClientsTimeline2 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditClientsTimeline3 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditClientsTimeline4 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditClientsTimeline5 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditClientsTimeline6 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditClientsTimeline7 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });

  if (!auditClientsTimeline2.children.length) {
    auditClientsTimeline2.add({
      targets: '.welcome-content',
      translateY: ['0', '-100%'],
      duration: 1000,
    }, 0).add({
      targets: '.why-section, .why-content',
      translateY: ['100%', '0'],
      opacity: [0, 1],
      duration: 1000,
    }, 0).add({
      targets: '.why-content .section-title',
      translateY: ['100%', '0'],
      rotate: ['-90deg', '-90deg'],
      duration: 1000,
    }, 0).add({
      targets: '.why-section .fact-0',
      opacity: [0, 1],
      duration: 1000,
    }, 1000).add({
        targets: '.why-section .fact-1',
      opacity: [0, 1],
      duration: 1000,
    }, 2000).add({
        targets: '.why-section .fact-2',
      opacity: [0, 1],
      duration: 1000,
    }, 3000);
  }

  // set audit clients next timeline
  setNextTimeline(
    [
      [auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
      [auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
      [auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
      [auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
      [auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
      [auditClientsTimeline7, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
    ],
    [
      [
        [
          {
            targets: '.welcome-content',
            translateY: ['0', '-100%'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-section, .why-content',
            translateY: ['100%', '0'],
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-content .section-title',
            translateY: ['100%', '0'],
            rotate: ['-90deg', '-90deg'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-section, .why-content',
            translateY: ['100%', '0'],
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-content .section-title',
            translateY: ['100%', '0'],
            rotate: ['-90deg', '-90deg'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-section .fact-0',
            opacity: [0, 1],
            duration: 1000,
          },
          1000
        ],
        [
          {
            targets: '.why-section .fact-1',
            opacity: [0, 1],
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '.why-section .fact-2',
            opacity: [0, 1],
            duration: 1000,
          },
          3000
        ]
      ],
      [
        [
          {
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          },
          1000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          3000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          },
          3000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          4000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          },
          4000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          5000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          },
          5000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          6000
        ],
        [
          {
            targets: '#audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          },
          6000
        ]
      ],
      [
        [
          {
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          },
          500
        ]
      ],
      [
        [
          {
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          },
          1000
        ],
        [
          {
            targets: '#audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '#audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '#audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          },
          3000
        ],
        [
          {
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ]
      ],
      [
        [
          {
            targets: '#audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0
        ]
      ],
      [
        [
          {
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ]
      ],
    ]
  );

  function auditClientsScroll7(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      // end of audit clients
      console.log('end audit clients animations');
    } else {
      console.log('no scrolling events inside blog');
    }
  }

  function auditClientsScroll6(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditClientsTimeline7.children.length) {
        auditClientsTimeline7.add({
          targets: '.new-blog',
          zIndex: {
            value: [2, 3],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
          opacity: [1, 0],
          duration: 1,
        }, 0).add({
          targets: '.new-blog',
          opacity: [0, 1],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditClientsTimeline7, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
    } else {
      playAnimation(auditClientsTimeline6, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
    }
  }

  function auditClientsScroll5(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditClientsTimeline6.children.length) {
        auditClientsTimeline6.add({
          targets: '#audit-clients-5',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditClientsTimeline6, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
    } else {
      playAnimation(auditClientsTimeline5, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
    }
  }

  function auditClientsScroll4(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditClientsTimeline5.children.length) {
        auditClientsTimeline5.add({
          targets: '.left-blob',
          zIndex: {
            value: [3, 2],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '#audit-clients-3',
          zIndex: {
            value: [3, 2],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '#audit-clients-3',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-clients-4',
          translateX: ['100%', '0'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-clients-4 .first-title',
          opacity: [0, 1],
          duration: 1000,
        }, 0).add({
          targets: '#audit-clients-4 .second-title',
          opacity: [0, 1],
          duration: 1000,
        }, 1000).add({
          targets: '#audit-clients-4 .third-title',
          opacity: [0, 1],
          duration: 1000,
        }, 2000).add({
          targets: '#audit-clients-4 p',
          opacity: [0, 1],
          duration: 1000,
        }, 2000).add({
          targets: '#audit-clients-4 .industries',
          opacity: [0, 1],
          duration: 1000,
        }, 3000).add({
          targets: '.lines-single .top-line path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .bot-line path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .hor-line-1 path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .hor-line-2 path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .hor-line-3 path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0);
      }

      playAnimation(auditClientsTimeline5, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
    } else {
      playAnimation(auditClientsTimeline4, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
    }
  }

  function auditClientsScroll3(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditClientsTimeline4.children.length) {
        auditClientsTimeline4.add({
          targets: '.left-blob',
          zIndex: {
            value: [2, 3],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '.left-blob',
          translateX: ['100%', '-50%'],
          duration: 1500,
        }, 0).add({
          targets: '#audit-clients-3',
          translateX: ['100%', '0'],
          zIndex: {
            value: [2, 3],
            round: true
          },
          duration: 1000,
        }, 500);
      }

      playAnimation(auditClientsTimeline4, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
    } else {
      playAnimation(auditClientsTimeline3, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
    }
  }

  function auditClientsScroll2(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditClientsTimeline3.children.length) {
        auditClientsTimeline3.add({
          targets: '.top-blob',
          top: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-clients-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-clients-2 .side-title',
          top: ['150%', '50%'],
          duration: 1000,
        }, 1000).add({
          targets: '#audit-clients-2 .audit-facts h2',
          opacity: [0, 0.25],
          borderBottom: '1px solid #fff',
          duration: 1000,
        }, 2000).add({
          targets: '#audit-clients-2 .audit-facts .fact-0 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 3000).add({
          targets: '#audit-clients-2 .audit-facts .fact-0 p',
          opacity: [0, 1],
          duration: 1000,
        }, 3000).add({
          targets: '#audit-clients-2 .audit-facts .fact-1 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 4000).add({
          targets: '#audit-clients-2 .audit-facts .fact-1 p',
          opacity: [0, 1],
          duration: 1000,
        }, 4000).add({
          targets: '#audit-clients-2 .audit-facts .fact-2 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 5000).add({
          targets: '#audit-clients-2 .audit-facts .fact-2 p',
          opacity: [0, 1],
          duration: 1000,
        }, 5000).add({
          targets: '#audit-clients-2 .audit-facts .fact-3 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 6000).add({
          targets: '#audit-clients-2 .audit-facts .fact-3 p',
          opacity: [0, 1],
          duration: 1000,
        }, 6000);
      }

      playAnimation(auditClientsTimeline3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
    } else {
      playAnimation(auditClientsTimeline2, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
    }
  }

  function auditClientsScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      playAnimation(auditClientsTimeline2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
    }
  }

  window.addEventListener('wheel', auditClientsScroll);
  window.addEventListener('touchmove', auditClientsScroll);
}