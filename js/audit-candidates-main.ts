const isAuditCandidates = document.querySelector('.audit-candidates-main');

if (isAuditCandidates) {
  // Audit Candidates
  route.current = '/audit/applicants';

  let auditCandidatesTimeline2 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditCandidatesTimeline3 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditCandidatesTimeline4 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let auditCandidatesTimeline5 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });

  const auditCandidatesButton = document.querySelector('#audit-candidates');

  // set audit candidates next timeline
  setNextTimeline(
    [
      [auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
      [auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
      [auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
      [auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
    ],
    [
      [
        [{
          targets: '#audit-candidates-1-1',
          translateY: ['0', '-100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#audit-candidates-1-2',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#audit-candidates-1-3',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000]
      ],
      [
        [{
          targets: '.top-blob',
          top: ['0', '-100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#audit-candidates-1',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0],
        [{
          targets: '#audit-candidates-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0]
      ],
      [
        [{
          targets: '#audit-candidates-2 .slide-title-top',
          translateX: ['-100%', '0'],
          duration: 2000,
        }, 0],
        [{
          targets: '#audit-candidates-2 .slide-title-bottom',
          translateX: ['100%', '0'],
          duration: 2000,
        }, 0],
        [{
          targets: '#audit-candidates-2 .slide-title',
          background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
          duration: 2000,
        }, 0]
      ],
      [
        [{
          targets: '#audit-candidates-2 .slide-title-top',
          translateX: ['0', '100%'],
          duration: 2000,
        }, 0],
        [{
          targets: '#audit-candidates-2 .slide-title-bottom',
          translateX: ['0', '-100%'],
          duration: 2000,
        }, 0],
        [{
          targets: '#audit-candidates-3',
          opacity: [0, 1],
          zIndex: {
            value: [1, 3],
            round: true
          },
          duration: 1,
        }, 1000],
        [{
          targets: '#audit-candidates-3 .before',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 1000],
        [{
          targets: '#audit-candidates-3 .after',
          translateX: ['0', '100%'],
          duration: 1000,
        }, 1000],
        [{
          targets: '#audit-candidates-3 .call-to-action',
          opacity: ['0', '1'],
          duration: 1000,
        }, 2000]
      ]
    ]
  );

  function auditCandidatesScroll5(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      // end audit candidates
      console.log('end audit candidates');
    } else {
      playAnimation(auditCandidatesTimeline5, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
    }
  }

  function auditCandidatesScroll4(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditCandidatesTimeline5.children.length) {
        auditCandidatesTimeline5.add({
          targets: '#audit-candidates-2 .slide-title-top',
          translateX: ['0', '100%'],
          duration: 2000,
        }, 0).add({
          targets: '#audit-candidates-2 .slide-title-bottom',
          translateX: ['0', '-100%'],
          duration: 2000,
        }, 0).add({
          targets: '#audit-candidates-3',
          opacity: [0, 1],
          zIndex: {
            value: [1, 3],
            round: true
          },
          duration: 1,
        }, 1000).add({
          targets: '#audit-candidates-3 .before',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 1000).add({
          targets: '#audit-candidates-3 .after',
          translateX: ['0', '100%'],
          duration: 1000,
        }, 1000).add({
          targets: '#audit-candidates-3 .call-to-action',
          opacity: ['0', '1'],
          duration: 1000,
        }, 2000);
      }

      playAnimation(auditCandidatesTimeline5, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
    } else {
      playAnimation(auditCandidatesTimeline4, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
    }
  }

  function auditCandidatesScroll3(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditCandidatesTimeline4.children.length) {
        auditCandidatesTimeline4.add({
          targets: '#audit-candidates-2 .slide-title-top',
          translateX: ['-100%', '0'],
          duration: 2000,
        }, 0).add({
          targets: '#audit-candidates-2 .slide-title-bottom',
          translateX: ['100%', '0'],
          duration: 2000,
        }, 0).add({
          targets: '#audit-candidates-2 .slide-title',
          background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
          duration: 2000,
        }, 0);
      }

      playAnimation(auditCandidatesTimeline4, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
    } else {
      playAnimation(auditCandidatesTimeline3, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
    }
  }

  function auditCandidatesScroll2(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditCandidatesTimeline3.children.length) {
        auditCandidatesTimeline3.add({
          targets: '.top-blob',
          top: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(auditCandidatesTimeline3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
    } else {
      playAnimation(auditCandidatesTimeline2, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
    }
  }

  function auditCandidatesScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!auditCandidatesTimeline2.children.length) {
        auditCandidatesTimeline2.add({
          targets: '#audit-candidates-1-1',
          translateY: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1-2',
          translateY: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#audit-candidates-1-3',
          translateX: ['-100%', '0'],
          duration: 1000,
        }, 1000);
      }

      playAnimation(auditCandidatesTimeline2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
    }
  }

  window.addEventListener('wheel', auditCandidatesScroll);
  window.addEventListener('touchmove', auditCandidatesScroll);
}