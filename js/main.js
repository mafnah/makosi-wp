// @ts-nocheck
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
// Math.sign polyfill
if (!Math.sign) {
    Math.sign = function (x) {
        // If x is NaN, the result is NaN.
        // If x is -0, the result is -0.
        // If x is +0, the result is +0.
        // If x is negative and not -0, the result is -1.
        // If x is positive and not +0, the result is +1.
        return ((x > 0) - (x < 0)) || +x;
        // A more aesthetic pseudo-representation:
        //
        // ( (x > 0) ? 1 : 0 )  // if x is positive, then positive one
        //          +           // else (because you can't be both - and +)
        // ( (x < 0) ? -1 : 0 ) // if x is negative, then negative one
        //         ||           // if x is 0, -0, or NaN, or not a number,
        //         +x           // then the result will be x, (or) if x is
        //                      // not a number, then x converts to number
    };
}
// mobile
var mobileDevice = new MobileDetect(window.navigator.userAgent);
// loader
var loader = document.querySelector('.lds-loader');
// header
var header = document.querySelector('.header');
var headerNav = document.querySelector('.header .nav-button');
// global
var html = document.querySelector('html');
var body = document.querySelector('body');
// detect IE
function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        body.classList.add('ie');
    }
}
msieversion();
// handle nav
function navHandler(route) {
    if (!/\d/.test(route) && route !== '/apply') {
        window.history.pushState('', '', route);
        var navLinks = document.querySelectorAll('.header nav a');
        var currentLink = document.querySelector(".header nav a[href*=\"" + route + "\"]");
        for (var _i = 0, navLinks_1 = navLinks; _i < navLinks_1.length; _i++) {
            var item = navLinks_1[_i];
            item.classList.remove('active');
        }
        currentLink.classList.add('active');
    }
}
// route
var route = {
    value: '',
    animationStatus: '',
    // previous: 'home',
    changed: function () {
        console.warn("The route has changed to " + this.current);
        navHandler(this.current);
    },
    statusChanged: function () {
        console.warn("Animation is " + this.status);
        // loader.classList.toggle('active');
    },
    get current() {
        return this.value;
    },
    set current(value) {
        this.value = value;
        this.changed();
    },
    get status() {
        return this.animationStatus;
    },
    set status(animationStatus) {
        this.animationStatus = animationStatus;
        this.statusChanged();
    }
};
// helper functions
jQuery(window).on('load', function () {
    jQuery('#page-loader').fadeOut();
});
function calcScreenOffset() {
    var intViewportHeight = window.innerHeight;
    var linesHeight = 1337;
    var linesOffset = 100;
    var offset = (intViewportHeight - linesHeight + linesOffset);
    return offset;
}
function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    return (rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth));
}
function homeLogo() {
    var home = document.querySelector('#home-1');
    var logo = document.querySelector('.top-bar .logo');
    if (home && isElementInViewport(home)) {
        logo.classList.add('hide');
    }
    else {
        logo.classList.remove('hide');
    }
}
// const routes = ['home', 'audit', 'clients', 'clients2', 'clients3', 'clients4', 'clients5', 'clients6'];
// function getPrevRoute(value) {
//   const current = routes.indexOf(value);
//   console.warn(current - 1);
//   if (current) {
//     return current - 1;
//   } else {
//     return 0;
//   }
// }
var xDown = null;
var yDown = null;
function handleTouchStart(event) {
    xDown = event.touches[0].clientX;
    yDown = event.touches[0].clientY;
}
;
window.addEventListener('touchstart', handleTouchStart, false);
function handleTouchMove(event) {
    var delta = 0;
    var xUp = event.touches[0].clientX;
    var yUp = event.touches[0].clientY;
    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;
    if (Math.abs(xDiff) > Math.abs(yDiff)) { /*most significant*/
        if (xDiff > 0) {
            /* left swipe */
            delta = 1;
        }
        else {
            /* right swipe */
        }
    }
    else {
        if (yDiff > 0) {
            /* up swipe */
            delta = 1;
        }
        else {
            /* down swipe */
        }
    }
    return delta;
}
;
var step = 0;
var timelines = [];
var back = document.querySelector('.go-back');
var next = document.querySelector('.go-next');
var nextLink = document.querySelector('.go-next a');
var scrollsToProceed = 0;
function playAnimation(timeline, reverse, current, remove, add, button, light, fast, duration, speed, arrow, overflow) {
    if (speed === void 0) { speed = -1; }
    if (arrow === void 0) { arrow = false; }
    // console.log([timeline, reverse, current, remove, add, button, light, fast, duration, speed, arrow]);
    if (!route.status && !mobileDevice.phone()) {
        if (reverse && speed) {
            scrollsToProceed = scrollsToProceed + 1;
            if (scrollsToProceed < 15) {
                console.log('scroll more ' + scrollsToProceed);
                return;
            }
        }
        route.status = true;
        if (reverse) {
            step = step - 1;
            timeline.reverse();
            body.classList.remove("animation-" + route.current.replace('/', ''));
            if (speed) {
                timeline.duration = speed;
                timeline.reversed = false;
                timeline.reverse();
                console.warn('stagger complete');
                scrollsToProceed = 0;
            }
        }
        else {
            step = step + 1;
            timeline.reversed = false;
            timeline.duration = duration;
        }
        if (fast) {
            if (light) {
                header.classList.add('header-light');
            }
            else {
                header.classList.remove('header-light');
            }
        }
        timeline.play();
        timeline.begin = function () {
            console.warn('fire begin');
            if (button) {
                button.classList.add('active');
            }
        };
        timeline.complete = function () {
            console.warn('fire complete');
            route.status = false;
            homeLogo();
            if (current) {
                route.current = current;
            }
            if (remove) {
                window.removeEventListener('wheel', remove);
                window.removeEventListener('touchmove', remove);
            }
            if (add) {
                window.addEventListener('wheel', add);
                window.addEventListener('touchmove', add);
            }
            if (button) {
                button.classList.remove('active');
            }
            if (light) {
                header.classList.add('header-light');
            }
            else {
                header.classList.remove('header-light');
            }
            if (arrow) {
                next.classList.add('active');
            }
            else {
                next.classList.remove('active');
            }
            if (step) {
                back.classList.add('active');
            }
            else {
                back.classList.remove('active');
            }
            setNextStep(reverse);
            body.classList.add("animation-" + route.current.replace('/', ''));
            // set previous timeline settings for back button
            if (reverse) {
                timelines.pop();
            }
            else {
                timelines.push([timeline, true, current, add, remove, button, light, fast, duration, speed, arrow]); // switching add/remove events
            }
            setBlogVisibility();
            handleBlogScrollbars();
        };
        if (overflow) {
            body.classList.add('overflow-auto');
            body.classList.add('white-board');
        }
        else {
            body.classList.remove('overflow-auto');
            body.classList.remove('white-board');
        }
    }
    else {
        console.log('wait... still plays', mobileDevice.phone());
    }
}
var blog = document.querySelector('.new-blog');
var isBlogVisible = false;
function setBlogVisibility() {
    if (blog) {
        if (parseInt(blog.style.opacity)) {
            isBlogVisible = true;
        }
        else {
            isBlogVisible = false;
        }
    }
}
function handleBlogScrollbars(click) {
    var overrideBody = document.querySelector('body');
    var overrideMain = document.querySelector('main');
    if (blog) {
        if (isBlogVisible) {
            overrideBody.style.setProperty('overflow', 'auto');
            overrideMain.style.setProperty('position', 'static');
            overrideMain.style.setProperty('top', 'auto');
            // overrideMain.style.setProperty('left', 'auto');
            overrideMain.style.setProperty('height', 'auto');
            overrideMain.style.setProperty('margin-top', 0);
            blog.style.setProperty('margin-top', 0);
        }
        else {
            overrideBody.style.setProperty('overflow', 'hidden');
            overrideMain.style.setProperty('position', 'fixed');
            overrideMain.style.setProperty('top', 0);
            // overrideMain.style.setProperty('left', 0);
            overrideMain.style.setProperty('width', '100%');
            overrideMain.style.setProperty('height', '100%');
            blog.style.setProperty('margin-top', '76px');
        }
        if (click && isBlogVisible) {
            overrideBody.style.setProperty('overflow', 'visible', 'important');
            window.scrollTo(0, 0);
        }
    }
}
var firstTimelineLight;
var firstTimelineArrow;
back.addEventListener('click', function (e) {
    e.preventDefault();
    setBlogVisibility();
    handleBlogScrollbars(true);
    var ancestorTimeline = timelines[timelines.length - 2];
    var previousTimeline = timelines[timelines.length - 1];
    previousTimeline[6] = (ancestorTimeline) ? ancestorTimeline[6] : firstTimelineLight;
    previousTimeline[10] = (ancestorTimeline) ? ancestorTimeline[10] : firstTimelineArrow;
    console.log(previousTimeline);
    scrollsToProceed = 15;
    playAnimation.apply(void 0, previousTimeline);
});
var nextTimelines;
var nextAnimations;
var nextStep = 0;
var prevNextTimelines = [];
var prevNextAnimations = [];
next.addEventListener('click', function (e) {
    e.preventDefault();
    if (!route.status && !mobileDevice.phone()) {
        if (isMiltiArray(nextTimelines)) {
            applyAnimation(nextAnimations[nextStep], nextTimelines[nextStep][0]);
            playAnimation.apply(void 0, nextTimelines[nextStep]);
        }
        else {
            playAnimation.apply(void 0, nextTimelines);
        }
    }
});
function isMiltiArray(arr) {
    if (arr) {
        return arr[0].constructor === Array;
    }
    else {
        return false;
    }
}
function applyAnimation(animation, timeline) {
    if (animation) {
        Array.prototype.forEach.call(animation, function (e) { return timeline.add(e[0], e[1]); });
    }
}
function setNextTimeline(timelines, animations, step) {
    if (nextTimelines) {
        prevNextTimelines.push(nextTimelines);
    }
    if (nextAnimations) {
        prevNextAnimations.push(nextAnimations);
    }
    next.classList.add('active');
    nextTimelines = timelines;
    nextAnimations = animations;
    if (step) {
        nextStep = -1;
    }
}
function setNextStep(reverse) {
    if (reverse === void 0) { reverse = false; }
    if (isMiltiArray(nextTimelines)) {
        if (reverse && nextStep > -1) {
            nextStep = nextStep - 1;
            if (nextStep == -1) {
                if (prevNextTimelines) {
                    nextTimelines = prevNextTimelines.pop();
                }
                if (prevNextAnimations) {
                    nextAnimations = prevNextAnimations.pop();
                }
            }
        }
        else {
            if (nextStep < nextTimelines.length) {
                nextStep = nextStep + 1;
                if (nextStep == nextTimelines.length) {
                    next.classList.remove('active');
                }
            }
        }
    }
    console.log(nextStep);
}
// Override browser's back button
var isBlogSingle = document.querySelector('.single-post');
if (!mobileDevice.phone() && !isBlogSingle) {
    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function (event) {
        history.pushState(null, document.title, location.href);
    });
    window.onpopstate = function () {
        if (back.classList.contains('active')) {
            back.click();
        }
    };
}
// toggle menu
headerNav.addEventListener('click', function () {
    header.classList.toggle('opened');
});
// scroll to anchor
Array.prototype.forEach.call(document.querySelectorAll('a[href^="#"]'), function (anchor) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        if (this.getAttribute('href') !== '#back' || this.getAttribute('href') !== '#next') {
            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        }
    });
});
// fetch json feed
function getFeed(url) {
    return __awaiter(this, void 0, void 0, function () {
        var response, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, fetch(url)];
                case 1:
                    response = _a.sent();
                    return [4 /*yield*/, response.json()];
                case 2:
                    data = _a.sent();
                    return [2 /*return*/, data];
            }
        });
    });
}
jQuery(window).load(function () {
    // set first timeline
    var header = document.querySelector('.header');
    firstTimelineLight = (header.classList.contains('header-light')) ? true : false;
    firstTimelineArrow = (next.classList.contains('active')) ? true : false;
});
jQuery(document).ready(function ($) {
    // shave blog content
    function shaveBlogs() {
        var blogPosts = document.querySelectorAll('.blog-post');
        Array.prototype.forEach.call(blogPosts, function (blogPost) {
            var postHeading = (blogPost.querySelector('h2')) ? blogPost.querySelector('h2').offsetHeight : blogPost.querySelector('h4').offsetHeight;
            var postContent = blogPost.querySelector('p');
            shave(postContent, 100 - postHeading);
        });
    }
    // fetch instagram images
    function loadInstagram() {
        var instagram = document.querySelector('.insta');
        var feed = (instagram.dataset.feed) ? instagram.dataset.feed : 'https://www.instagram.com/makosi_consulting/';
        getFeed(feed + "?__a=1").then(function (data) {
            if (data) {
                var images = data.graphql.user.edge_owner_to_timeline_media.edges;
                if (images) {
                    instagram.innerHTML = '';
                    images.slice(0, 15).map(function (image) {
                        var url = "https://www.instagram.com/p/" + image.node.shortcode + "/";
                        var title = (image.node.edge_media_to_caption && image.node.edge_media_to_caption.edges[0] && image.node.edge_media_to_caption.edges[0].node && image.node.edge_media_to_caption.edges[0].node.text) ? image.node.edge_media_to_caption.edges[0].node.text : 'Makosi';
                        var thumbnail = (image.node.thumbnail_resources[1].src) ? image.node.thumbnail_resources[1].src : '/wp-content/themes/makosi/images/insta-8@2x.jpg';
                        instagram.innerHTML += "\n              <a href=\"" + url + "\" target=\"_blank\">\n                <img src=\"" + thumbnail + "\" alt=\"" + title + "\">\n              </a>\n            ";
                    });
                }
            }
        });
    }
    // get blog content
    function getBlog(remove) {
        $('#page-loader').show();
        if (remove) {
            window.removeEventListener('wheel', remove);
            window.removeEventListener('touchmove', remove);
        }
        $('main').load('/blog .new-blog', function () {
            $('main').attr('class', 'main main-new-blog');
            $('body').attr('class', 'page-template page-template-template-new-blog page-template-template-new-blog-php page page-parent overflow-auto');
            $('header').addClass('header-light');
            $('#go-back').hide();
            route.current = '/blog';
            $('#page-loader').hide();
            shaveBlogs();
            loadInstagram();
        });
    }
    if (mobileDevice.phone()) {
        html.classList.add('mobile-device');
    }
    var isHome = document.querySelector('#home-1');
    if (isHome) {
        // Home functions
        var moreButton_1 = document.querySelector('#more');
        var homeTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        homeLogo();
        if (!homeTimeline_1.children.length) {
            homeTimeline_1.add({
                targets: '.lines svg',
                opacity: [0, 1],
                bottom: [
                    {
                        value: [-700, calcScreenOffset()],
                        duration: 3000,
                        delay: 0
                    }
                ],
                right: [
                    {
                        value: [-1150, -1380],
                        duration: 3000,
                        delay: 0
                    }
                ],
                duration: 100
            }, 0).add({
                targets: '.lines .top-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250; },
                duration: 3000
            }, 0).add({
                targets: '.lines .bot-line path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250; },
                duration: 3000
            }, 0).add({
                targets: '.lines .hor-line-1 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250; },
                duration: 3000
            }, 0).add({
                targets: '.lines .hor-line-2 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250; },
                duration: 3000
            }, 0).add({
                targets: '.lines .hor-line-3 path',
                strokeDashoffset: [anime.setDashoffset, 0],
                delay: function (el, i) { return i * 250; },
                duration: 3000
            }, 0).add({
                targets: ['#home-1', '.lines'],
                translateX: ['0', '-100%'],
                duration: 2000
            }, 1000).add({
                targets: '#home-2',
                translateX: ['100%', '0'],
                duration: 2000
            }, 1000);
        }
        // set home next timeline
        setNextTimeline([homeTimeline_1, false, '/', homeScroll, homeScroll2, false, false, false, 3000, -1, false]);
        function homeScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(homeTimeline_1, false, '/', homeScroll, homeScroll2, false, false, false, 3000, -1, false);
            }
        }
        function homeScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta != '1') {
                playAnimation(homeTimeline_1, true, '/', homeScroll2, homeScroll, false, false, false, 3000, -1, true);
            }
        }
        function animateHome(event) {
            event.preventDefault();
            playAnimation(homeTimeline_1, false, '/', homeScroll, homeScroll2, moreButton_1, false, false, 3000, -1, false);
        }
        window.addEventListener('wheel', homeScroll);
        window.addEventListener('touchmove', homeScroll);
        if (moreButton_1) {
            moreButton_1.addEventListener('click', animateHome);
        }
        // Audit
        var auditButton_1 = document.querySelector('#audit');
        var auditTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        function auditScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta != '1') {
                if (isHome) {
                    playAnimation(auditTimeline_1, true, '/', auditScroll, homeScroll2, auditButton_1, false, false, 1000);
                }
            }
        }
        function animateAudit(event) {
            if (auditButton_1) {
                event.preventDefault();
            }
            if (!auditTimeline_1.children.length) {
                auditTimeline_1.add({
                    targets: '.lines .rest',
                    opacity: [0, 1],
                    duration: 1000
                }, 0).add({
                    targets: '.lines',
                    translateX: '-100%',
                    translateY: '-50%',
                    right: '100px',
                    duration: 1000
                }, 0).add({
                    targets: '#home-2',
                    translateY: [0, '-100%'],
                    duration: 1000
                }, 0).add({
                    targets: '#audit-1',
                    translateY: ['100%', '-100%'],
                    duration: 1000
                }, 0);
            }
            playAnimation(auditTimeline_1, false, '/audit', homeScroll2, auditScroll, auditButton_1, false, false, 1000);
        }
        if (auditButton_1) {
            auditButton_1.addEventListener('click', animateAudit);
        }
        // Audit Clients
        var auditClientsButton_1 = document.querySelector('#audit-clients');
        var auditClientsTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline4_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline5_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline6_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline7_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        if (!auditClientsTimeline2_1.children.length) {
            auditClientsTimeline2_1.add({
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000
            }, 0).add({
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000
            }, 0).add({
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000
            }, 0).add({
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000
            }, 1000).add({
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000
            }, 2000).add({
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000
            }, 3000);
        }
        function animateAuditClients(event) {
            if (auditClientsButton_1) {
                event.preventDefault();
            }
            if (!auditClientsTimeline_1.children.length) {
                auditClientsTimeline_1.add({
                    targets: '.top-blob, #audit-clients-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0);
            }
            playAnimation(auditClientsTimeline_1, false, '/audit/clients', auditScroll, auditClientsScroll, auditClientsButton_1, false, false, 1000, -1, true);
            // set audit clients next timeline
            setNextTimeline([
                [auditClientsTimeline2_1, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
                [auditClientsTimeline3_1, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
                [auditClientsTimeline4_1, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
                [auditClientsTimeline5_1, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
                [auditClientsTimeline6_1, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
                [auditClientsTimeline7_1, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
            ], [
                [
                    [
                        {
                            targets: '.welcome-content',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section .fact-0',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '.why-section .fact-1',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '.why-section .fact-2',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ]
                ],
                [
                    [
                        {
                            targets: '.top-blob',
                            top: ['0', '100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .side-title',
                            top: ['150%', '50%'],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts h2',
                            opacity: [0, 0.25],
                            borderBottom: '1px solid #fff',
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-0 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-1 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-2 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        6000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-3 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        6000
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.left-blob',
                            translateX: ['100%', '-50%'],
                            duration: 1500
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-3',
                            translateX: ['100%', '0'],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1000
                        },
                        500
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-3',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-3',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-4',
                            translateX: ['100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .first-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .second-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .third-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#audit-clients-4 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .industries',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '.lines-single .top-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .bot-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-1 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-2 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-3 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ]
                ],
                [
                    [
                        {
                            targets: '#audit-clients-5',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0
                    ]
                ],
                [
                    [
                        {
                            targets: '.new-blog',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#home-1, #audit-1',
                            height: ['100vh', 0],
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.new-blog',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ]
                ],
            ], true);
        }
        function auditClientsScroll7(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end of audit clients
                console.log('end audit clients animations');
            }
            else {
                console.log('no scrolling events inside blog');
            }
        }
        function auditClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline7_1.children.length) {
                    auditClientsTimeline7_1.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#home-1, #audit-1',
                        height: ['100vh', 0],
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditClientsTimeline7_1, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(auditClientsTimeline6_1, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
            }
        }
        function auditClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline6_1.children.length) {
                    auditClientsTimeline6_1.add({
                        targets: '#audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditClientsTimeline6_1, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline5_1, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
            }
        }
        function auditClientsScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline5_1.children.length) {
                    auditClientsTimeline5_1.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0);
                }
                playAnimation(auditClientsTimeline5_1, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline4_1, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
            }
        }
        function auditClientsScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline4_1.children.length) {
                    auditClientsTimeline4_1.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    }, 0).add({
                        targets: '#audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 500);
                }
                playAnimation(auditClientsTimeline4_1, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline3_1, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
            }
        }
        function auditClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline3_1.children.length) {
                    auditClientsTimeline3_1.add({
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 3000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000);
                }
                playAnimation(auditClientsTimeline3_1, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline2_1, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
            }
        }
        function auditClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(auditClientsTimeline2_1, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline_1, true, '/audit', auditClientsScroll, auditScroll, auditClientsButton_1, false, false, 1000);
            }
        }
        if (auditClientsButton_1) {
            auditClientsButton_1.addEventListener('click', animateAuditClients);
        }
        // Audit Candidates
        var auditCandidatesTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline4_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline5_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesButton_1 = document.querySelector('#audit-candidates');
        function auditCandidatesScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end audit candidates
                console.log('end audit candidates');
            }
            else {
                playAnimation(auditCandidatesTimeline5_1, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
            }
        }
        function auditCandidatesScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline5_1.children.length) {
                    auditCandidatesTimeline5_1.add({
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000).add({
                        targets: '#audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(auditCandidatesTimeline5_1, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline4_1, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
            }
        }
        function auditCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline4_1.children.length) {
                    auditCandidatesTimeline4_1.add({
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
                        duration: 2000
                    }, 0);
                }
                playAnimation(auditCandidatesTimeline4_1, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline3_1, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
            }
        }
        function auditCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline3_1.children.length) {
                    auditCandidatesTimeline3_1.add({
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditCandidatesTimeline3_1, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline2_1, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
            }
        }
        function auditCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline2_1.children.length) {
                    auditCandidatesTimeline2_1.add({
                        targets: '#audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000);
                }
                playAnimation(auditCandidatesTimeline2_1, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline_1, true, '/audit', auditCandidatesScroll, auditScroll, auditCandidatesButton_1, false, false, 2000);
            }
        }
        function animateCandidates(event) {
            if (auditCandidatesButton_1) {
                event.preventDefault();
            }
            if (!auditCandidatesTimeline_1.children.length) {
                auditCandidatesTimeline_1.add({
                    targets: '.top-blob, #audit-candidates-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0).add({
                    targets: '#audit-candidates-1-2',
                    translateX: ['-100%', '0'],
                    duration: 1000
                }, 1000);
            }
            playAnimation(auditCandidatesTimeline_1, false, '/audit/applicants', auditScroll, auditCandidatesScroll, auditCandidatesButton_1, false, false, 2000, -1, true);
            // set audit candidates next timeline
            setNextTimeline([
                [auditCandidatesTimeline2_1, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
                [auditCandidatesTimeline3_1, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
                [auditCandidatesTimeline4_1, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
                [auditCandidatesTimeline5_1, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
            ], [
                [
                    [{
                            targets: '#audit-candidates-1-1',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-1-2',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-1-3',
                            translateX: ['-100%', '0'],
                            duration: 1000
                        }, 1000]
                ],
                [
                    [{
                            targets: '.top-blob',
                            top: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-1',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0]
                ],
                [
                    [{
                            targets: '#audit-candidates-2 .slide-title-top',
                            translateX: ['-100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2 .slide-title-bottom',
                            translateX: ['100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2 .slide-title',
                            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
                            duration: 2000
                        }, 0]
                ],
                [
                    [{
                            targets: '#audit-candidates-2 .slide-title-top',
                            translateX: ['0', '100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2 .slide-title-bottom',
                            translateX: ['0', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-3',
                            opacity: [0, 1],
                            zIndex: {
                                value: [1, 3],
                                round: true
                            },
                            duration: 1
                        }, 1000],
                    [{
                            targets: '#audit-candidates-3 .before',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#audit-candidates-3 .after',
                            translateX: ['0', '100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#audit-candidates-3 .call-to-action',
                            opacity: ['0', '1'],
                            duration: 1000
                        }, 2000]
                ]
            ], true);
        }
        if (auditCandidatesButton_1) {
            auditCandidatesButton_1.addEventListener('click', animateCandidates);
        }
        // new start
        // itAudit
        var itAuditButton_1 = document.querySelector('#it-audit');
        var itAuditTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        function itAuditScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta != '1') {
                if (isHome) {
                    playAnimation(itAuditTimeline_1, true, '/', itAuditScroll, homeScroll2, itAuditButton_1, false, false, 1000);
                }
            }
        }
        function animateItAudit(event) {
            if (itAuditButton_1) {
                event.preventDefault();
            }
            if (!itAuditTimeline_1.children.length) {
                itAuditTimeline_1.add({
                    targets: '.lines .rest',
                    opacity: [0, 1],
                    duration: 1000
                }, 0).add({
                    targets: '.lines',
                    translateX: '-100%',
                    translateY: '-50%',
                    right: '100px',
                    duration: 1000
                }, 0).add({
                    targets: '#home-2',
                    translateY: [0, '-100%'],
                    duration: 1000
                }, 0).add({
                    targets: '#it-audit-1',
                    translateY: ['100%', '-100%'],
                    duration: 1000
                }, 0);
            }
            playAnimation(itAuditTimeline_1, false, '/it-audit', homeScroll2, itAuditScroll, itAuditButton_1, false, false, 1000);
        }
        if (itAuditButton_1) {
            itAuditButton_1.addEventListener('click', animateItAudit);
        }
        // itAudit Clients
        var itAuditClientsButton_1 = document.querySelector('#it-audit-clients');
        var itAuditClientsTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline4_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline5_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline6_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline7_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        if (!itAuditClientsTimeline2_1.children.length) {
            itAuditClientsTimeline2_1.add({
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000
            }, 0).add({
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000
            }, 0).add({
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000
            }, 0).add({
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000
            }, 1000).add({
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000
            }, 2000).add({
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000
            }, 3000);
        }
        function animateItAuditClients(event) {
            if (itAuditClientsButton_1) {
                event.preventDefault();
            }
            if (!itAuditClientsTimeline_1.children.length) {
                itAuditClientsTimeline_1.add({
                    targets: '.top-blob, #it-audit-clients-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0);
            }
            playAnimation(itAuditClientsTimeline_1, false, '/it-audit/clients', itAuditScroll, itAuditClientsScroll, itAuditClientsButton_1, false, false, 1000, -1, true);
            // set itAudit clients next timeline
            setNextTimeline([
                [itAuditClientsTimeline2_1, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
                [itAuditClientsTimeline3_1, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
                [itAuditClientsTimeline4_1, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
                [itAuditClientsTimeline5_1, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
                [itAuditClientsTimeline6_1, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
                [itAuditClientsTimeline7_1, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
            ], [
                [
                    [
                        {
                            targets: '.welcome-content',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section .fact-0',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '.why-section .fact-1',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '.why-section .fact-2',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ]
                ],
                [
                    [
                        {
                            targets: '.top-blob',
                            top: ['0', '100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .side-title',
                            top: ['150%', '50%'],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts h2',
                            opacity: [0, 0.25],
                            borderBottom: '1px solid #fff',
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        6000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        6000
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.left-blob',
                            translateX: ['100%', '-50%'],
                            duration: 1500
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-3',
                            translateX: ['100%', '0'],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1000
                        },
                        500
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-3',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-3',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4',
                            translateX: ['100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .first-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .second-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .third-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .industries',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '.lines-single .top-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .bot-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-1 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-2 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-3 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ]
                ],
                [
                    [
                        {
                            targets: '#it-audit-clients-5',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0
                    ]
                ],
                [
                    [
                        {
                            targets: '.new-blog',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#home-1, #it-audit-1',
                            height: ['100vh', 0],
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.new-blog',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ]
                ],
            ], true);
        }
        function itAuditClientsScroll7(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end of itAudit clients
                console.log('end itAudit clients animations');
            }
            else {
                console.log('no scrolling events inside blog');
            }
        }
        function itAuditClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline7_1.children.length) {
                    itAuditClientsTimeline7_1.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#home-1, #it-audit-1, #audit-1',
                        height: ['100vh', 0],
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline7_1, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(itAuditClientsTimeline6_1, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
            }
        }
        function itAuditClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline6_1.children.length) {
                    itAuditClientsTimeline6_1.add({
                        targets: '#it-audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline6_1, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline5_1, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
            }
        }
        function itAuditClientsScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline5_1.children.length) {
                    itAuditClientsTimeline5_1.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline5_1, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline4_1, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
            }
        }
        function itAuditClientsScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline4_1.children.length) {
                    itAuditClientsTimeline4_1.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 500);
                }
                playAnimation(itAuditClientsTimeline4_1, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline3_1, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
            }
        }
        function itAuditClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline3_1.children.length) {
                    itAuditClientsTimeline3_1.add({
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 3000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000);
                }
                playAnimation(itAuditClientsTimeline3_1, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline2_1, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
            }
        }
        function itAuditClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(itAuditClientsTimeline2_1, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline_1, true, '/it-audit', itAuditClientsScroll, itAuditScroll, itAuditClientsButton_1, false, false, 1000);
            }
        }
        if (itAuditClientsButton_1) {
            itAuditClientsButton_1.addEventListener('click', animateItAuditClients);
        }
        // itAudit Candidates
        var itAuditCandidatesTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline4_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline5_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesButton_1 = document.querySelector('#it-audit-candidates');
        function itAuditCandidatesScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end itAudit candidates
                console.log('end itAudit candidates');
            }
            else {
                playAnimation(itAuditCandidatesTimeline5_1, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
            }
        }
        function itAuditCandidatesScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline5_1.children.length) {
                    itAuditCandidatesTimeline5_1.add({
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(itAuditCandidatesTimeline5_1, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline4_1, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
            }
        }
        function itAuditCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline4_1.children.length) {
                    itAuditCandidatesTimeline4_1.add({
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
                        duration: 2000
                    }, 0);
                }
                playAnimation(itAuditCandidatesTimeline4_1, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline3_1, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
            }
        }
        function itAuditCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline3_1.children.length) {
                    itAuditCandidatesTimeline3_1.add({
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditCandidatesTimeline3_1, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline2_1, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
            }
        }
        function itAuditCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline2_1.children.length) {
                    itAuditCandidatesTimeline2_1.add({
                        targets: '#it-audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000);
                }
                playAnimation(itAuditCandidatesTimeline2_1, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline_1, true, '/it-audit', itAuditCandidatesScroll, itAuditScroll, itAuditCandidatesButton_1, false, false, 2000);
            }
        }
        function animateItCandidates(event) {
            if (itAuditCandidatesButton_1) {
                event.preventDefault();
            }
            if (!itAuditCandidatesTimeline_1.children.length) {
                itAuditCandidatesTimeline_1.add({
                    targets: '.top-blob, #it-audit-candidates-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0).add({
                    targets: '#it-audit-candidates-1-2',
                    translateX: ['-100%', '0'],
                    duration: 1000
                }, 1000);
            }
            playAnimation(itAuditCandidatesTimeline_1, false, '/it-audit/applicants', itAuditScroll, itAuditCandidatesScroll, itAuditCandidatesButton_1, false, false, 2000, -1, true);
            // set itAudit candidates next timeline
            setNextTimeline([
                [itAuditCandidatesTimeline2_1, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
                [itAuditCandidatesTimeline3_1, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
                [itAuditCandidatesTimeline4_1, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
                [itAuditCandidatesTimeline5_1, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
            ], [
                [
                    [{
                            targets: '#it-audit-candidates-1-1',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-1-2',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-1-3',
                            translateX: ['-100%', '0'],
                            duration: 1000
                        }, 1000]
                ],
                [
                    [{
                            targets: '.top-blob',
                            top: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-1',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0]
                ],
                [
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-top',
                            translateX: ['-100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-bottom',
                            translateX: ['100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2 .slide-title',
                            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
                            duration: 2000
                        }, 0]
                ],
                [
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-top',
                            translateX: ['0', '100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-bottom',
                            translateX: ['0', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-3',
                            opacity: [0, 1],
                            zIndex: {
                                value: [1, 3],
                                round: true
                            },
                            duration: 1
                        }, 1000],
                    [{
                            targets: '#it-audit-candidates-3 .before',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#it-audit-candidates-3 .after',
                            translateX: ['0', '100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#it-audit-candidates-3 .call-to-action',
                            opacity: ['0', '1'],
                            duration: 1000
                        }, 2000]
                ]
            ], true);
        }
        if (itAuditCandidatesButton_1) {
            itAuditCandidatesButton_1.addEventListener('click', animateItCandidates);
        }
        // new end
        // Tax
        var taxButton_1 = document.querySelector('#tax');
        var taxTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        function taxScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta != '1') {
                if (isHome) {
                    playAnimation(taxTimeline_1, true, '/', taxScroll, homeScroll2, taxButton_1, false, false, 1000);
                }
            }
        }
        function animateTax(event) {
            if (taxButton_1) {
                event.preventDefault();
            }
            if (!taxTimeline_1.children.length) {
                taxTimeline_1.add({
                    targets: '.lines .rest',
                    opacity: [0, 1],
                    duration: 1000
                }, 0).add({
                    targets: '.lines',
                    translateX: '-225%',
                    translateY: '-75%',
                    scale: 2,
                    duration: 1000
                }, 0).add({
                    targets: '#home-2',
                    translateX: [0, '-100%'],
                    translateY: [0, '-100%'],
                    duration: 1000
                }, 0).add({
                    targets: '#tax-1',
                    translateX: ['100%', '0'],
                    duration: 1000
                }, 0);
            }
            playAnimation(taxTimeline_1, false, '/tax', homeScroll2, taxScroll, taxButton_1, false, false, 1000);
        }
        if (taxButton_1) {
            taxButton_1.addEventListener('click', animateTax);
        }
        // Tax Clients
        var taxClientsButton_1 = document.querySelector('#tax-clients');
        var taxClientsTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxClientsTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // let taxClientsTimeline3 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        // let taxClientsTimeline4 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        var taxClientsTimeline5_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxClientsTimeline6_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        function taxClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end tax clients
                console.log('end tax clients');
            }
            else {
                console.log('no scrolling inside blog');
            }
        }
        function taxClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxClientsTimeline6_1.children.length) {
                    taxClientsTimeline6_1.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#home-1, #audit-1, #tax-1',
                        height: ['100vh', 0],
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(taxClientsTimeline6_1, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(taxClientsTimeline5_1, true, 'tax/clients2', taxClientsScroll5, taxClientsScroll2, false, false, false, 1600, -1, true);
            }
        }
        // function taxClientsScroll4(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // end tax clients
        //    console.log('end tax clients');
        //  } else {
        //    playAnimation(taxClientsTimeline4, true, 'tax/clients3', taxClientsScroll4, taxClientsScroll3, false, true);
        //  }
        // }
        // function taxClientsScroll3(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // taxClientsTimeline4.add({
        //    //  targets: '.left-blob',
        //    //  opacity: 0,
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-3',
        //    //  zIndex: {
        //    //    value: [3, 2],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '.left-blob-blue',
        //    //  zIndex: {
        //    //    value: [2, 3],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-4',
        //    //  zIndex: {
        //    //    value: [2, 3],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '.left-blob-blue',
        //    //  translateX: ['100%', '-75%'],
        //    //  duration: 1500,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-4',
        //    //  translateX: ['100%', '0'],
        //    //  duration: 1000,
        //    //  offset: 600
        //    // });
        //     // playAnimation(taxClientsTimeline4, false, 'tax/clients4', taxClientsScroll3, taxClientsScroll4);
        //     // end tax clients
        //    console.log('end tax clients');
        //  } else {
        //    playAnimation(taxClientsTimeline3, true, 'tax/clients2', taxClientsScroll3, taxClientsScroll2);
        //  }
        // }
        function taxClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxClientsTimeline3.add({
                //  targets: '.bottom-blob',
                //  opacity: 0,
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '.left-blob',
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '#tax-clients-2',
                //  zIndex: {
                //    value: [3, 2],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '.left-blob',
                //  translateX: ['100%', '-50%'],
                //  duration: 1500,
                //  offset: 0
                // }).add({
                //  targets: '#tax-clients-3',
                //  translateX: ['100%', '0'],
                //  duration: 1000,
                //  offset: 500,
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                // });
                // playAnimation(taxClientsTimeline3, false, 'tax/clients3', taxClientsScroll2, taxClientsScroll3, false, true);
                // end tax clients
                // console.log('end tax clients');
                if (!taxClientsTimeline5_1.children.length) {
                    taxClientsTimeline5_1.add({
                        targets: '.bottom-blob',
                        opacity: 0,
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-2',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-2',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 600).add({
                        targets: '#tax-clients-5',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-5',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 600);
                }
                playAnimation(taxClientsTimeline5_1, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true);
            }
            else {
                playAnimation(taxClientsTimeline2_1, true, '/tax/clients', taxClientsScroll2, taxClientsScroll, false, true, false, 8000, -1, true);
            }
        }
        function taxClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxClientsTimeline2_1.children.length) {
                    taxClientsTimeline2_1.add({
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#tax-clients-2',
                        translateY: ['100%', '0'],
                        duration: 1000,
                        zIndex: {
                            value: [2, 3],
                            round: true
                        }
                    }, 1000).add({
                        targets: '#tax-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 2000).add({
                        targets: '#tax-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 3000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 7000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 7000);
                }
                playAnimation(taxClientsTimeline2_1, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true);
            }
            else {
                playAnimation(taxClientsTimeline_1, true, '/tax', taxClientsScroll, taxScroll, taxClientsButton_1, false, true, 4500);
            }
        }
        function animateTaxClients(event) {
            if (taxClientsButton_1) {
                event.preventDefault();
            }
            if (!taxClientsTimeline_1.children.length) {
                taxClientsTimeline_1.add({
                    targets: '.lines',
                    opacity: [1, 0],
                    duration: 1000
                }, 0).add({
                    targets: '#tax-1',
                    translateX: ['0', '-100%'],
                    duration: 1000
                }, 0).add({
                    targets: '.right-blob',
                    translateX: ['100%', '0'],
                    left: ['0', '-100%'],
                    duration: 2000
                }, 0).add({
                    targets: '.right-blob',
                    opacity: 0,
                    duration: 100
                }, 2000).add({
                    targets: '#tax-clients-1',
                    translateX: ['100%', '0'],
                    duration: 1000
                }, 500).add({
                    targets: '#tax-clients-1 #tax-item-0',
                    opacity: [0, 1],
                    duration: 1000
                }, 1500).add({
                    targets: '#tax-clients-1 #tax-item-1',
                    opacity: [0, 1],
                    duration: 1000
                }, 2500).add({
                    targets: '#tax-clients-1 #tax-item-2',
                    opacity: [0, 1],
                    duration: 1000
                }, 3500);
            }
            playAnimation(taxClientsTimeline_1, false, '/tax/clients', taxScroll, taxClientsScroll, taxClientsButton_1, true, false, 4500, -1, true);
            // set taxt clients next timeline
            setNextTimeline([
                [taxClientsTimeline2_1, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true],
                [taxClientsTimeline5_1, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true],
                [taxClientsTimeline6_1, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true]
            ], [
                [
                    [{
                            targets: '.bottom-blob',
                            translateY: ['100%', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#tax-clients-2',
                            translateY: ['100%', '0'],
                            duration: 1000,
                            zIndex: {
                                value: [2, 3],
                                round: true
                            }
                        }, 1000],
                    [{
                            targets: '#tax-clients-2 .side-title',
                            top: ['150%', '50%'],
                            duration: 1000
                        }, 2000],
                    [{
                            targets: '#tax-clients-2 .audit-facts h2',
                            opacity: [0, 0.25],
                            borderBottom: '1px solid #fff',
                            duration: 1000
                        }, 3000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-0 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 4000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-0 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 4000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-1 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 5000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-1 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 5000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-2 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 6000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-2 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 6000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-3 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 7000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-3 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 7000]
                ],
                [
                    [{
                            targets: '.bottom-blob',
                            opacity: 0,
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-clients-2',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-clients-2',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 600],
                    [{
                            targets: '#tax-clients-5',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-clients-5',
                            translateX: ['100%', '0'],
                            duration: 1000
                        }, 600]
                ],
                [
                    [{
                            targets: '.new-blog',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#home-1, #audit-1, #tax-1',
                            height: ['100vh', 0],
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [{
                            targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
                            opacity: [1, 0],
                            duration: 1
                        }, 0],
                    [{
                            targets: '.new-blog',
                            opacity: [0, 1],
                            duration: 1000
                        }, 0]
                ]
            ], true);
        }
        if (taxClientsButton_1) {
            taxClientsButton_1.addEventListener('click', animateTaxClients);
        }
        // Tax Candidates
        var taxCandidatesButton_1 = document.querySelector('#tax-candidates');
        var taxCandidatesTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxCandidatesTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxCandidatesTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // let taxCandidatesTimeline4 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        // function taxCandidatesScroll4(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // end tax candidates
        //    console.log('end tax candidates');
        //  } else {
        //    playAnimation(taxCandidatesTimeline4, true, 'tax/candidates3', taxCandidatesScroll4, taxCandidatesScroll3);
        //  }
        // }
        function taxCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxCandidatesTimeline4.add({
                //  targets: '#col-0, #col-1',
                //  translateX: ['0', '-100vw'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#col-2, #col-3',
                //  translateX: ['0', '100vw'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#mid-arrow',
                //  opacity: 0,
                //  duration: 100,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3 h1',
                //  translateY: ['0', '-100vh'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3',
                //  zIndex: {
                //    value: [3, 2],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 1000,
                // }).add({
                //  targets: '#tax-candidates-4',
                //  zIndex: {
                //    value: [1, 3],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 1000
                // }).add({
                //  targets: '#tax-candidates-4',
                //  opacity: [0, 1],
                //  duration: 1000,
                //  offset: 1000
                // }).add({
                //  targets: '#tax-candidates-4 .call-to-action',
                //  opacity: [0, 1],
                //  duration: 1000,
                //  offset: 2000
                // });
                // playAnimation(taxCandidatesTimeline4, false, 'tax/candidates4', taxCandidatesScroll3, taxCandidatesScroll4);
                // end tax candidates
                console.log('end tax candidates');
            }
            else {
                playAnimation(taxCandidatesTimeline3_1, true, 'tax/applicants2', taxCandidatesScroll3, taxCandidatesScroll2, false, false, false, 2100, -1, true);
            }
        }
        function taxCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxCandidatesTimeline3.add({
                //  targets: '.bottom-blob',
                //  translateY: ['100%', '-100%'],
                //  duration: 2000,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3',
                //  translateY: ['100%', '0'],
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                //  duration: 1000,
                //  offset: 1000,
                // }).add({
                //  targets: '#col-0 svg, #col-0 h2, #col-0 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 2000
                // }).add({
                //  targets: '#col-1 svg, #col-1 h2, #col-1 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 3000
                // }).add({
                //  targets: '#col-2 svg, #col-2 h2, #col-2 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 4000
                // }).add({
                //  targets: '#col-3 svg, #col-3 h2, #col-3 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 5000
                // });
                if (!taxCandidatesTimeline3_1.children.length) {
                    taxCandidatesTimeline3_1.add({
                        targets: '.right-blob',
                        left: ['-100%', '0'],
                        translateX: ['0', '-100%'],
                        opacity: [0, 1],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-candidates-2',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#tax-candidates-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-candidates-3 .container-fluid',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-candidates-1',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.lines',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-1',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '.right-blob',
                        translateX: ['100%', '0'],
                        left: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '.right-blob',
                        opacity: 0,
                        duration: 100
                    }, 2000).add({
                        targets: '#tax-candidates-1',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 500);
                }
                playAnimation(taxCandidatesTimeline3_1, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true);
            }
            else {
                playAnimation(taxCandidatesTimeline2_1, true, '/tax/applicants', taxCandidatesScroll2, taxCandidatesScroll, false, true, true, 6000, -1, true);
            }
        }
        function taxCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxCandidatesTimeline2_1.children.length) {
                    taxCandidatesTimeline2_1.add({
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#tax-candidates-3',
                        translateY: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 1000).add({
                        targets: '#col-0 svg, #col-0 h2, #col-0 p',
                        opacity: 1,
                        duration: 1000
                    }, 2000).add({
                        targets: '#col-1 svg, #col-1 h2, #col-1 p',
                        opacity: 1,
                        duration: 1000
                    }, 3000).add({
                        targets: '#col-2 svg, #col-2 h2, #col-2 p',
                        opacity: 1,
                        duration: 1000
                    }, 4000).add({
                        targets: '#col-3 svg, #col-3 h2, #col-3 p',
                        opacity: 1,
                        duration: 1000
                    }, 5000);
                }
                playAnimation(taxCandidatesTimeline2_1, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true);
            }
            else {
                playAnimation(taxCandidatesTimeline_1, true, '/tax', taxCandidatesScroll, taxScroll, taxCandidatesButton_1, false, true, 3000);
            }
            // if (delta == '1') {
            // taxCandidatesTimeline2.add({
            //  targets: '#tax-candidates-1',
            //  translateX: ['0', '-100%'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2',
            //  marginLeft: [0, -1],
            //  duration: 1,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2',
            //  translateX: ['100%', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 #love',
            //  translateX: ['200%', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 #tax',
            //  translateX: ['-200vw', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 .content-wrap',
            //  opacity: [0, 1],
            //  duration: 1000,
            //  offset: 1000
            // });
            // playAnimation(taxCandidatesTimeline, false, 'tax/candidates2', taxCandidatesScroll, taxCandidatesScroll2, false, true, true);
            // }
        }
        function animateTaxCandidates(event) {
            if (taxCandidatesButton_1) {
                event.preventDefault();
            }
            if (!taxCandidatesTimeline_1.children.length) {
                taxCandidatesTimeline_1.add({
                    targets: '.lines',
                    opacity: [1, 0],
                    duration: 1000
                }, 0).add({
                    targets: '.right-blob',
                    translateX: ['100%', '0'],
                    left: ['0', '-100%'],
                    duration: 2000
                }, 0).add({
                    targets: '.right-blob',
                    opacity: [1, 0],
                    duration: 100
                }, 2500).add({
                    targets: '#tax-candidates-2',
                    marginLeft: [0, -1],
                    duration: 1
                }, 0).add({
                    targets: '#tax-candidates-2',
                    translateX: ['100%', '0'],
                    duration: 1000
                }, 500).add({
                    targets: '#tax-candidates-2 #love',
                    translateX: ['200%', '0'],
                    duration: 1000
                }, 1000).add({
                    targets: '#tax-candidates-2 #tax',
                    translateX: ['-200vw', '0'],
                    duration: 1000
                }, 1000).add({
                    targets: '#tax-candidates-2 .content-wrap',
                    opacity: [0, 1],
                    duration: 1000
                }, 2000);
            }
            playAnimation(taxCandidatesTimeline_1, false, '/tax/applicants', taxScroll, taxCandidatesScroll, taxCandidatesButton_1, true, false, 3000, -1, true);
            // set tat candidates next timeline
            setNextTimeline([
                [taxCandidatesTimeline2_1, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true],
                [taxCandidatesTimeline3_1, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true]
            ], [
                [
                    [{
                            targets: '.bottom-blob',
                            translateY: ['100%', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#tax-candidates-3',
                            translateY: ['100%', '0'],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#col-0 svg, #col-0 h2, #col-0 p',
                            opacity: 1,
                            duration: 1000
                        }, 2000],
                    [{
                            targets: '#col-1 svg, #col-1 h2, #col-1 p',
                            opacity: 1,
                            duration: 1000
                        }, 3000],
                    [{
                            targets: '#col-2 svg, #col-2 h2, #col-2 p',
                            opacity: 1,
                            duration: 1000
                        }, 4000],
                    [{
                            targets: '#col-3 svg, #col-3 h2, #col-3 p',
                            opacity: 1,
                            duration: 1000
                        }, 5000]
                ],
                [
                    [{
                            targets: '.right-blob',
                            left: ['-100%', '0'],
                            translateX: ['0', '-100%'],
                            opacity: [0, 1],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-candidates-2',
                            opacity: [1, 0],
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-candidates-3',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#tax-candidates-3 .container-fluid',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#tax-candidates-1',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '.lines',
                            opacity: [1, 0],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#tax-1',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '.right-blob',
                            translateX: ['100%', '0'],
                            left: ['0', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '.right-blob',
                            opacity: 0,
                            duration: 100
                        }, 2000],
                    [{
                            targets: '#tax-candidates-1',
                            translateX: ['100%', '0'],
                            duration: 1000
                        }, 500]
                ]
            ], true);
        }
        if (taxCandidatesButton_1) {
            taxCandidatesButton_1.addEventListener('click', animateTaxCandidates);
        }
    }
    var isAbout = document.querySelector('#about-1');
    if (isAbout) {
        // About functions
        route.current = '/about';
        var aboutTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var aboutTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var aboutTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // add about next timeline
        setNextTimeline([
            [aboutTimeline_1, false, 'about-2', aboutScroll, aboutScroll2, false, false, false, 1000, -1, true],
            [aboutTimeline2_1, false, 'about-3', aboutScroll2, aboutScroll3, false, false, false, 2000, -1, true],
            [aboutTimeline3_1, false, 'about-4', aboutScroll3, aboutScroll4, false, false, false, 3000, -1, true]
        ], [
            [
                [{
                        targets: '#about-1 h1 strong',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg',
                        scale: [0.125, 0.2],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path',
                        strokeWidth: [20, 15],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path.middle-ring',
                        strokeWidth: [40, 30],
                        duration: 1000
                    }, 0]
            ],
            [
                [{
                        targets: '#about-1',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#about-2, #about-2 p, #about-2 h2',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000],
                [{
                        targets: '.about-circle svg',
                        scale: [0.2, 0.4],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path',
                        strokeWidth: [15, 10],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path.middle-ring',
                        strokeWidth: [30, 20],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path.user-icon-fill',
                        fill: '#071738',
                        easing: 'linear',
                        duration: 1000
                    }, 0]
            ],
            [
                [{
                        targets: '#about-2',
                        translateY: ['-50%', '-50%'],
                        translateX: ['0', '-100vw'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle',
                        translateX: ['0', '-50vw'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg',
                        scale: [0.4, 1],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path',
                        strokeWidth: [10, 3],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.about-circle svg path.middle-ring',
                        strokeWidth: [20, 6],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#about-3, #about-3 .first p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000],
                [{
                        targets: '#about-3 .large p, #about-3 .btn',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000]
            ]
        ]);
        function aboutScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end about
                console.log('end about');
            }
            else {
                playAnimation(aboutTimeline3_1, true, 'about-3', aboutScroll4, aboutScroll3, false, false, false, 3000, -1, true);
            }
        }
        function aboutScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!aboutTimeline3_1.children.length) {
                    aboutTimeline3_1.add({
                        targets: '#about-2',
                        translateY: ['-50%', '-50%'],
                        translateX: ['0', '-100vw'],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle',
                        translateX: ['0', '-50vw'],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg',
                        scale: [0.4, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path',
                        strokeWidth: [10, 3],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path.middle-ring',
                        strokeWidth: [20, 6],
                        duration: 1000
                    }, 0).add({
                        targets: '#about-3, #about-3 .first p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#about-3 .large p, #about-3 .btn',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(aboutTimeline3_1, false, 'about-4', aboutScroll3, aboutScroll4, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(aboutTimeline2_1, true, 'about-2', aboutScroll3, aboutScroll2, false, false, false, 2000, -1, true);
            }
        }
        function aboutScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!aboutTimeline2_1.children.length) {
                    aboutTimeline2_1.add({
                        targets: '#about-1',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0).add({
                        targets: '#about-2, #about-2 p, #about-2 h2',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '.about-circle svg',
                        scale: [0.2, 0.4],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path',
                        strokeWidth: [15, 10],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path.middle-ring',
                        strokeWidth: [30, 20],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path.user-icon-fill',
                        fill: '#071738',
                        easing: 'linear',
                        duration: 1000
                    }, 0);
                }
                playAnimation(aboutTimeline2_1, false, 'about-3', aboutScroll2, aboutScroll3, false, false, false, 2000, -1, true);
            }
            else {
                playAnimation(aboutTimeline_1, true, 'about-1', aboutScroll2, aboutScroll, false, false, false, 1000, -1, true);
            }
        }
        function aboutScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!aboutTimeline_1.children.length) {
                    aboutTimeline_1.add({
                        targets: '#about-1 h1 strong',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg',
                        scale: [0.125, 0.2],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path',
                        strokeWidth: [20, 15],
                        duration: 1000
                    }, 0).add({
                        targets: '.about-circle svg path.middle-ring',
                        strokeWidth: [40, 30],
                        duration: 1000
                    }, 0);
                }
                playAnimation(aboutTimeline_1, false, 'about-2', aboutScroll, aboutScroll2, false, false, false, 1000, -1, true);
            }
        }
        window.addEventListener('wheel', aboutScroll);
        window.addEventListener('touchmove', aboutScroll);
    }
    var isWebinar = document.querySelector('#webinar-1');
    if (isWebinar) {
        // Webinar functions
        route.current = '/webinar';
        // fetch webinars
        var table = document.querySelector('.webinar-table');
        var localFeed = '/wp-content/themes/makosi/json/webinars.json';
        var feed = (table.dataset.feed) ? table.dataset.feed : localFeed;
        var webinarsUrl = (location.hostname === 'makosi.local' || location.hostname === 'localhost' || location.hostname === 'makosi.savyclients.com') ? localFeed : feed;
        getFeed(webinarsUrl).then(function (data) {
            if (data) {
                var table_1 = document.querySelector('.webinar-table table tbody');
                table_1.innerHTML = '';
                data.slice(0, 5).map(function (webinar) {
                    var date = moment(webinar.start_time);
                    var dateFormatted = date.format('MM.DD.YYYY');
                    var tz = (Intl.DateTimeFormat().resolvedOptions().timeZone) ? Intl.DateTimeFormat().resolvedOptions().timeZone : webinar.timezone;
                    var timezone = date.tz(tz).format('h:mm a z');
                    table_1.innerHTML += "\n              <tr>\n                <td><strong>" + dateFormatted + "</strong> | " + timezone + "</td>\n                <td>" + webinar.topic + "</td>\n                <td><a href=\"" + webinar.join_url + "\" target=\"_blank\">sign up</a></td>\n              </tr>\n            ";
                });
            }
        });
        var webinarTimeline_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var webinarTimeline2_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var webinarTimeline3_1 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // add webinar next timeline
        setNextTimeline([
            [webinarTimeline_1, false, 'webinar-2', webinarScroll, webinarScroll2, false, false, false, 1000, -1, true],
            [webinarTimeline2_1, false, 'webinar-3', webinarScroll2, webinarScroll3, false, false, false, 1000, -1, true],
            [webinarTimeline3_1, false, '/apply', webinarScroll3, webinarScroll4, false, false, false, 1000, -1, true]
        ], [
            [
                [{
                        targets: '#webinar-1',
                        translateX: ['0', '100vw'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.stroke-blob',
                        translateX: ['0', '125vw'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#webinar-2',
                        translateX: ['-100vw', '0'],
                        duration: 1000
                    }, 0]
            ],
            [
                [{
                        targets: '#webinar-3',
                        translateX: ['0', '100vw'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.stroke-blob',
                        top: [-830, -1280],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#webinar-4',
                        translateX: ['-100vw', '0'],
                        duration: 1000
                    }, 0]
            ],
            [
                [{
                        targets: '#webinar-main',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.stroke-blob',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.top-blob',
                        translateY: ['100vh', '0'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#application-1',
                        translateY: ['100%', '0'],
                        duration: 1000
                    }, 0]
            ]
        ]);
        function webinarScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end webinar
                console.log('end webinar');
            }
            else {
                playAnimation(webinarTimeline3_1, true, 'webinar-3', webinarScroll4, webinarScroll3, false, false, false, 1000, -1, true);
            }
        }
        function webinarScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!webinarTimeline3_1.children.length) {
                    webinarTimeline3_1.add({
                        targets: '#webinar-main',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '.stroke-blob',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0).add({
                        targets: '.top-blob',
                        translateY: ['100vh', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#application-1',
                        translateY: ['100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(webinarTimeline3_1, false, '/apply', webinarScroll3, webinarScroll4, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(webinarTimeline2_1, true, 'webinar-2', webinarScroll3, webinarScroll2, false, false, false, 1000, -1, true);
            }
        }
        function webinarScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!webinarTimeline2_1.children.length) {
                    webinarTimeline2_1.add({
                        targets: '#webinar-3',
                        translateX: ['0', '100vw'],
                        duration: 1000
                    }, 0).add({
                        targets: '.stroke-blob',
                        top: [-830, -1280],
                        duration: 1000
                    }, 0).add({
                        targets: '#webinar-4',
                        translateX: ['-100vw', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(webinarTimeline2_1, false, 'webinar-3', webinarScroll2, webinarScroll3, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(webinarTimeline_1, true, 'webinar-1', webinarScroll2, webinarScroll, false, false, false, 1000, -1, true);
            }
        }
        function webinarScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!webinarTimeline_1.children.length) {
                    webinarTimeline_1.add({
                        targets: '#webinar-1',
                        translateX: ['0', '100vw'],
                        duration: 1000
                    }, 0).add({
                        targets: '.stroke-blob',
                        translateX: ['0', '125vw'],
                        duration: 1000
                    }, 0).add({
                        targets: '#webinar-2',
                        translateX: ['-100vw', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(webinarTimeline_1, false, 'webinar-2', webinarScroll, webinarScroll2, false, false, false, 1000, -1, true);
            }
        }
        window.addEventListener('wheel', webinarScroll);
        window.addEventListener('touchmove', webinarScroll);
    }
    var isAudit = document.querySelector('.audit-main');
    if (isAudit) {
        // Audit functions
        route.current = '/audit';
        // Audit Clients
        var auditClientsButton_2 = document.querySelector('#audit-clients');
        var auditClientsTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline2_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline3_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline4_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline5_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline6_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline7_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        if (!auditClientsTimeline2_2.children.length) {
            auditClientsTimeline2_2.add({
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000
            }, 0).add({
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000
            }, 0).add({
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000
            }, 0).add({
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000
            }, 1000).add({
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000
            }, 2000).add({
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000
            }, 3000);
        }
        function animateAuditClients(event) {
            if (auditClientsButton_2) {
                event.preventDefault();
            }
            if (!auditClientsTimeline_2.children.length) {
                auditClientsTimeline_2.add({
                    targets: '.top-blob, #audit-clients-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0);
            }
            playAnimation(auditClientsTimeline_2, false, '/audit/clients', false, auditClientsScroll, auditClientsButton_2, false, false, 1000, -1, true);
            // set audit clients next timeline
            setNextTimeline([
                [auditClientsTimeline2_2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
                [auditClientsTimeline3_2, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
                [auditClientsTimeline4_2, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
                [auditClientsTimeline5_2, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
                [auditClientsTimeline6_2, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
                [auditClientsTimeline7_2, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
            ], [
                [
                    [
                        {
                            targets: '.welcome-content',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section .fact-0',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '.why-section .fact-1',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '.why-section .fact-2',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ]
                ],
                [
                    [
                        {
                            targets: '.top-blob',
                            top: ['0', '100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .side-title',
                            top: ['150%', '50%'],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts h2',
                            opacity: [0, 0.25],
                            borderBottom: '1px solid #fff',
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-0 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-1 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-2 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        6000
                    ],
                    [
                        {
                            targets: '#audit-clients-2 .audit-facts .fact-3 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        6000
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.left-blob',
                            translateX: ['100%', '-50%'],
                            duration: 1500
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-3',
                            translateX: ['100%', '0'],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1000
                        },
                        500
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-3',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-3',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-4',
                            translateX: ['100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .first-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .second-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .third-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#audit-clients-4 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#audit-clients-4 .industries',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '.lines-single .top-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .bot-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-1 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-2 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-3 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ]
                ],
                [
                    [
                        {
                            targets: '#audit-clients-5',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0
                    ]
                ],
                [
                    [
                        {
                            targets: '.new-blog',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.new-blog',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ]
                ],
            ], true);
        }
        function auditClientsScroll7(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end of audit clients
                console.log('end audit clients animations');
            }
            else {
                console.log('no scrolling events inside blog');
            }
        }
        function auditClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline7_2.children.length) {
                    auditClientsTimeline7_2.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditClientsTimeline7_2, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(auditClientsTimeline6_2, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
            }
        }
        function auditClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline6_2.children.length) {
                    auditClientsTimeline6_2.add({
                        targets: '#audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditClientsTimeline6_2, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline5_2, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
            }
        }
        function auditClientsScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline5_2.children.length) {
                    auditClientsTimeline5_2.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0);
                }
                playAnimation(auditClientsTimeline5_2, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline4_2, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
            }
        }
        function auditClientsScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline4_2.children.length) {
                    auditClientsTimeline4_2.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    }, 0).add({
                        targets: '#audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 500);
                }
                playAnimation(auditClientsTimeline4_2, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline3_2, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
            }
        }
        function auditClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline3_2.children.length) {
                    auditClientsTimeline3_2.add({
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 3000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000);
                }
                playAnimation(auditClientsTimeline3_2, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline2_2, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
            }
        }
        function auditClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(auditClientsTimeline2_2, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline_2, true, '/audit', auditClientsScroll, false, auditClientsButton_2, false, false, 1000);
            }
        }
        if (auditClientsButton_2) {
            auditClientsButton_2.addEventListener('click', animateAuditClients);
        }
        // Audit Candidates
        var auditCandidatesTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline2_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline3_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline4_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline5_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesButton_2 = document.querySelector('#audit-candidates');
        function auditCandidatesScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end audit candidates
                console.log('end audit candidates');
            }
            else {
                playAnimation(auditCandidatesTimeline5_2, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
            }
        }
        function auditCandidatesScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline5_2.children.length) {
                    auditCandidatesTimeline5_2.add({
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000).add({
                        targets: '#audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(auditCandidatesTimeline5_2, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline4_2, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
            }
        }
        function auditCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline4_2.children.length) {
                    auditCandidatesTimeline4_2.add({
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
                        duration: 2000
                    }, 0);
                }
                playAnimation(auditCandidatesTimeline4_2, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline3_2, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
            }
        }
        function auditCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline3_2.children.length) {
                    auditCandidatesTimeline3_2.add({
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditCandidatesTimeline3_2, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline2_2, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
            }
        }
        function auditCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline2_2.children.length) {
                    auditCandidatesTimeline2_2.add({
                        targets: '#audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000);
                }
                playAnimation(auditCandidatesTimeline2_2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline_2, true, '/audit', auditCandidatesScroll, false, auditCandidatesButton_2, false, false, 2000);
            }
        }
        function animateCandidates(event) {
            if (auditCandidatesButton_2) {
                event.preventDefault();
            }
            if (!auditCandidatesTimeline_2.children.length) {
                auditCandidatesTimeline_2.add({
                    targets: '.top-blob, #audit-candidates-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0).add({
                    targets: '#audit-candidates-1-2',
                    translateX: ['-100%', '0'],
                    duration: 1000
                }, 1000);
            }
            playAnimation(auditCandidatesTimeline_2, false, '/audit/applicants', false, auditCandidatesScroll, auditCandidatesButton_2, false, false, 2000, -1, true);
            // set audit candidates next timeline
            setNextTimeline([
                [auditCandidatesTimeline2_2, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
                [auditCandidatesTimeline3_2, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
                [auditCandidatesTimeline4_2, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
                [auditCandidatesTimeline5_2, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
            ], [
                [
                    [{
                            targets: '#audit-candidates-1-1',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-1-2',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-1-3',
                            translateX: ['-100%', '0'],
                            duration: 1000
                        }, 1000]
                ],
                [
                    [{
                            targets: '.top-blob',
                            top: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-1',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0]
                ],
                [
                    [{
                            targets: '#audit-candidates-2 .slide-title-top',
                            translateX: ['-100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2 .slide-title-bottom',
                            translateX: ['100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2 .slide-title',
                            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
                            duration: 2000
                        }, 0]
                ],
                [
                    [{
                            targets: '#audit-candidates-2 .slide-title-top',
                            translateX: ['0', '100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-2 .slide-title-bottom',
                            translateX: ['0', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#audit-candidates-3',
                            opacity: [0, 1],
                            zIndex: {
                                value: [1, 3],
                                round: true
                            },
                            duration: 1
                        }, 1000],
                    [{
                            targets: '#audit-candidates-3 .before',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#audit-candidates-3 .after',
                            translateX: ['0', '100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#audit-candidates-3 .call-to-action',
                            opacity: ['0', '1'],
                            duration: 1000
                        }, 2000]
                ]
            ], true);
        }
        if (auditCandidatesButton_2) {
            auditCandidatesButton_2.addEventListener('click', animateCandidates);
        }
    }
    // new main
    var isItAudit = document.querySelector('.it-audit-main');
    if (isItAudit) {
        // itAudit functions
        route.current = '/it-audit';
        // itAudit Clients
        var itAuditClientsButton_2 = document.querySelector('#it-audit-clients');
        var itAuditClientsTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline2_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline3_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline4_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline5_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline6_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline7_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        if (!itAuditClientsTimeline2_2.children.length) {
            itAuditClientsTimeline2_2.add({
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000
            }, 0).add({
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000
            }, 0).add({
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000
            }, 0).add({
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000
            }, 1000).add({
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000
            }, 2000).add({
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000
            }, 3000);
        }
        function animateItAuditClients(event) {
            if (itAuditClientsButton_2) {
                event.preventDefault();
            }
            if (!itAuditClientsTimeline_2.children.length) {
                itAuditClientsTimeline_2.add({
                    targets: '.top-blob, #it-audit-clients-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0);
            }
            playAnimation(itAuditClientsTimeline_2, false, '/it-audit/clients', false, itAuditClientsScroll, itAuditClientsButton_2, false, false, 1000, -1, true);
            // set audit clients next timeline
            setNextTimeline([
                [itAuditClientsTimeline2_2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
                [itAuditClientsTimeline3_2, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
                [itAuditClientsTimeline4_2, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
                [itAuditClientsTimeline5_2, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
                [itAuditClientsTimeline6_2, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
                [itAuditClientsTimeline7_2, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
            ], [
                [
                    [
                        {
                            targets: '.welcome-content',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section, .why-content',
                            translateY: ['100%', '0'],
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-content .section-title',
                            translateY: ['100%', '0'],
                            rotate: ['-90deg', '-90deg'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.why-section .fact-0',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '.why-section .fact-1',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '.why-section .fact-2',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ]
                ],
                [
                    [
                        {
                            targets: '.top-blob',
                            top: ['0', '100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .side-title',
                            top: ['150%', '50%'],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts h2',
                            opacity: [0, 0.25],
                            borderBottom: '1px solid #fff',
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        4000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        5000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        },
                        6000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        6000
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.left-blob',
                            translateX: ['100%', '-50%'],
                            duration: 1500
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-3',
                            translateX: ['100%', '0'],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1000
                        },
                        500
                    ]
                ],
                [
                    [
                        {
                            targets: '.left-blob',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-3',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-3',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4',
                            translateX: ['100%', '0'],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .first-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .second-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        1000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .third-title',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 p',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        2000
                    ],
                    [
                        {
                            targets: '#it-audit-clients-4 .industries',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        3000
                    ],
                    [
                        {
                            targets: '.lines-single .top-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .bot-line path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-1 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-2 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ],
                    [
                        {
                            targets: '.lines-single .hor-line-3 path',
                            strokeDashoffset: [anime.setDashoffset, 0],
                            delay: function (el, i) { return i * 250; },
                            duration: 3000
                        },
                        0
                    ]
                ],
                [
                    [
                        {
                            targets: '#it-audit-clients-5',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0
                    ]
                ],
                [
                    [
                        {
                            targets: '.new-blog',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [
                        {
                            targets: '.new-blog',
                            opacity: [0, 1],
                            duration: 1000
                        },
                        0
                    ]
                ],
            ], true);
        }
        function itAuditClientsScroll7(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end of audit clients
                console.log('end audit clients animations');
            }
            else {
                console.log('no scrolling events inside blog');
            }
        }
        function itAuditClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline7_2.children.length) {
                    itAuditClientsTimeline7_2.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline7_2, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(itAuditClientsTimeline6_2, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
            }
        }
        function itAuditClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline6_2.children.length) {
                    itAuditClientsTimeline6_2.add({
                        targets: '#it-audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline6_2, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline5_2, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
            }
        }
        function itAuditClientsScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline5_2.children.length) {
                    itAuditClientsTimeline5_2.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline5_2, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline4_2, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
            }
        }
        function itAuditClientsScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline4_2.children.length) {
                    itAuditClientsTimeline4_2.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 500);
                }
                playAnimation(itAuditClientsTimeline4_2, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline3_2, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
            }
        }
        function itAuditClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline3_2.children.length) {
                    itAuditClientsTimeline3_2.add({
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 3000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000);
                }
                playAnimation(itAuditClientsTimeline3_2, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline2_2, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
            }
        }
        function itAuditClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(itAuditClientsTimeline2_2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline_2, true, '/it-audit', itAuditClientsScroll, false, itAuditClientsButton_2, false, false, 1000);
            }
        }
        if (itAuditClientsButton_2) {
            itAuditClientsButton_2.addEventListener('click', animateItAuditClients);
        }
        // itAudit Candidates
        var itAuditCandidatesTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline2_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline3_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline4_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline5_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesButton_2 = document.querySelector('#it-audit-candidates');
        function itAuditCandidatesScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end audit candidates
                console.log('end audit candidates');
            }
            else {
                playAnimation(itAuditCandidatesTimeline5_2, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
            }
        }
        function itAuditCandidatesScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline5_2.children.length) {
                    itAuditCandidatesTimeline5_2.add({
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(itAuditCandidatesTimeline5_2, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline4_2, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
            }
        }
        function itAuditCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline4_2.children.length) {
                    itAuditCandidatesTimeline4_2.add({
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
                        duration: 2000
                    }, 0);
                }
                playAnimation(itAuditCandidatesTimeline4_2, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline3_2, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
            }
        }
        function itAuditCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline3_2.children.length) {
                    itAuditCandidatesTimeline3_2.add({
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditCandidatesTimeline3_2, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline2_2, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
            }
        }
        function itAuditCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline2_2.children.length) {
                    itAuditCandidatesTimeline2_2.add({
                        targets: '#it-audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000);
                }
                playAnimation(itAuditCandidatesTimeline2_2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline_2, true, '/it-audit', itAuditCandidatesScroll, false, itAuditCandidatesButton_2, false, false, 2000);
            }
        }
        function animateItCandidates(event) {
            if (itAuditCandidatesButton_2) {
                event.preventDefault();
            }
            if (!itAuditCandidatesTimeline_2.children.length) {
                itAuditCandidatesTimeline_2.add({
                    targets: '.top-blob, #it-audit-candidates-1',
                    translateY: ['-100%', '0'],
                    duration: 1000
                }, 0).add({
                    targets: '#it-audit-candidates-1-2',
                    translateX: ['-100%', '0'],
                    duration: 1000
                }, 1000);
            }
            playAnimation(itAuditCandidatesTimeline_2, false, '/it-audit/applicants', false, itAuditCandidatesScroll, itAuditCandidatesButton_2, false, false, 2000, -1, true);
            // set audit candidates next timeline
            setNextTimeline([
                [itAuditCandidatesTimeline2_2, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
                [itAuditCandidatesTimeline3_2, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
                [itAuditCandidatesTimeline4_2, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
                [itAuditCandidatesTimeline5_2, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
            ], [
                [
                    [{
                            targets: '#it-audit-candidates-1-1',
                            translateY: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-1-2',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-1-3',
                            translateX: ['-100%', '0'],
                            duration: 1000
                        }, 1000]
                ],
                [
                    [{
                            targets: '.top-blob',
                            top: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-1',
                            translateY: ['0', '100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2',
                            translateY: ['-100%', '0'],
                            duration: 1000
                        }, 0]
                ],
                [
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-top',
                            translateX: ['-100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-bottom',
                            translateX: ['100%', '0'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2 .slide-title',
                            background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
                            duration: 2000
                        }, 0]
                ],
                [
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-top',
                            translateX: ['0', '100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-2 .slide-title-bottom',
                            translateX: ['0', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#it-audit-candidates-3',
                            opacity: [0, 1],
                            zIndex: {
                                value: [1, 3],
                                round: true
                            },
                            duration: 1
                        }, 1000],
                    [{
                            targets: '#it-audit-candidates-3 .before',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#it-audit-candidates-3 .after',
                            translateX: ['0', '100%'],
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#it-audit-candidates-3 .call-to-action',
                            opacity: ['0', '1'],
                            duration: 1000
                        }, 2000]
                ]
            ], true);
        }
        if (itAuditCandidatesButton_2) {
            itAuditCandidatesButton_2.addEventListener('click', animateItCandidates);
        }
    }
    // end new main
    var isAuditClients = document.querySelector('.audit-clients-main');
    if (isAuditClients) {
        // Audit Clients
        route.current = '/audit/clients';
        var auditClientsTimeline2_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline3_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline4_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline5_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline6_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditClientsTimeline7_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        if (!auditClientsTimeline2_3.children.length) {
            auditClientsTimeline2_3.add({
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000
            }, 0).add({
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000
            }, 0).add({
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000
            }, 0).add({
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000
            }, 1000).add({
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000
            }, 2000).add({
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000
            }, 3000);
        }
        // set audit clients next timeline
        setNextTimeline([
            [auditClientsTimeline2_3, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true],
            [auditClientsTimeline3_3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true],
            [auditClientsTimeline4_3, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true],
            [auditClientsTimeline5_3, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true],
            [auditClientsTimeline6_3, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true],
            [auditClientsTimeline7_3, false, 'clients7', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true]
        ], [
            [
                [
                    {
                        targets: '.welcome-content',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-section, .why-content',
                        translateY: ['100%', '0'],
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-content .section-title',
                        translateY: ['100%', '0'],
                        rotate: ['-90deg', '-90deg'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-section, .why-content',
                        translateY: ['100%', '0'],
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-content .section-title',
                        translateY: ['100%', '0'],
                        rotate: ['-90deg', '-90deg'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-section .fact-0',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    1000
                ],
                [
                    {
                        targets: '.why-section .fact-1',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '.why-section .fact-2',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    3000
                ]
            ],
            [
                [
                    {
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    },
                    1000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    3000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    3000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    4000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    4000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    5000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    5000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    6000
                ],
                [
                    {
                        targets: '#audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    6000
                ]
            ],
            [
                [
                    {
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    },
                    500
                ]
            ],
            [
                [
                    {
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    1000
                ],
                [
                    {
                        targets: '#audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '#audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '#audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    3000
                ],
                [
                    {
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ]
            ],
            [
                [
                    {
                        targets: '#audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0
                ]
            ],
            [
                [
                    {
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ]
            ],
        ]);
        function auditClientsScroll7(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end of audit clients
                console.log('end audit clients animations');
            }
            else {
                console.log('no scrolling events inside blog');
            }
        }
        function auditClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline7_3.children.length) {
                    auditClientsTimeline7_3.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-5, #audit-clients-4, #audit-clients-3, #audit-clients-2, #audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditClientsTimeline7_3, false, '/blog', auditClientsScroll6, auditClientsScroll7, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(auditClientsTimeline6_3, true, 'clients5', auditClientsScroll6, auditClientsScroll5, false, true, false, 1000, -1, true);
            }
        }
        function auditClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline6_3.children.length) {
                    auditClientsTimeline6_3.add({
                        targets: '#audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditClientsTimeline6_3, false, 'clients6', auditClientsScroll5, auditClientsScroll6, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline5_3, true, 'clients4', auditClientsScroll5, auditClientsScroll4, false, true, false, 4000, -1, true);
            }
        }
        function auditClientsScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline5_3.children.length) {
                    auditClientsTimeline5_3.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0);
                }
                playAnimation(auditClientsTimeline5_3, false, 'clients5', auditClientsScroll4, auditClientsScroll5, false, true, false, 4000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline4_3, true, 'clients3', auditClientsScroll4, auditClientsScroll3, false, false, false, 1500, -1, true);
            }
        }
        function auditClientsScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline4_3.children.length) {
                    auditClientsTimeline4_3.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    }, 0).add({
                        targets: '#audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 500);
                }
                playAnimation(auditClientsTimeline4_3, false, 'clients4', auditClientsScroll3, auditClientsScroll4, false, true, false, 1500, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline3_3, true, 'clients2', auditClientsScroll3, auditClientsScroll2, false, false, false, 7000, -1, true);
            }
        }
        function auditClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditClientsTimeline3_3.children.length) {
                    auditClientsTimeline3_3.add({
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 2000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 3000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000);
                }
                playAnimation(auditClientsTimeline3_3, false, 'clients3', auditClientsScroll2, auditClientsScroll3, false, false, false, 7000, -1, true);
            }
            else {
                playAnimation(auditClientsTimeline2_3, true, '/audit/clients', auditClientsScroll2, auditClientsScroll, false, false, false, 4000, -1, true);
            }
        }
        function auditClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(auditClientsTimeline2_3, false, 'clients2', auditClientsScroll, auditClientsScroll2, false, false, false, 4000, -1, true);
            }
        }
        window.addEventListener('wheel', auditClientsScroll);
        window.addEventListener('touchmove', auditClientsScroll);
    }
    // new clients
    var isItAuditClients = document.querySelector('.it-audit-clients-main');
    if (isItAuditClients) {
        // Audit Clients
        route.current = '/it-audit/clients';
        var itAuditClientsTimeline2_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline3_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline4_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline5_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline6_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditClientsTimeline7_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        if (!itAuditClientsTimeline2_3.children.length) {
            itAuditClientsTimeline2_3.add({
                targets: '.welcome-content',
                translateY: ['0', '-100%'],
                duration: 1000
            }, 0).add({
                targets: '.why-section, .why-content',
                translateY: ['100%', '0'],
                opacity: [0, 1],
                duration: 1000
            }, 0).add({
                targets: '.why-content .section-title',
                translateY: ['100%', '0'],
                rotate: ['-90deg', '-90deg'],
                duration: 1000
            }, 0).add({
                targets: '.why-section .fact-0',
                opacity: [0, 1],
                duration: 1000
            }, 1000).add({
                targets: '.why-section .fact-1',
                opacity: [0, 1],
                duration: 1000
            }, 2000).add({
                targets: '.why-section .fact-2',
                opacity: [0, 1],
                duration: 1000
            }, 3000);
        }
        // set audit clients next timeline
        setNextTimeline([
            [itAuditClientsTimeline2_3, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
            [itAuditClientsTimeline3_3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
            [itAuditClientsTimeline4_3, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
            [itAuditClientsTimeline5_3, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
            [itAuditClientsTimeline6_3, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
            [itAuditClientsTimeline7_3, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
        ], [
            [
                [
                    {
                        targets: '.welcome-content',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-section, .why-content',
                        translateY: ['100%', '0'],
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-content .section-title',
                        translateY: ['100%', '0'],
                        rotate: ['-90deg', '-90deg'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-section, .why-content',
                        translateY: ['100%', '0'],
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-content .section-title',
                        translateY: ['100%', '0'],
                        rotate: ['-90deg', '-90deg'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '.why-section .fact-0',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    1000
                ],
                [
                    {
                        targets: '.why-section .fact-1',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '.why-section .fact-2',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    3000
                ]
            ],
            [
                [
                    {
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    },
                    1000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    3000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    3000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    4000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    4000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    5000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    5000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    },
                    6000
                ],
                [
                    {
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    6000
                ]
            ],
            [
                [
                    {
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    },
                    500
                ]
            ],
            [
                [
                    {
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    1000
                ],
                [
                    {
                        targets: '#it-audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '#it-audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    2000
                ],
                [
                    {
                        targets: '#it-audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    3000
                ],
                [
                    {
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ],
                [
                    {
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    },
                    0
                ]
            ],
            [
                [
                    {
                        targets: '#it-audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0
                ]
            ],
            [
                [
                    {
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    },
                    0
                ],
                [
                    {
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    },
                    0
                ]
            ],
        ]);
        function itAuditClientsScroll7(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end of audit clients
                console.log('end audit clients animations');
            }
            else {
                console.log('no scrolling events inside blog');
            }
        }
        function itAuditClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline7_3.children.length) {
                    itAuditClientsTimeline7_3.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline7_3, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(itAuditClientsTimeline6_3, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
            }
        }
        function itAuditClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline6_3.children.length) {
                    itAuditClientsTimeline6_3.add({
                        targets: '#it-audit-clients-5',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline6_3, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline5_3, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
            }
        }
        function itAuditClientsScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline5_3.children.length) {
                    itAuditClientsTimeline5_3.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4 .first-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-4 .second-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-clients-4 .third-title',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-4 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-4 .industries',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '.lines-single .top-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .bot-line path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-1 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-2 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0).add({
                        targets: '.lines-single .hor-line-3 path',
                        strokeDashoffset: [anime.setDashoffset, 0],
                        delay: function (el, i) { return i * 250; },
                        duration: 3000
                    }, 0);
                }
                playAnimation(itAuditClientsTimeline5_3, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline4_3, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
            }
        }
        function itAuditClientsScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline4_3.children.length) {
                    itAuditClientsTimeline4_3.add({
                        targets: '.left-blob',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.left-blob',
                        translateX: ['100%', '-50%'],
                        duration: 1500
                    }, 0).add({
                        targets: '#it-audit-clients-3',
                        translateX: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 500);
                }
                playAnimation(itAuditClientsTimeline4_3, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline3_3, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
            }
        }
        function itAuditClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditClientsTimeline3_3.children.length) {
                    itAuditClientsTimeline3_3.add({
                        targets: '.top-blob',
                        top: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 2000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 3000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 3000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000);
                }
                playAnimation(itAuditClientsTimeline3_3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
            }
            else {
                playAnimation(itAuditClientsTimeline2_3, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
            }
        }
        function itAuditClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                playAnimation(itAuditClientsTimeline2_3, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
            }
        }
        window.addEventListener('wheel', itAuditClientsScroll);
        window.addEventListener('touchmove', itAuditClientsScroll);
    }
    // end new clients
    var isAuditCandidates = document.querySelector('.audit-candidates-main');
    if (isAuditCandidates) {
        // Audit Candidates
        route.current = '/audit/applicants';
        var auditCandidatesTimeline2_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline3_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline4_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesTimeline5_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var auditCandidatesButton = document.querySelector('#audit-candidates');
        // set audit candidates next timeline
        setNextTimeline([
            [auditCandidatesTimeline2_3, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true],
            [auditCandidatesTimeline3_3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true],
            [auditCandidatesTimeline4_3, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true],
            [auditCandidatesTimeline5_3, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000]
        ], [
            [
                [{
                        targets: '#audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000]
            ],
            [
                [{
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0]
            ],
            [
                [{
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
                        duration: 2000
                    }, 0]
            ],
            [
                [{
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000],
                [{
                        targets: '#audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000],
                [{
                        targets: '#audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000],
                [{
                        targets: '#audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000]
            ]
        ]);
        function auditCandidatesScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end audit candidates
                console.log('end audit candidates');
            }
            else {
                playAnimation(auditCandidatesTimeline5_3, true, 'candidates4', auditCandidatesScroll5, auditCandidatesScroll4, false, true, false, 3000, -1, true);
            }
        }
        function auditCandidatesScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline5_3.children.length) {
                    auditCandidatesTimeline5_3.add({
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000).add({
                        targets: '#audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(auditCandidatesTimeline5_3, false, 'candidates4', auditCandidatesScroll4, auditCandidatesScroll5, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline4_3, true, 'candidates3', auditCandidatesScroll4, auditCandidatesScroll3, false, true, false, 2000, -1, true);
            }
        }
        function auditCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline4_3.children.length) {
                    auditCandidatesTimeline4_3.add({
                        targets: '#audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
                        duration: 2000
                    }, 0);
                }
                playAnimation(auditCandidatesTimeline4_3, false, 'candidates4', auditCandidatesScroll3, auditCandidatesScroll4, false, true, false, 2000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline3_3, true, 'candidates2', auditCandidatesScroll3, auditCandidatesScroll2, false, false, false, 1000, -1, true);
            }
        }
        function auditCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline3_3.children.length) {
                    auditCandidatesTimeline3_3.add({
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(auditCandidatesTimeline3_3, false, 'candidates3', auditCandidatesScroll2, auditCandidatesScroll3, false, true, false, 1000, -1, true);
            }
            else {
                playAnimation(auditCandidatesTimeline2_3, true, '/audit/applicants', auditCandidatesScroll2, auditCandidatesScroll, false, false, false, 2000, -1, true);
            }
        }
        function auditCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!auditCandidatesTimeline2_3.children.length) {
                    auditCandidatesTimeline2_3.add({
                        targets: '#audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000);
                }
                playAnimation(auditCandidatesTimeline2_3, false, 'candidates2', auditCandidatesScroll, auditCandidatesScroll2, false, false, false, 2000, -1, true);
            }
        }
        window.addEventListener('wheel', auditCandidatesScroll);
        window.addEventListener('touchmove', auditCandidatesScroll);
    }
    // new candidates
    var isItAuditCandidates = document.querySelector('.it-audit-candidates-main');
    if (isItAuditCandidates) {
        // Audit Candidates
        route.current = '/it-audit/applicants';
        var itAuditCandidatesTimeline2_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline3_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline4_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesTimeline5_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var itAuditCandidatesButton = document.querySelector('#it-audit-candidates');
        // set audit candidates next timeline
        setNextTimeline([
            [itAuditCandidatesTimeline2_3, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true],
            [itAuditCandidatesTimeline3_3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true],
            [itAuditCandidatesTimeline4_3, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true],
            [itAuditCandidatesTimeline5_3, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000]
        ], [
            [
                [{
                        targets: '#it-audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000]
            ],
            [
                [{
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0]
            ],
            [
                [{
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0]', 'rgba(255, 255, 255, 1]'],
                        duration: 2000
                    }, 0]
            ],
            [
                [{
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#it-audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000],
                [{
                        targets: '#it-audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000],
                [{
                        targets: '#it-audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000],
                [{
                        targets: '#it-audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000]
            ]
        ]);
        function itAuditCandidatesScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end audit candidates
                console.log('end audit candidates');
            }
            else {
                playAnimation(itAuditCandidatesTimeline5_3, true, 'candidates4', itAuditCandidatesScroll5, itAuditCandidatesScroll4, false, true, false, 3000, -1, true);
            }
        }
        function itAuditCandidatesScroll4(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline5_3.children.length) {
                    itAuditCandidatesTimeline5_3.add({
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['0', '100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-3',
                        opacity: [0, 1],
                        zIndex: {
                            value: [1, 3],
                            round: true
                        },
                        duration: 1
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .before',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .after',
                        translateX: ['0', '100%'],
                        duration: 1000
                    }, 1000).add({
                        targets: '#it-audit-candidates-3 .call-to-action',
                        opacity: ['0', '1'],
                        duration: 1000
                    }, 2000);
                }
                playAnimation(itAuditCandidatesTimeline5_3, false, 'candidates4', itAuditCandidatesScroll4, itAuditCandidatesScroll5, false, false, false, 3000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline4_3, true, 'candidates3', itAuditCandidatesScroll4, itAuditCandidatesScroll3, false, true, false, 2000, -1, true);
            }
        }
        function itAuditCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline4_3.children.length) {
                    itAuditCandidatesTimeline4_3.add({
                        targets: '#it-audit-candidates-2 .slide-title-top',
                        translateX: ['-100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title-bottom',
                        translateX: ['100%', '0'],
                        duration: 2000
                    }, 0).add({
                        targets: '#it-audit-candidates-2 .slide-title',
                        background: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)'],
                        duration: 2000
                    }, 0);
                }
                playAnimation(itAuditCandidatesTimeline4_3, false, 'candidates4', itAuditCandidatesScroll3, itAuditCandidatesScroll4, false, true, false, 2000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline3_3, true, 'candidates2', itAuditCandidatesScroll3, itAuditCandidatesScroll2, false, false, false, 1000, -1, true);
            }
        }
        function itAuditCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline3_3.children.length) {
                    itAuditCandidatesTimeline3_3.add({
                        targets: '.top-blob',
                        top: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-2',
                        translateY: ['-100%', '0'],
                        duration: 1000
                    }, 0);
                }
                playAnimation(itAuditCandidatesTimeline3_3, false, 'candidates3', itAuditCandidatesScroll2, itAuditCandidatesScroll3, false, true, false, 1000, -1, true);
            }
            else {
                playAnimation(itAuditCandidatesTimeline2_3, true, '/it-audit/applicants', itAuditCandidatesScroll2, itAuditCandidatesScroll, false, false, false, 2000, -1, true);
            }
        }
        function itAuditCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!itAuditCandidatesTimeline2_3.children.length) {
                    itAuditCandidatesTimeline2_3.add({
                        targets: '#it-audit-candidates-1-1',
                        translateY: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1-2',
                        translateY: ['0', '100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#it-audit-candidates-1-3',
                        translateX: ['-100%', '0'],
                        duration: 1000
                    }, 1000);
                }
                playAnimation(itAuditCandidatesTimeline2_3, false, 'candidates2', itAuditCandidatesScroll, itAuditCandidatesScroll2, false, false, false, 2000, -1, true);
            }
        }
        window.addEventListener('wheel', itAuditCandidatesScroll);
        window.addEventListener('touchmove', itAuditCandidatesScroll);
    }
    //end new candidates
    var isTax = document.querySelector('.tax-main');
    if (isTax) {
        // Tax
        route.current = '/tax';
        var taxButton_2 = document.querySelector('#tax');
        var taxTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        function taxScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta != '1') {
                if (isHome) {
                    playAnimation(taxTimeline_2, true, '/', taxScroll, homeScroll, taxButton_2, false, false, 1000);
                }
            }
        }
        function animateTax(event) {
            if (taxButton_2) {
                event.preventDefault();
            }
            if (!taxTimeline_2.children.length) {
                taxTimeline_2.add({
                    targets: '.lines .rest',
                    opacity: [0, 1],
                    duration: 1000
                }, 0).add({
                    targets: '.lines',
                    translateX: '-225%',
                    translateY: '-75%',
                    scale: 2,
                    duration: 1000
                }, 0).add({
                    targets: '#home-2',
                    translateX: [0, '-100%'],
                    translateY: [0, '-100%'],
                    duration: 1000
                }, 0).add({
                    targets: '#tax-1',
                    translateX: ['100%', '0'],
                    duration: 1000
                }, 0);
            }
            playAnimation(taxTimeline_2, false, '/tax', homeScroll, taxScroll, taxButton_2, false, false, 1000);
        }
        if (taxButton_2) {
            taxButton_2.addEventListener('click', animateTax);
        }
        // Tax Clients
        var taxClientsButton_2 = document.querySelector('#tax-clients');
        var taxClientsTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxClientsTimeline2_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // let taxClientsTimeline3 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        // let taxClientsTimeline4 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        var taxClientsTimeline5_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxClientsTimeline6_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        function taxClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end tax clients
                console.log('end tax clients');
            }
            else {
                console.log('no scrolling inside blog');
            }
        }
        function taxClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxClientsTimeline6_2.children.length) {
                    taxClientsTimeline6_2.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#home-1, #audit-1, #tax-1',
                        height: ['100vh', 0],
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(taxClientsTimeline6_2, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(taxClientsTimeline5_2, true, 'tax/clients2', taxClientsScroll5, taxClientsScroll2, false, false, false, 1600, -1, true);
            }
        }
        // function taxClientsScroll4(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // end tax clients
        //    console.log('end tax clients');
        //  } else {
        //    playAnimation(taxClientsTimeline4, true, 'tax/clients3', taxClientsScroll4, taxClientsScroll3, false, true);
        //  }
        // }
        // function taxClientsScroll3(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // taxClientsTimeline4.add({
        //    //  targets: '.left-blob',
        //    //  opacity: 0,
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-3',
        //    //  zIndex: {
        //    //    value: [3, 2],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '.left-blob-blue',
        //    //  zIndex: {
        //    //    value: [2, 3],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-4',
        //    //  zIndex: {
        //    //    value: [2, 3],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '.left-blob-blue',
        //    //  translateX: ['100%', '-75%'],
        //    //  duration: 1500,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-4',
        //    //  translateX: ['100%', '0'],
        //    //  duration: 1000,
        //    //  offset: 600
        //    // });
        //     // playAnimation(taxClientsTimeline4, false, 'tax/clients4', taxClientsScroll3, taxClientsScroll4);
        //     // end tax clients
        //    console.log('end tax clients');
        //  } else {
        //    playAnimation(taxClientsTimeline3, true, 'tax/clients2', taxClientsScroll3, taxClientsScroll2);
        //  }
        // }
        function taxClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxClientsTimeline3.add({
                //  targets: '.bottom-blob',
                //  opacity: 0,
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '.left-blob',
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '#tax-clients-2',
                //  zIndex: {
                //    value: [3, 2],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '.left-blob',
                //  translateX: ['100%', '-50%'],
                //  duration: 1500,
                //  offset: 0
                // }).add({
                //  targets: '#tax-clients-3',
                //  translateX: ['100%', '0'],
                //  duration: 1000,
                //  offset: 500,
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                // });
                // playAnimation(taxClientsTimeline3, false, 'tax/clients3', taxClientsScroll2, taxClientsScroll3, false, true);
                // end tax clients
                // console.log('end tax clients');
                if (!taxClientsTimeline5_2.children.length) {
                    taxClientsTimeline5_2.add({
                        targets: '.bottom-blob',
                        opacity: 0,
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-2',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-2',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 600).add({
                        targets: '#tax-clients-5',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-5',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 600);
                }
                playAnimation(taxClientsTimeline5_2, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true);
            }
            else {
                playAnimation(taxClientsTimeline2_2, true, '/tax/clients', taxClientsScroll2, taxClientsScroll, false, true, false, 8000, -1, true);
            }
        }
        function taxClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxClientsTimeline2_2.children.length) {
                    taxClientsTimeline2_2.add({
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#tax-clients-2',
                        translateY: ['100%', '0'],
                        duration: 1000,
                        zIndex: {
                            value: [2, 3],
                            round: true
                        }
                    }, 1000).add({
                        targets: '#tax-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 2000).add({
                        targets: '#tax-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 3000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 7000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 7000);
                }
                playAnimation(taxClientsTimeline2_2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true);
            }
            else {
                playAnimation(taxClientsTimeline_2, true, '/tax', taxClientsScroll, taxScroll, taxClientsButton_2, false, true, 4500);
            }
        }
        function animateTaxClients(event) {
            if (taxClientsButton_2) {
                event.preventDefault();
            }
            if (!taxClientsTimeline_2.children.length) {
                taxClientsTimeline_2.add({
                    targets: '.lines',
                    opacity: [1, 0],
                    duration: 1000
                }, 0).add({
                    targets: '#tax-1',
                    translateX: ['0', '-100%'],
                    duration: 1000
                }, 0).add({
                    targets: '.right-blob',
                    translateX: ['100%', '0'],
                    left: ['0', '-100%'],
                    duration: 2000
                }, 0).add({
                    targets: '.right-blob',
                    opacity: 0,
                    duration: 100
                }, 2000).add({
                    targets: '#tax-clients-1',
                    translateX: ['100%', '0'],
                    duration: 1000
                }, 500).add({
                    targets: '#tax-clients-1 #tax-item-0',
                    opacity: [0, 1],
                    duration: 1000
                }, 1500).add({
                    targets: '#tax-clients-1 #tax-item-1',
                    opacity: [0, 1],
                    duration: 1000
                }, 2500).add({
                    targets: '#tax-clients-1 #tax-item-2',
                    opacity: [0, 1],
                    duration: 1000
                }, 3500);
            }
            playAnimation(taxClientsTimeline_2, false, '/tax/clients', taxScroll, taxClientsScroll, taxClientsButton_2, true, false, 4500, -1, true);
            // set taxt clients next timeline
            setNextTimeline([
                [taxClientsTimeline2_2, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true],
                [taxClientsTimeline5_2, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true],
                [taxClientsTimeline6_2, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true]
            ], [
                [
                    [{
                            targets: '.bottom-blob',
                            translateY: ['100%', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#tax-clients-2',
                            translateY: ['100%', '0'],
                            duration: 1000,
                            zIndex: {
                                value: [2, 3],
                                round: true
                            }
                        }, 1000],
                    [{
                            targets: '#tax-clients-2 .side-title',
                            top: ['150%', '50%'],
                            duration: 1000
                        }, 2000],
                    [{
                            targets: '#tax-clients-2 .audit-facts h2',
                            opacity: [0, 0.25],
                            borderBottom: '1px solid #fff',
                            duration: 1000
                        }, 3000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-0 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 4000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-0 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 4000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-1 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 5000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-1 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 5000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-2 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 6000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-2 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 6000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-3 h2',
                            opacity: [0.25, 1],
                            borderColor: ['#fff', '#16e7cf'],
                            borderWidth: [1, 3],
                            duration: 1000
                        }, 7000],
                    [{
                            targets: '#tax-clients-2 .audit-facts .fact-3 p',
                            opacity: [0, 1],
                            duration: 1000
                        }, 7000]
                ],
                [
                    [{
                            targets: '.bottom-blob',
                            opacity: 0,
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-clients-2',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-clients-2',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 600],
                    [{
                            targets: '#tax-clients-5',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-clients-5',
                            translateX: ['100%', '0'],
                            duration: 1000
                        }, 600]
                ],
                [
                    [{
                            targets: '.new-blog',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#home-1, #audit-1, #tax-1',
                            height: ['100vh', 0],
                            opacity: [1, 0],
                            duration: 1
                        },
                        0
                    ],
                    [{
                            targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
                            opacity: [1, 0],
                            duration: 1
                        }, 0],
                    [{
                            targets: '.new-blog',
                            opacity: [0, 1],
                            duration: 1000
                        }, 0]
                ]
            ], true);
        }
        if (taxClientsButton_2) {
            taxClientsButton_2.addEventListener('click', animateTaxClients);
        }
        // Tax Candidates
        var taxCandidatesButton_2 = document.querySelector('#tax-candidates');
        var taxCandidatesTimeline_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxCandidatesTimeline2_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxCandidatesTimeline3_2 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // let taxCandidatesTimeline4 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        // function taxCandidatesScroll4(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // end tax candidates
        //    console.log('end tax candidates');
        //  } else {
        //    playAnimation(taxCandidatesTimeline4, true, 'tax/candidates3', taxCandidatesScroll4, taxCandidatesScroll3);
        //  }
        // }
        function taxCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxCandidatesTimeline4.add({
                //  targets: '#col-0, #col-1',
                //  translateX: ['0', '-100vw'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#col-2, #col-3',
                //  translateX: ['0', '100vw'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#mid-arrow',
                //  opacity: 0,
                //  duration: 100,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3 h1',
                //  translateY: ['0', '-100vh'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3',
                //  zIndex: {
                //    value: [3, 2],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 1000,
                // }).add({
                //  targets: '#tax-candidates-4',
                //  zIndex: {
                //    value: [1, 3],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 1000
                // }).add({
                //  targets: '#tax-candidates-4',
                //  opacity: [0, 1],
                //  duration: 1000,
                //  offset: 1000
                // }).add({
                //  targets: '#tax-candidates-4 .call-to-action',
                //  opacity: [0, 1],
                //  duration: 1000,
                //  offset: 2000
                // });
                // playAnimation(taxCandidatesTimeline4, false, 'tax/candidates4', taxCandidatesScroll3, taxCandidatesScroll4);
                // end tax candidates
                console.log('end tax candidates');
            }
            else {
                playAnimation(taxCandidatesTimeline3_2, true, 'tax/applicants2', taxCandidatesScroll3, taxCandidatesScroll2, false, false, false, 2100, -1, true);
            }
        }
        function taxCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxCandidatesTimeline3.add({
                //  targets: '.bottom-blob',
                //  translateY: ['100%', '-100%'],
                //  duration: 2000,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3',
                //  translateY: ['100%', '0'],
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                //  duration: 1000,
                //  offset: 1000,
                // }).add({
                //  targets: '#col-0 svg, #col-0 h2, #col-0 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 2000
                // }).add({
                //  targets: '#col-1 svg, #col-1 h2, #col-1 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 3000
                // }).add({
                //  targets: '#col-2 svg, #col-2 h2, #col-2 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 4000
                // }).add({
                //  targets: '#col-3 svg, #col-3 h2, #col-3 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 5000
                // });
                if (!taxCandidatesTimeline3_2.children.length) {
                    taxCandidatesTimeline3_2.add({
                        targets: '.right-blob',
                        left: ['-100%', '0'],
                        translateX: ['0', '-100%'],
                        opacity: [0, 1],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-candidates-2',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#tax-candidates-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-candidates-3 .container-fluid',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-candidates-1',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.lines',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-1',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '.right-blob',
                        translateX: ['100%', '0'],
                        left: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '.right-blob',
                        opacity: 0,
                        duration: 100
                    }, 2000).add({
                        targets: '#tax-candidates-1',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 500);
                }
                playAnimation(taxCandidatesTimeline3_2, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true);
            }
            else {
                playAnimation(taxCandidatesTimeline2_2, true, '/tax/applicants', taxCandidatesScroll2, taxCandidatesScroll, false, true, false, 6000, -1, true);
            }
        }
        function taxCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxCandidatesTimeline2_2.children.length) {
                    taxCandidatesTimeline2_2.add({
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#tax-candidates-3',
                        translateY: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 1000).add({
                        targets: '#col-0 svg, #col-0 h2, #col-0 p',
                        opacity: 1,
                        duration: 1000
                    }, 2000).add({
                        targets: '#col-1 svg, #col-1 h2, #col-1 p',
                        opacity: 1,
                        duration: 1000
                    }, 3000).add({
                        targets: '#col-2 svg, #col-2 h2, #col-2 p',
                        opacity: 1,
                        duration: 1000
                    }, 4000).add({
                        targets: '#col-3 svg, #col-3 h2, #col-3 p',
                        opacity: 1,
                        duration: 1000
                    }, 5000);
                }
                playAnimation(taxCandidatesTimeline2_2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true);
            }
            else {
                playAnimation(taxCandidatesTimeline_2, true, '/tax', taxScroll, taxCandidatesScroll, taxCandidatesButton_2, false, true, 3000);
            }
            // if (delta == '1') {
            // taxCandidatesTimeline2.add({
            //  targets: '#tax-candidates-1',
            //  translateX: ['0', '-100%'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2',
            //  marginLeft: [0, -1],
            //  duration: 1,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2',
            //  translateX: ['100%', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 #love',
            //  translateX: ['200%', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 #tax',
            //  translateX: ['-200vw', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 .content-wrap',
            //  opacity: [0, 1],
            //  duration: 1000,
            //  offset: 1000
            // });
            // playAnimation(taxCandidatesTimeline, false, 'tax/candidates2', taxCandidatesScroll, taxCandidatesScroll2, false, true, true);
            // }
        }
        function animateTaxCandidates(event) {
            if (taxCandidatesButton_2) {
                event.preventDefault();
            }
            if (!taxCandidatesTimeline_2.children.length) {
                taxCandidatesTimeline_2.add({
                    targets: '.lines',
                    opacity: [1, 0],
                    duration: 1000
                }, 0).add({
                    targets: '.right-blob',
                    translateX: ['100%', '0'],
                    left: ['0', '-100%'],
                    duration: 2000
                }, 0).add({
                    targets: '.right-blob',
                    opacity: [1, 0],
                    duration: 100
                }, 2000).add({
                    targets: '#tax-candidates-2',
                    marginLeft: [0, -1],
                    duration: 1
                }, 0).add({
                    targets: '#tax-candidates-2',
                    translateX: ['100%', '0'],
                    duration: 1000
                }, 500).add({
                    targets: '#tax-candidates-2 #love',
                    translateX: ['200%', '0'],
                    duration: 1000
                }, 1000).add({
                    targets: '#tax-candidates-2 #tax',
                    translateX: ['-200vw', '0'],
                    duration: 1000
                }, 1000).add({
                    targets: '#tax-candidates-2 .content-wrap',
                    opacity: [0, 1],
                    duration: 1000
                }, 2000);
            }
            playAnimation(taxCandidatesTimeline_2, false, '/tax/applicants', taxScroll, taxCandidatesScroll, taxCandidatesButton_2, true, false, 3000, -1, true);
            // set tat candidates next timeline
            setNextTimeline([
                [taxCandidatesTimeline2_2, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true],
                [taxCandidatesTimeline3_2, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true]
            ], [
                [
                    [{
                            targets: '.bottom-blob',
                            translateY: ['100%', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '#tax-candidates-3',
                            translateY: ['100%', '0'],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1000
                        }, 1000],
                    [{
                            targets: '#col-0 svg, #col-0 h2, #col-0 p',
                            opacity: 1,
                            duration: 1000
                        }, 2000],
                    [{
                            targets: '#col-1 svg, #col-1 h2, #col-1 p',
                            opacity: 1,
                            duration: 1000
                        }, 3000],
                    [{
                            targets: '#col-2 svg, #col-2 h2, #col-2 p',
                            opacity: 1,
                            duration: 1000
                        }, 4000],
                    [{
                            targets: '#col-3 svg, #col-3 h2, #col-3 p',
                            opacity: 1,
                            duration: 1000
                        }, 5000]
                ],
                [
                    [{
                            targets: '.right-blob',
                            left: ['-100%', '0'],
                            translateX: ['0', '-100%'],
                            opacity: [0, 1],
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-candidates-2',
                            opacity: [1, 0],
                            duration: 1
                        }, 0],
                    [{
                            targets: '#tax-candidates-3',
                            zIndex: {
                                value: [3, 2],
                                round: true
                            },
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#tax-candidates-3 .container-fluid',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#tax-candidates-1',
                            zIndex: {
                                value: [2, 3],
                                round: true
                            },
                            duration: 1
                        }, 0],
                    [{
                            targets: '.lines',
                            opacity: [1, 0],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '#tax-1',
                            translateX: ['0', '-100%'],
                            duration: 1000
                        }, 0],
                    [{
                            targets: '.right-blob',
                            translateX: ['100%', '0'],
                            left: ['0', '-100%'],
                            duration: 2000
                        }, 0],
                    [{
                            targets: '.right-blob',
                            opacity: 0,
                            duration: 100
                        }, 2000],
                    [{
                            targets: '#tax-candidates-1',
                            translateX: ['100%', '0'],
                            duration: 1000
                        }, 500]
                ]
            ], true);
        }
        if (taxCandidatesButton_2) {
            taxCandidatesButton_2.addEventListener('click', animateTaxCandidates);
        }
    }
    var isTaxClients = document.querySelector('.tax-clients-main');
    if (isTaxClients) {
        // Tax Clients
        route.current = '/tax/clients';
        if (!mobileDevice.phone()) {
            var header_1 = document.querySelector('.header');
            header_1.classList.add('header-light');
        }
        var taxClientsTimeline2_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // let taxClientsTimeline3 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        // let taxClientsTimeline4 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        var taxClientsTimeline5_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxClientsTimeline6_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // set taxt clients next timeline
        setNextTimeline([
            [taxClientsTimeline2_3, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true],
            [taxClientsTimeline5_3, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true],
            [taxClientsTimeline6_3, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true]
        ], [
            [
                [{
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#tax-clients-2',
                        translateY: ['100%', '0'],
                        duration: 1000,
                        zIndex: {
                            value: [2, 3],
                            round: true
                        }
                    }, 1000],
                [{
                        targets: '#tax-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 2000],
                [{
                        targets: '#tax-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 3000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 7000],
                [{
                        targets: '#tax-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 7000]
            ],
            [
                [{
                        targets: '.bottom-blob',
                        opacity: 0,
                        duration: 1
                    }, 0],
                [{
                        targets: '#tax-clients-2',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0],
                [{
                        targets: '#tax-clients-2',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 600],
                [{
                        targets: '#tax-clients-5',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0],
                [{
                        targets: '#tax-clients-5',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 600]
            ],
            [
                [{
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0],
                [{
                        targets: '#home-1, #audit-1, #tax-1',
                        height: ['100vh', 0],
                        opacity: [1, 0],
                        duration: 1
                    },
                    0
                ],
                [{
                        targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0],
                [{
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0]
            ]
        ]);
        function taxClientsScroll6(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // end tax clients
                console.log('end tax clients');
            }
            else {
                console.log('no scrolling inside blog');
            }
        }
        function taxClientsScroll5(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxClientsTimeline6_3.children.length) {
                    taxClientsTimeline6_3.add({
                        targets: '.new-blog',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#home-1, #audit-1, #tax-1',
                        height: ['100vh', 0],
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-5, #tax-clients-4, #tax-clients-3, #tax-clients-2, #tax-clients-1',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '.new-blog',
                        opacity: [0, 1],
                        duration: 1000
                    }, 0);
                }
                playAnimation(taxClientsTimeline6_3, false, '/blog', taxClientsScroll5, taxClientsScroll6, false, true, true, 1000, -1, false, true);
            }
            else {
                playAnimation(taxClientsTimeline5_3, true, 'tax/clients2', taxClientsScroll5, taxClientsScroll2, false, false, false, 1600, -1, true);
            }
        }
        // function taxClientsScroll4(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // end tax clients
        //    console.log('end tax clients');
        //  } else {
        //    playAnimation(taxClientsTimeline4, true, 'tax/clients3', taxClientsScroll4, taxClientsScroll3, false, true);
        //  }
        // }
        // function taxClientsScroll3(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // taxClientsTimeline4.add({
        //    //  targets: '.left-blob',
        //    //  opacity: 0,
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-3',
        //    //  zIndex: {
        //    //    value: [3, 2],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '.left-blob-blue',
        //    //  zIndex: {
        //    //    value: [2, 3],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-4',
        //    //  zIndex: {
        //    //    value: [2, 3],
        //    //    round: true
        //    //  },
        //    //  duration: 1,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '.left-blob-blue',
        //    //  translateX: ['100%', '-75%'],
        //    //  duration: 1500,
        //    //  offset: 0
        //    // }).add({
        //    //  targets: '#tax-clients-4',
        //    //  translateX: ['100%', '0'],
        //    //  duration: 1000,
        //    //  offset: 600
        //    // });
        //     // playAnimation(taxClientsTimeline4, false, 'tax/clients4', taxClientsScroll3, taxClientsScroll4);
        //     // end tax clients
        //    console.log('end tax clients');
        //  } else {
        //    playAnimation(taxClientsTimeline3, true, 'tax/clients2', taxClientsScroll3, taxClientsScroll2);
        //  }
        // }
        function taxClientsScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxClientsTimeline3.add({
                //  targets: '.bottom-blob',
                //  opacity: 0,
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '.left-blob',
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '#tax-clients-2',
                //  zIndex: {
                //    value: [3, 2],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 0
                // }).add({
                //  targets: '.left-blob',
                //  translateX: ['100%', '-50%'],
                //  duration: 1500,
                //  offset: 0
                // }).add({
                //  targets: '#tax-clients-3',
                //  translateX: ['100%', '0'],
                //  duration: 1000,
                //  offset: 500,
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                // });
                // playAnimation(taxClientsTimeline3, false, 'tax/clients3', taxClientsScroll2, taxClientsScroll3, false, true);
                // end tax clients
                // console.log('end tax clients');
                if (!taxClientsTimeline5_3.children.length) {
                    taxClientsTimeline5_3.add({
                        targets: '.bottom-blob',
                        opacity: 0,
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-2',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-2',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 600).add({
                        targets: '#tax-clients-5',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '#tax-clients-5',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 600);
                }
                playAnimation(taxClientsTimeline5_3, false, 'tax/clients5', taxClientsScroll2, taxClientsScroll5, false, false, false, 1600, -1, true);
            }
            else {
                playAnimation(taxClientsTimeline2_3, true, '/tax/clients', taxClientsScroll2, taxClientsScroll, false, true, false, 8000, -1, true);
                // staggerAnimation(taxClientsTimeline2);
            }
        }
        function taxClientsScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxClientsTimeline2_3.children.length) {
                    taxClientsTimeline2_3.add({
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#tax-clients-2',
                        translateY: ['100%', '0'],
                        duration: 1000,
                        zIndex: {
                            value: [2, 3],
                            round: true
                        }
                    }, 1000).add({
                        targets: '#tax-clients-2 .side-title',
                        top: ['150%', '50%'],
                        duration: 1000
                    }, 2000).add({
                        targets: '#tax-clients-2 .audit-facts h2',
                        opacity: [0, 0.25],
                        borderBottom: '1px solid #fff',
                        duration: 1000
                    }, 3000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-0 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 4000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-0 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 4000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-1 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 5000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-1 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 5000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-2 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 6000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-2 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 6000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-3 h2',
                        opacity: [0.25, 1],
                        borderColor: ['#fff', '#16e7cf'],
                        borderWidth: [1, 3],
                        duration: 1000
                    }, 7000).add({
                        targets: '#tax-clients-2 .audit-facts .fact-3 p',
                        opacity: [0, 1],
                        duration: 1000
                    }, 7000);
                }
                playAnimation(taxClientsTimeline2_3, false, 'tax/clients2', taxClientsScroll, taxClientsScroll2, false, false, true, 8000, -1, true);
            }
        }
        window.addEventListener('wheel', taxClientsScroll);
        window.addEventListener('touchmove', taxClientsScroll);
    }
    var isTaxCandidates = document.querySelector('.tax-candidates-main');
    if (isTaxCandidates) {
        // Tax Candidates
        route.current = '/tax/applicants';
        if (!mobileDevice.phone()) {
            var header_2 = document.querySelector('.header');
            header_2.classList.add('header-light');
        }
        var taxCandidatesTimeline2_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        var taxCandidatesTimeline3_3 = anime.timeline({
            easing: 'easeInOutSine',
            autoplay: false
        });
        // let taxCandidatesTimeline4 = anime.timeline({
        //  easing: 'easeInOutSine',
        //  autoplay: false
        // });
        // function taxCandidatesScroll4(event) {
        //  let delta = Math.sign(event.deltaY);
        //  if (event.touches) {
        //    delta = handleTouchMove(event);
        //  }
        //  if (delta == '1') {
        //    // end tax candidates
        //    console.log('end tax candidates');
        //  } else {
        //    playAnimation(taxCandidatesTimeline4, true, 'tax/candidates3', taxCandidatesScroll4, taxCandidatesScroll3);
        //  }
        // }
        // set tat candidates next timeline
        setNextTimeline([
            [taxCandidatesTimeline2_3, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true],
            [taxCandidatesTimeline3_3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true]
        ], [
            [
                [{
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '#tax-candidates-3',
                        translateY: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 1000],
                [{
                        targets: '#col-0 svg, #col-0 h2, #col-0 p',
                        opacity: 1,
                        duration: 1000
                    }, 2000],
                [{
                        targets: '#col-1 svg, #col-1 h2, #col-1 p',
                        opacity: 1,
                        duration: 1000
                    }, 3000],
                [{
                        targets: '#col-2 svg, #col-2 h2, #col-2 p',
                        opacity: 1,
                        duration: 1000
                    }, 4000],
                [{
                        targets: '#col-3 svg, #col-3 h2, #col-3 p',
                        opacity: 1,
                        duration: 1000
                    }, 5000]
            ],
            [
                [{
                        targets: '.right-blob',
                        left: ['-100%', '0'],
                        translateX: ['0', '-100%'],
                        opacity: [0, 1],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0],
                [{
                        targets: '#tax-candidates-2',
                        opacity: [1, 0],
                        duration: 1
                    }, 0],
                [{
                        targets: '#tax-candidates-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1000
                    }, 0],
                [{
                        targets: '#tax-candidates-3 .container-fluid',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#tax-candidates-1',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0],
                [{
                        targets: '.lines',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0],
                [{
                        targets: '#tax-1',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0],
                [{
                        targets: '.right-blob',
                        translateX: ['100%', '0'],
                        left: ['0', '-100%'],
                        duration: 2000
                    }, 0],
                [{
                        targets: '.right-blob',
                        opacity: 0,
                        duration: 100
                    }, 2000],
                [{
                        targets: '#tax-candidates-1',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 500]
            ]
        ]);
        function taxCandidatesScroll3(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxCandidatesTimeline4.add({
                //  targets: '#col-0, #col-1',
                //  translateX: ['0', '-100vw'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#col-2, #col-3',
                //  translateX: ['0', '100vw'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#mid-arrow',
                //  opacity: 0,
                //  duration: 100,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3 h1',
                //  translateY: ['0', '-100vh'],
                //  duration: 1000,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3',
                //  zIndex: {
                //    value: [3, 2],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 1000,
                // }).add({
                //  targets: '#tax-candidates-4',
                //  zIndex: {
                //    value: [1, 3],
                //    round: true
                //  },
                //  duration: 1,
                //  offset: 1000
                // }).add({
                //  targets: '#tax-candidates-4',
                //  opacity: [0, 1],
                //  duration: 1000,
                //  offset: 1000
                // }).add({
                //  targets: '#tax-candidates-4 .call-to-action',
                //  opacity: [0, 1],
                //  duration: 1000,
                //  offset: 2000
                // });
                // playAnimation(taxCandidatesTimeline4, false, 'tax/candidates4', taxCandidatesScroll3, taxCandidatesScroll4);
                // end tax candidates
                console.log('end tax candidates');
            }
            else {
                playAnimation(taxCandidatesTimeline3_3, true, 'tax/applicants2', taxCandidatesScroll3, taxCandidatesScroll2, false, false, false, 2100, -1, true);
            }
        }
        function taxCandidatesScroll2(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                // taxCandidatesTimeline3.add({
                //  targets: '.bottom-blob',
                //  translateY: ['100%', '-100%'],
                //  duration: 2000,
                //  offset: 0
                // }).add({
                //  targets: '#tax-candidates-3',
                //  translateY: ['100%', '0'],
                //  zIndex: {
                //    value: [2, 3],
                //    round: true
                //  },
                //  duration: 1000,
                //  offset: 1000,
                // }).add({
                //  targets: '#col-0 svg, #col-0 h2, #col-0 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 2000
                // }).add({
                //  targets: '#col-1 svg, #col-1 h2, #col-1 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 3000
                // }).add({
                //  targets: '#col-2 svg, #col-2 h2, #col-2 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 4000
                // }).add({
                //  targets: '#col-3 svg, #col-3 h2, #col-3 p',
                //  opacity: 1,
                //  duration: 1000,
                //  offset: 5000
                // });
                if (!taxCandidatesTimeline3_3.children.length) {
                    taxCandidatesTimeline3_3.add({
                        targets: '#tax-candidates-2',
                        opacity: [1, 0],
                        duration: 1
                    }, 0).add({
                        targets: '#tax-candidates-3',
                        zIndex: {
                            value: [3, 2],
                            round: true
                        },
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-candidates-3 .container-fluid',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-candidates-1',
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1
                    }, 0).add({
                        targets: '.lines',
                        opacity: [1, 0],
                        duration: 1000
                    }, 0).add({
                        targets: '#tax-1',
                        translateX: ['0', '-100%'],
                        duration: 1000
                    }, 0).add({
                        targets: '.right-blob',
                        translateX: ['100%', '0'],
                        left: ['0', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '.right-blob',
                        opacity: [1, 0],
                        duration: 100
                    }, 2000).add({
                        targets: '#tax-candidates-1',
                        translateX: ['100%', '0'],
                        duration: 1000
                    }, 500);
                }
                playAnimation(taxCandidatesTimeline3_3, false, 'tax/applicants3', taxCandidatesScroll2, taxCandidatesScroll3, false, true, false, 2100, -1, true);
            }
            else {
                playAnimation(taxCandidatesTimeline2_3, true, '/tax/applicants', taxCandidatesScroll2, taxCandidatesScroll, false, true, true, 6000, -1, true);
            }
        }
        function taxCandidatesScroll(event) {
            var delta = Math.sign(event.deltaY);
            if (event.touches) {
                delta = handleTouchMove(event);
            }
            if (delta == '1') {
                if (!taxCandidatesTimeline2_3.children.length) {
                    taxCandidatesTimeline2_3.add({
                        targets: '.bottom-blob',
                        translateY: ['100%', '-100%'],
                        duration: 2000
                    }, 0).add({
                        targets: '#tax-candidates-3',
                        translateY: ['100%', '0'],
                        zIndex: {
                            value: [2, 3],
                            round: true
                        },
                        duration: 1000
                    }, 1000).add({
                        targets: '#col-0 svg, #col-0 h2, #col-0 p',
                        opacity: 1,
                        duration: 1000
                    }, 2000).add({
                        targets: '#col-1 svg, #col-1 h2, #col-1 p',
                        opacity: 1,
                        duration: 1000
                    }, 3000).add({
                        targets: '#col-2 svg, #col-2 h2, #col-2 p',
                        opacity: 1,
                        duration: 1000
                    }, 4000).add({
                        targets: '#col-3 svg, #col-3 h2, #col-3 p',
                        opacity: 1,
                        duration: 1000
                    }, 5000);
                }
                playAnimation(taxCandidatesTimeline2_3, false, 'tax/applicants2', taxCandidatesScroll, taxCandidatesScroll2, false, false, false, 6000, -1, true);
            }
            // if (delta == '1') {
            // taxCandidatesTimeline2.add({
            //  targets: '#tax-candidates-1',
            //  translateX: ['0', '-100%'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2',
            //  marginLeft: [0, -1],
            //  duration: 1,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2',
            //  translateX: ['100%', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 #love',
            //  translateX: ['200%', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 #tax',
            //  translateX: ['-200vw', '0'],
            //  duration: 1000,
            //  offset: 0
            // }).add({
            //  targets: '#tax-candidates-2 .content-wrap',
            //  opacity: [0, 1],
            //  duration: 1000,
            //  offset: 1000
            // });
            // playAnimation(taxCandidatesTimeline, false, 'tax/candidates2', taxCandidatesScroll, taxCandidatesScroll2, false, true, true);
            // }
        }
        window.addEventListener('wheel', taxCandidatesScroll);
        window.addEventListener('touchmove', taxCandidatesScroll);
    }
    var isBlog = document.querySelector('.new-blog');
    var isMainBlog = document.querySelector('.main-new-blog');
    var isBlogSingle = document.querySelector('.single-post');
    if (isBlogSingle || isBlog) {
        shaveBlogs();
    }
    if (isMainBlog) {
        route.current = '/blog';
        header.classList.add('header-light');
    }
    if (isBlog) {
        // truncate
        function truncate(content) {
            Array.prototype.forEach.call(content, function (post) {
                setTimeout(function () {
                    post.style.height = post.offsetWidth + 'px';
                    var heading = post.querySelector('h2');
                    var headingLink = post.querySelector('h2 a');
                    var headingHeight = heading.offsetHeight;
                    var dateHeight = post.querySelector('time').offsetHeight;
                    var maxHeight = post.offsetHeight - 32; // padding
                    var paragraphHeight = maxHeight - headingHeight - dateHeight;
                    var paragraph = post.querySelector('p');
                    if (heading.offsetHeight >= 72) {
                        headingHeight = 72;
                    }
                    shave(headingLink, headingHeight);
                    shave(paragraph, paragraphHeight);
                }, 100);
            });
        }
        var postContent_1 = document.querySelectorAll('.new-blog .slider .slide .blog-post .post-content');
        truncate(postContent_1);
        window.addEventListener('resize', function () {
            truncate(postContent_1);
        });
        if (!mobileDevice.phone()) {
            // slider
            var slider = new Swiper('.blog-slider', {
                spaceBetween: 40,
                mousewheel: true,
                navigation: {
                    nextEl: '.slider-nav-wrap .next',
                    prevEl: '.slider-nav-wrap .prev'
                }
            });
        }
    }
    var isClients = document.querySelector('.clients-main');
    if (isClients) {
        route.current = '/clients';
    }
    var isContact = document.querySelector('.contact-main');
    if (isContact) {
        route.current = '/contact';
    }
    var isTeam = document.querySelector('.main-team');
    if (isTeam) {
        route.current = '/team';
    }
    var isBlogSingle = document.querySelector('.single-post');
    if (isBlogSingle) {
        if (!mobileDevice.phone()) {
            var header_3 = document.querySelector('.header');
            header_3.classList.add('header-light');
        }
    }
    var instagram = document.querySelector('.insta');
    if (instagram) {
        loadInstagram();
    }
    var isApply = document.querySelector('.apply-main');
    if (isApply) {
        if (!mobileDevice.phone()) {
            var header_4 = document.querySelector('.header');
            header_4.classList.add('header-light');
        }
    }
    var isAuditCost = document.querySelector('.audit-cost-main');
    if (isAuditCost) {
        if (!mobileDevice.phone()) {
            var header_5 = document.querySelector('.header');
            header_5.classList.add('header-light');
        }
    }
    var isHowWorks = document.querySelector('.how-works-main');
    if (isHowWorks) {
        if (!mobileDevice.phone()) {
            var header_6 = document.querySelector('.header');
            header_6.classList.add('header-light');
        }
    }
    var isThanks = document.querySelector('.thanks-main');
    if (isThanks) {
        if (!mobileDevice.phone()) {
            var header_7 = document.querySelector('.header');
            header_7.classList.add('header-light');
        }
    }
    var isCaseStudy = document.querySelector('.case-study-main');
    if (isCaseStudy) {
        if (!mobileDevice.phone()) {
            var header_8 = document.querySelector('.header');
            header_8.classList.add('header-light');
        }
    }
    // $('.covid-banner').on('closed.bs.alert', function () {
    //   body.style.paddingTop = 0;
    // });
});
