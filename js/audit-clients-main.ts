const isItAuditClients = document.querySelector('.it-audit-clients-main');

if (isItAuditClients) {
  // Audit Clients
  route.current = '/it-audit/clients';

  let itAuditClientsTimeline2 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditClientsTimeline3 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditClientsTimeline4 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditClientsTimeline5 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditClientsTimeline6 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });
  let itAuditClientsTimeline7 = anime.timeline({
    easing: 'easeInOutSine',
    autoplay: false
  });

  if (!itAuditClientsTimeline2.children.length) {
    itAuditClientsTimeline2.add({
      targets: '.welcome-content',
      translateY: ['0', '-100%'],
      duration: 1000,
    }, 0).add({
      targets: '.why-section, .why-content',
      translateY: ['100%', '0'],
      opacity: [0, 1],
      duration: 1000,
    }, 0).add({
      targets: '.why-content .section-title',
      translateY: ['100%', '0'],
      rotate: ['-90deg', '-90deg'],
      duration: 1000,
    }, 0).add({
      targets: '.why-section .fact-0',
      opacity: [0, 1],
      duration: 1000,
    }, 1000).add({
        targets: '.why-section .fact-1',
      opacity: [0, 1],
      duration: 1000,
    }, 2000).add({
        targets: '.why-section .fact-2',
      opacity: [0, 1],
      duration: 1000,
    }, 3000);
  }

  // set audit clients next timeline
  setNextTimeline(
    [
      [itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true],
      [itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true],
      [itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true],
      [itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true],
      [itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true],
      [itAuditClientsTimeline7, false, 'clients7', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true]
    ],
    [
      [
        [
          {
            targets: '.welcome-content',
            translateY: ['0', '-100%'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-section, .why-content',
            translateY: ['100%', '0'],
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-content .section-title',
            translateY: ['100%', '0'],
            rotate: ['-90deg', '-90deg'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-section, .why-content',
            translateY: ['100%', '0'],
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-content .section-title',
            translateY: ['100%', '0'],
            rotate: ['-90deg', '-90deg'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '.why-section .fact-0',
            opacity: [0, 1],
            duration: 1000,
          },
          1000
        ],
        [
          {
            targets: '.why-section .fact-1',
            opacity: [0, 1],
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '.why-section .fact-2',
            opacity: [0, 1],
            duration: 1000,
          },
          3000
        ]
      ],
      [
        [
          {
            targets: '.top-blob',
            top: ['0', '100%'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-2',
            translateY: ['-100%', '0'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-2 .side-title',
            top: ['150%', '50%'],
            duration: 1000,
          },
          1000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts h2',
            opacity: [0, 0.25],
            borderBottom: '1px solid #fff',
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          3000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
            opacity: [0, 1],
            duration: 1000,
          },
          3000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          4000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
            opacity: [0, 1],
            duration: 1000,
          },
          4000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          5000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
            opacity: [0, 1],
            duration: 1000,
          },
          5000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
            opacity: [0.25, 1],
            borderColor: ['#fff', '#16e7cf'],
            borderWidth: [1, 3],
            duration: 1000,
          },
          6000
        ],
        [
          {
            targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
            opacity: [0, 1],
            duration: 1000,
          },
          6000
        ]
      ],
      [
        [
          {
            targets: '.left-blob',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '.left-blob',
            translateX: ['100%', '-50%'],
            duration: 1500,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-3',
            translateX: ['100%', '0'],
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1000,
          },
          500
        ]
      ],
      [
        [
          {
            targets: '.left-blob',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-3',
            zIndex: {
              value: [3, 2],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-3',
            translateX: ['0', '-100%'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-4',
            translateX: ['100%', '0'],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-4 .first-title',
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-4 .second-title',
            opacity: [0, 1],
            duration: 1000,
          },
          1000
        ],
        [
          {
            targets: '#it-audit-clients-4 .third-title',
            opacity: [0, 1],
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '#it-audit-clients-4 p',
            opacity: [0, 1],
            duration: 1000,
          },
          2000
        ],
        [
          {
            targets: '#it-audit-clients-4 .industries',
            opacity: [0, 1],
            duration: 1000,
          },
          3000
        ],
        [
          {
            targets: '.lines-single .top-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .bot-line path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .hor-line-1 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .hor-line-2 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ],
        [
          {
            targets: '.lines-single .hor-line-3 path',
            strokeDashoffset: [anime.setDashoffset, 0],
            delay: function (el, i) { return i * 250 },
            duration: 3000,
          },
          0
        ]
      ],
      [
        [
          {
            targets: '#it-audit-clients-5',
            translateY: ['-100%', '0'],
            duration: 1000,
          }, 0
        ]
      ],
      [
        [
          {
            targets: '.new-blog',
            zIndex: {
              value: [2, 3],
              round: true
            },
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
            opacity: [1, 0],
            duration: 1,
          },
          0
        ],
        [
          {
            targets: '.new-blog',
            opacity: [0, 1],
            duration: 1000,
          },
          0
        ]
      ],
    ]
  );

  function itAuditClientsScroll7(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      // end of audit clients
      console.log('end audit clients animations');
    } else {
      console.log('no scrolling events inside blog');
    }
  }

  function itAuditClientsScroll6(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditClientsTimeline7.children.length) {
        itAuditClientsTimeline7.add({
          targets: '.new-blog',
          zIndex: {
            value: [2, 3],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '#it-audit-clients-5, #it-audit-clients-4, #it-audit-clients-3, #it-audit-clients-2, #it-audit-clients-1',
          opacity: [1, 0],
          duration: 1,
        }, 0).add({
          targets: '.new-blog',
          opacity: [0, 1],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditClientsTimeline7, false, '/blog', itAuditClientsScroll6, itAuditClientsScroll7, false, true, true, 1000, -1, false, true);
    } else {
      playAnimation(itAuditClientsTimeline6, true, 'clients5', itAuditClientsScroll6, itAuditClientsScroll5, false, true, false, 1000, -1, true);
    }
  }

  function itAuditClientsScroll5(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditClientsTimeline6.children.length) {
        itAuditClientsTimeline6.add({
          targets: '#it-audit-clients-5',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0);
      }

      playAnimation(itAuditClientsTimeline6, false, 'clients6', itAuditClientsScroll5, itAuditClientsScroll6, false, false, false, 1000, -1, true);
    } else {
      playAnimation(itAuditClientsTimeline5, true, 'clients4', itAuditClientsScroll5, itAuditClientsScroll4, false, true, false, 4000, -1, true);
    }
  }

  function itAuditClientsScroll4(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditClientsTimeline5.children.length) {
        itAuditClientsTimeline5.add({
          targets: '.left-blob',
          zIndex: {
            value: [3, 2],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '#it-audit-clients-3',
          zIndex: {
            value: [3, 2],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '#it-audit-clients-3',
          translateX: ['0', '-100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-clients-4',
          translateX: ['100%', '0'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-clients-4 .first-title',
          opacity: [0, 1],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-clients-4 .second-title',
          opacity: [0, 1],
          duration: 1000,
        }, 1000).add({
          targets: '#it-audit-clients-4 .third-title',
          opacity: [0, 1],
          duration: 1000,
        }, 2000).add({
          targets: '#it-audit-clients-4 p',
          opacity: [0, 1],
          duration: 1000,
        }, 2000).add({
          targets: '#it-audit-clients-4 .industries',
          opacity: [0, 1],
          duration: 1000,
        }, 3000).add({
          targets: '.lines-single .top-line path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .bot-line path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .hor-line-1 path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .hor-line-2 path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0).add({
          targets: '.lines-single .hor-line-3 path',
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: function (el, i) { return i * 250 },
          duration: 3000,
        }, 0);
      }

      playAnimation(itAuditClientsTimeline5, false, 'clients5', itAuditClientsScroll4, itAuditClientsScroll5, false, true, false, 4000, -1, true);
    } else {
      playAnimation(itAuditClientsTimeline4, true, 'clients3', itAuditClientsScroll4, itAuditClientsScroll3, false, false, false, 1500, -1, true);
    }
  }

  function itAuditClientsScroll3(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditClientsTimeline4.children.length) {
        itAuditClientsTimeline4.add({
          targets: '.left-blob',
          zIndex: {
            value: [2, 3],
            round: true
          },
          duration: 1,
        }, 0).add({
          targets: '.left-blob',
          translateX: ['100%', '-50%'],
          duration: 1500,
        }, 0).add({
          targets: '#it-audit-clients-3',
          translateX: ['100%', '0'],
          zIndex: {
            value: [2, 3],
            round: true
          },
          duration: 1000,
        }, 500);
      }

      playAnimation(itAuditClientsTimeline4, false, 'clients4', itAuditClientsScroll3, itAuditClientsScroll4, false, true, false, 1500, -1, true);
    } else {
      playAnimation(itAuditClientsTimeline3, true, 'clients2', itAuditClientsScroll3, itAuditClientsScroll2, false, false, false, 7000, -1, true);
    }
  }

  function itAuditClientsScroll2(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      if (!itAuditClientsTimeline3.children.length) {
        itAuditClientsTimeline3.add({
          targets: '.top-blob',
          top: ['0', '100%'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-clients-2',
          translateY: ['-100%', '0'],
          duration: 1000,
        }, 0).add({
          targets: '#it-audit-clients-2 .side-title',
          top: ['150%', '50%'],
          duration: 1000,
        }, 1000).add({
          targets: '#it-audit-clients-2 .audit-facts h2',
          opacity: [0, 0.25],
          borderBottom: '1px solid #fff',
          duration: 1000,
        }, 2000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-0 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 3000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-0 p',
          opacity: [0, 1],
          duration: 1000,
        }, 3000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-1 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 4000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-1 p',
          opacity: [0, 1],
          duration: 1000,
        }, 4000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-2 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 5000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-2 p',
          opacity: [0, 1],
          duration: 1000,
        }, 5000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-3 h2',
          opacity: [0.25, 1],
          borderColor: ['#fff', '#16e7cf'],
          borderWidth: [1, 3],
          duration: 1000,
        }, 6000).add({
          targets: '#it-audit-clients-2 .audit-facts .fact-3 p',
          opacity: [0, 1],
          duration: 1000,
        }, 6000);
      }

      playAnimation(itAuditClientsTimeline3, false, 'clients3', itAuditClientsScroll2, itAuditClientsScroll3, false, false, false, 7000, -1, true);
    } else {
      playAnimation(itAuditClientsTimeline2, true, '/it-audit/clients', itAuditClientsScroll2, itAuditClientsScroll, false, false, false, 4000, -1, true);
    }
  }

  function itAuditClientsScroll(event) {
    let delta = Math.sign(event.deltaY);

    if (event.touches) {
      delta = handleTouchMove(event);
    }

    if (delta == '1') {
      playAnimation(itAuditClientsTimeline2, false, 'clients2', itAuditClientsScroll, itAuditClientsScroll2, false, false, false, 4000, -1, true);
    }
  }

  window.addEventListener('wheel', itAuditClientsScroll);
  window.addEventListener('touchmove', itAuditClientsScroll);
}
