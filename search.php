<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Makosi
 */

get_header();
?>

<main class="main page-single">
	<div class="container single-content">
		<h1><?php esc_html_e( 'Search results for:' ); ?> <strong><?php the_search_query(); ?></strong></h1>
		<?php get_template_part( 'template-parts/content', 'posts' ); ?>
	</div>
</main>

<?php
get_footer();
