<?php
/**
 * The blog single template file
 *
 * Template Name: Blog Single
 *
 * @package Makosi
 */

$prefix = 'options-';

get_header();
?>

<main class="main main-single">
	<?php if ( is_acf( $prefix . 'blog-title', 'option' ) ) : ?>
		<div class="side-title"><?php the_acf( $prefix . 'blog-title', 'option' ); ?></div>
	<?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<section class="blog-single">
					<div class="blog-nav">
						<a class="prev nav-arrow" href="/blog/single"></a>
						<a class="tiles" href="/blog">
							<span></span>
							<span></span>
							<span></span>
						</a>
						<a class="next nav-arrow" href="/blog/single"></a>
					</div>
					<div class="featured">
						<time datetime="2018-02-08 20:00">
							<strong>02</strong>
							<span>AUG</span>
							<span>2018</span>
						</time>
						<img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-1@2x.jpg" alt="Blog Featured">
					</div>
					<div class="single-content">
						<h1 class="single-title">Makosi - Serving the community</h1>
						<div class="author">Posted by Darren Isaacs</div>
						<h1>Title 1</h1>
						<h2>Title 2</h2>
						<h3>Title 3</h3>
						<h4>Title 4</h4>
						<h5>Title 5</h5>
						<h6>Title 6</h6>
						<p>On Feb 18, 2016; Makosi’s internal team rolled up their sleeves and marched together to the NY Common Pantry to contribute over 100 hours of community service.  The NY Common Pantry mission is to reduce hunger and promote dignity and self-sufficiency.</p>
						<p><strong>Why was it important for Makosi to give back to the community?</strong></p>
						<p>Makosi is strongly rooted in the idea that ‘we need to contribute with our hands’. This was a perfect way to work as a team and to contribute to a fantastic organization that reduces hunger every day in NYC.  It was empowering to see how efficiently we can work towards a common goal.</p>
						<p><strong>What was the day like?</strong></p>
						<p>We arrived at 9am and started to open up boxes filled with food supplies: bananas, cucumbers, raisins, etc…  We separated and packed the food items into smaller packages. Some of our team members took food orders from families, others filled the orders while others carried boxes and food supplies. We filled over 200 food orders that day. In addition, we had fun! We started competitions of ‘productivity’ and joked with the NY Common Pantry about the importance of ‘bag therapy’.</p>
						<p><strong>Will we do it again?</strong></p>
						<p>Absolutely! We were exhausted at 3pm from all the ‘manual labor’ yet we all walked out with smiles on our faces.  We are proud to be able to support the NY Common Pantry and the good work they do, and we thank Makosi’s employees for their willingness to give back.</p>
						<p>Check out more Makosi pictures on our Facebook Page:<a href="https://www.facebook.com/MakosiConsulting/" target="_blank"> https://www.facebook.com/MakosiConsulting/</a></p>
						<ul>
							<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
							<li>Iusto, repellat aspernatur temporibus pariatur dicta maiores id, officia veniam vel facilis ipsam cupiditate aperiam expedita possimus tempore fuga aliquid quaerat soluta.</li>
							<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
							<li>Iusto, repellat aspernatur temporibus pariatur dicta maiores id, officia veniam vel facilis ipsam cupiditate aperiam expedita possimus tempore fuga aliquid quaerat soluta.</li>
						</ul>
						<ol>
							<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
							<li>Iusto, repellat aspernatur temporibus pariatur dicta maiores id, officia veniam vel facilis ipsam cupiditate aperiam expedita possimus tempore fuga aliquid quaerat soluta.</li>
							<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
							<li>Iusto, repellat aspernatur temporibus pariatur dicta maiores id, officia veniam vel facilis ipsam cupiditate aperiam expedita possimus tempore fuga aliquid quaerat soluta.</li>
						</ol>
						<p class="gallery"><a href="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" data-fancybox="gallery" data-type="image"><img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" alt="Blog"></a><a href="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-3@2x.jpg" data-fancybox="gallery" data-type="image"><img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-3@2x.jpg" alt="Blog"></a><a href="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-4@2x.jpg" data-fancybox="gallery" data-type="image"><img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-4@2x.jpg" alt="Blog"></a><a href="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" data-fancybox="gallery" data-type="image"><img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" alt="Blog"></a><a href="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-3@2x.jpg" data-fancybox="gallery" data-type="image"><img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-3@2x.jpg" alt="Blog"></a><a href="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-4@2x.jpg" data-fancybox="gallery" data-type="image"><img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-4@2x.jpg" alt="Blog"></a></p>
						<p><img class="alignleft" src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" alt="Blog"></p>
						<p><img class="alignright" src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" alt="Blog"></p>
						<p><img class="aligncenter" src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-2@2x.jpg" alt="Blog"></p>
					</div>
				</section>
			</div>
			<div class="col-md-4">
				<aside class="sidebar">
					<?php get_template_part( 'template-parts/social' ); ?>

					<div class="recent-news">
						<h2 class="single-title">Recent News</h2>
						<div class="news-item">
							<time datetime="2018-02-08 20:00">May 16, 2019</time>
							<h3><a href="/blog/single">Employee of the Month (May 2019) – Jane Shkolnicova</a></h3>
						</div>
						<div class="news-item">
							<time datetime="2018-02-08 20:00">August 2, 2018</time>
							<h3><a href="/blog/single">Welcome to The Team – Karla Zaballero</a></h3>
						</div>
						<div class="news-item">
							<time datetime="2018-02-08 20:00">August 26, 2016</time>
							<h3><a href="/blog/single">We made the Inc. 5000 list…again!</a></h3>
						</div>
						<div class="news-item">
							<time datetime="2018-02-08 20:00">May 23, 2016</time>
							<h3><a href="/blog/single">Leaders of Makosi – Amod</a></h3>
						</div>
						<div class="news-item">
							<time datetime="2018-02-08 20:00">March 28, 2016</time>
							<h3><a href="/blog/single">Leaders of Makosi</a></h3>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</main>

<?php
get_footer();
