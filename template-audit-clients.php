<?php
/**
 * The audit clients template file
 *
 * Template Name: Audit Clients
 *
 * @package Makosi
 */

get_header();
?>

<main class="main audit-clients-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/audit', 'clients' ); ?>
	<?php get_template_part( 'template-parts/new-blog' ); ?>
</main>

<?php
get_footer();
