<?php
/**
 * The case study template file
 *
 * Template Name: Case Study
 *
 * @package Makosi
 */

get_header();
?>

<main class="main case-study-main">
	<?php get_template_part( 'template-parts/case-study' ); ?>
</main>

<?php
get_footer();
