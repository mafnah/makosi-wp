<?php
/**
 * The tax clients template file
 *
 * Template Name: Tax Clients
 *
 * @package Makosi
 */

get_header();
?>

<main class="main tax-clients-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/tax', 'clients' ); ?>
	<?php get_template_part( 'template-parts/new-blog' ); ?>
</main>

<?php
get_footer();
