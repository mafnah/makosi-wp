<?php
/**
 * The contact template file
 *
 * Template Name: Contact
 *
 * @package Makosi
 */

get_header();
?>

<main class="main contact-main">
	<?php get_template_part( 'template-parts/contact' ); ?>
</main>

<?php
get_footer();
