<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Makosi
 */

get_header();
?>

<main class="main page-single">
	<div class="container single-content">
		<h1><?php esc_html_e( '404 Not Found' ); ?></h1>
		<p><?php esc_html_e( 'Sorry, but the page you\'re looking for can\'t be found.' ); ?></p>
	</div>
</main>

<?php
get_footer();
