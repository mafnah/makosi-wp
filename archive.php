<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

get_header();
?>

<main class="main page-single">
	<div class="container single-content">
		<h1><?php the_archive_title(); ?></h1>
		<?php get_template_part( 'template-parts/content', 'posts' ); ?>
	</div>
</main>

<?php
get_footer();
