<?php
/**
 * Template part for displaying the search form
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

?>

<form method="get" class="searchform" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
		<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'search', 'makosi' ); ?>" />
		<div class="input-group-append">
			<button type="submit">
				<i class="fa fa-search"></i>
			</button>
		</div>
	</div>
</form>
