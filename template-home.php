<?php
/**
 * The home template file
 *
 * Template Name: Home
 *
 * @package Makosi
 */

get_header();
?>

<main class="main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/home' ); ?>

	<div class="mobile-hide">
		<?php get_template_part( 'template-parts/audit' ); ?>
		<?php get_template_part( 'template-parts/it-audit' ); ?>
	</div>
</main>

<?php
get_footer();
