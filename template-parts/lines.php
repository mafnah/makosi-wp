<?php
/**
 * Lines and Blobs
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

?>
<div class="mobile-hide">
	<div class="lines">
		<?php the_svg( 'images/lines-complete.svg' ); ?>
	</div>
	<div class="top-blob">
		<?php the_svg( 'images/top-blob-cut-pad.svg' ); ?>
	</div>
	<div class="bottom-blob">
		<?php the_svg( 'images/bottom-blob-cut.svg' ); ?>
	</div>
	<div class="left-blob">
		<?php the_svg( 'images/left-blob-cut.svg' ); ?>
	</div>
	<div class="left-blob-blue">
		<?php the_svg( 'images/left-blob-blue-cut.svg' ); ?>
	</div>
	<div class="right-blob">
		<?php the_svg( 'images/right-blob-cut.svg' ); ?>
	</div>
	<div class="stroke-blob">
		<?php the_svg( 'images/stroke-blob.svg' ); ?>
	</div>
</div>
