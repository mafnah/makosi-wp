<?php
/**
 * Audit Candidates Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'audit-candidates-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'audit/applicants' );
		?>
		<section class="candidates-section candidates-section-2 audit-section-7 d-flex align-items-center" id="audit-candidates-1">
			<div class="container-fluid">
				<div class="row text-center">
					<div class="col">
						<div id="audit-candidates-1-1">
							<?php if ( is_acf( $prefix . 'title' ) ) : ?>
								<?php the_acf( $prefix . 'title' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'content' ) ) : ?>
								<?php the_acf( $prefix . 'content' ); ?>
							<?php endif; ?>
						</div>
						<div id="audit-candidates-1-2">
							<div class="row icon-columns">
								<?php if ( is_acf( $prefix . 'icons' ) ) : ?>
									<?php $icons = get_field( $prefix . 'icons' ); ?>
									<?php foreach ( $icons as $icon ) : ?>
										<div class="col-4">
											<?php if ( is_acf_field( $icon[ $prefix . 'repeater-image' ] ) ) : ?>
												<?php the_acf_field_image( $icon[ $prefix . 'repeater-image' ] ); ?>
											<?php endif; ?>

											<?php if ( is_acf_field( $icon[ $prefix . 'repeater-title' ] ) ) : ?>
												<?php the_acf_field( $icon[ $prefix . 'repeater-title' ] ); ?>
											<?php endif; ?>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="col audit-section-8" id="audit-candidates-1-3">
						<div class="container">
							<?php if ( is_acf( $prefix . 'title-2' ) ) : ?>
								<?php the_acf( $prefix . 'title-2' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'content-2' ) ) : ?>
								<?php the_acf( $prefix . 'content-2' ); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="audit-section-2 audit-section-3 audit-section-7 audit-section-8 audit-section-9 d-flex align-items-center" id="audit-candidates-2">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-column justify-content-center align-items-center text-center">
						<?php if ( is_acf( $prefix . 'title-3' ) ) : ?>
							<?php the_acf( $prefix . 'title-3' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-3' ) ) : ?>
							<?php the_acf( $prefix . 'content-3' ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="slide-title">
				<?php if ( is_acf( $prefix . 'side-title-left' ) ) : ?>
					<div class="slide-title-top"><?php the_acf( $prefix . 'side-title-left' ); ?></div>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'side-title-right' ) ) : ?>
					<div class="slide-title-bottom"><?php the_acf( $prefix . 'side-title-right' ); ?></div>
				<?php endif; ?>
			</div>
		</section>
		<section class="audit-section-6 audit-section-10 d-flex align-items-center" id="audit-candidates-3">
			<div class="before mobile-hide"></div>
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<?php if ( is_acf( $prefix . 'title-4' ) ) : ?>
							<?php the_acf( $prefix . 'title-4' ); ?>
						<?php endif; ?>

						<div class="call-to-action">
							<div class="d-flex flex-row justify-content-center">
								<?php if ( is_acf( $prefix . 'button' ) && is_acf( $prefix . 'url' ) ) : ?>
									<a class="btn" href="<?php the_acf( $prefix . 'url' ); ?>"><?php the_acf( $prefix . 'button' ); ?></a>
								<?php endif; ?>

								<?php if ( is_acf( $prefix . 'button-2' ) && is_acf( $prefix . 'url-2' ) ) : ?>
									<a class="btn" href="<?php the_acf( $prefix . 'url-2' ); ?>"><?php the_acf( $prefix . 'button-2' ); ?></a>
								<?php endif; ?>
							</div>
							<?php if ( is_acf( $prefix . 'link' ) ) : ?>
								<p><a href="#" data-toggle="modal" data-target="#questions"><?php the_acf( $prefix . 'link' ); ?></a></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="after mobile-hide"></div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
