<?php
/**
 * Template part for displaying page content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

		<div class="container single-content">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		</div>

		<?php
	endwhile;

	the_posts_navigation();
else :
	?>

	<div class="container">
		<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
	</div>

	<?php
endif;
