<?php
/**
 * Clients Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'clients-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		<section class="clients">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'logos' ) ) : ?>
							<?php
							$logos = get_field( $prefix . 'logos' );
							$prev  = false;
							$count = 0;
							?>
							<div class="logos">
								<div class="row">
									<?php foreach ( $logos as $key => $logo ) : ?>
										<?php
										$row = ( 0 === $count % 3 && ! $prev ) ? '</div><div class=row>' : '';
										echo $row; // phpcs:ignore
										$small = ( is_acf_field( $logo[ $prefix . 'repeater-small' ] ) && $logo[ $prefix . 'repeater-small' ] ) ? $logo[ $prefix . 'repeater-small' ] : false;
										?>

										<?php if ( $small && ! $prev ) : ?>
											<div class="col space-between"><div class="row">
										<?php endif; ?>

										<?php if ( ! $small && $prev ) : ?>
											</div></div>
										<?php endif; ?>

										<div class="col">
											<?php if ( is_acf_field( $logo[ $prefix . 'repeater-image' ] ) ) : ?>
												<?php the_acf_field_image( $logo[ $prefix . 'repeater-image' ], 'medium', true ); ?>
											<?php endif; ?>
										</div>

										<?php
										$prev  = ( $small ) ? $small : false;
										$count = ( $prev ) ? $count - 1 : $count + 1;
										?>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
