<?php
/**
 * How Works Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'contact/thank-you' );
		?>
		<section class="thanks-section" id="thanks-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="thanks"><?php esc_html_e( 'thank you' ); ?></div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
