<?php
/**
 * About Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'about-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		<section class="about">
			<div class="about-circle mobile-hide">
				<?php the_svg( 'images/about.svg' ); ?>
			</div>
			<div class="container">
				<div class="row">
					<div class="col" id="about-1">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>
					</div>
					<div class="col" id="about-2">
						<?php if ( is_acf( $prefix . 'title-content' ) ) : ?>
							<?php the_acf( $prefix . 'title-content' ); ?>
						<?php endif; ?>
						<?php if ( is_acf( $prefix . 'content' ) ) : ?>
							<?php the_acf( $prefix . 'content' ); ?>
						<?php endif; ?>
					</div>
					<div class="col right-col" id="about-3">
						<?php if ( is_acf( $prefix . 'title-2' ) ) : ?>
							<?php the_acf( $prefix . 'title-2' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-2' ) ) : ?>
							<div class="first"><?php the_acf( $prefix . 'content-2' ); ?></div>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-large' ) ) : ?>
							<div class="large"><?php the_acf( $prefix . 'content-large' ); ?></div>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'button' ) && is_acf( $prefix . 'url' ) ) : ?>
							<div class="d-block"><a class="btn" href="<?php the_acf( $prefix . 'url' ); ?>"><?php the_acf( $prefix . 'button' ); ?></a></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
