<?php
/**
 * Home Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'home-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		<div class="container-fluid home-1 home d-flex flex-column align-items-center justify-content-center" id="home-1">
			<div class="row">
				<div class="col home d-flex flex-column align-items-center justify-content-center">
					<?php if ( is_acf( $prefix . 'logo' ) ) : ?>
						<div class="logo">
							<?php the_acf_image( $prefix . 'logo' ); ?>
						</div>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'title' ) ) : ?>
						<?php the_acf( $prefix . 'title' ); ?>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'content' ) ) : ?>
						<div class="large">
							<?php the_acf( $prefix . 'content' ); ?>
						</div>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'button' ) ) : ?>
						<a class="btn mobile-hide" id="more"><?php the_acf( $prefix . 'button' ); ?></a>
						<a class="btn mobile-show" href="#home-2"><?php the_acf( $prefix . 'button' ); ?></a>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'link' ) && is_acf( $prefix . 'url' ) ) : ?>
						<a class="small" href="<?php the_acf( $prefix . 'url' ); ?>">
							<?php the_acf( $prefix . 'link' ); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="container-fluid home-2 button-section d-flex flex-column align-items-center justify-content-center" id="home-2">
			<div class="row">
				<div class="col home d-flex flex-column justify-content-center">
					<?php if ( is_acf( $prefix . 'title-2' ) ) : ?>
						<?php the_acf( $prefix . 'title-2' ); ?>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'content-2' ) ) : ?>
						<?php the_acf( $prefix . 'content-2' ); ?>
					<?php endif; ?>

					<?php if ( is_acf( $prefix . 'button-1' ) && is_acf( $prefix . 'button-2' ) ) : ?>
						<div class="d-flex flex-row">
							<a class="btn mobile-hide" id="audit"><?php the_acf( $prefix . 'button-1' ); ?></a>
							<a class="btn mobile-hide" id="it-audit"><?php the_acf( $prefix . 'button-2' ); ?></a>
							<a class="btn mobile-show" href="<?php bloginfo( $prefix . 'url' ); ?>/audit"><?php the_acf( $prefix . 'button-1' ); ?></a>
							<a class="btn mobile-show" href="<?php bloginfo( $prefix . 'url' ); ?>/it-audit"><?php the_acf( $prefix . 'button-2' ); ?></a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
