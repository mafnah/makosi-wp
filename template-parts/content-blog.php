<?php
/**
 * Template part for displaying blog content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

$args      = array(
	'post_type'      => 'post',
	'posts_per_page' => -1,
);
$the_query = new WP_Query( $args );
$count     = 0;

if ( $the_query->have_posts() ) :
	?>
	<div class="slide swiper-slide">
		<div class="row first-row">

		<?php
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			?>

			<?php if ( 0 === $count % 4 && 0 !== $count ) : ?>
				</div></div><div class="slide swiper-slide"><div class="row first-row">
			<?php else : ?>
				<?php if ( 0 === $count % 2 && 0 !== $count ) : ?>
					</div><div class="row">
				<?php endif; ?>
			<?php endif; ?>

			<div class="col">
				<div class="blog-post">
					<div class="row">
						<div class="col post-content">
							<h2>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h2>
							<time datetime="<?php echo esc_html( get_the_date( 'Y-m-d H:i' ) ); ?>"><?php echo esc_html( get_the_date( 'd M Y' ) ); ?></time>
							<?php the_excerpt(); ?>
						</div>
						<div class="col">
							<a href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail() ) : ?>
									<?php the_post_thumbnail( 'medium' ); ?>
								<?php else : ?>
									<img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-1@2x.jpg" alt="Blog">
								<?php endif; ?>
							</a>
						</div>
					</div>
				</div>
			</div>

			<?php
			$count++;
			endwhile;
			wp_reset_postdata();
		?>

		</div>
	</div>
<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php
endif;
