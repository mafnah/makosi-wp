<?php
/**
 * How Works Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'how-our-program-works' );
		?>
		<section class="how-works-section" id="how-works-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
