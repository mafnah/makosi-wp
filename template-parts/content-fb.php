<?php
/**
 * Template part for displaying facebook content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

$fb_feed = json_decode( $fb_feed );

?>
<div class="slide swiper-slide">
	<div class="row first-row">

	<?php
	foreach ( $fb_feed->data as $key => $fb_post ) :
		$post_title          = ( property_exists( $fb_post, 'attachments' ) && $fb_post->attachments->data && $fb_post->attachments->data[0] && $fb_post->attachments->data[0]->title ) ? $fb_post->attachments->data[0]->title : $fb_post->message;
		$url                 = ( property_exists( $fb_post, 'permalink_url' ) ) ? $fb_post->permalink_url : '#';
		$story               = ( property_exists( $fb_post, 'story' ) ) ? $fb_post->story : '';
		$content             = ( property_exists( $fb_post, 'message' ) ) ? $fb_post->message : $story;
		$published_date      = gmdate( 'd M Y', strtotime( $fb_post->created_time ) );
		$published_date_time = gmdate( 'Y-m-d H:i', strtotime( $fb_post->created_time ) );
		$image               = ( property_exists( $fb_post, 'full_picture' ) ) ? $fb_post->full_picture : '';
		?>

		<?php if ( 0 === $key % 4 && 0 !== $key ) : ?>
			</div></div><div class="slide swiper-slide"><div class="row first-row">
		<?php else : ?>
			<?php if ( 0 === $key % 2 && 0 !== $key ) : ?>
				</div><div class="row">
			<?php endif; ?>
		<?php endif; ?>

		<div class="col">
			<div class="blog-post">
				<div class="row">
					<div class="col post-content">
						<h2>
							<a href="<?php echo esc_html( $url ); ?>"><?php echo esc_html( $post_title ); ?></a>
						</h2>
						<time datetime="<?php echo esc_html( $published_date_time ); ?>"><?php echo esc_html( $published_date ); ?></time>
						<p><?php echo esc_html( $content ); ?></p>
					</div>
					<div class="col">
						<a href="<?php echo esc_html( $url ); ?>">
							<?php if ( $image ) : ?>
								<img src="<?php echo esc_html( $image ); ?>" alt="<?php echo esc_html( $post_title ); ?>">
							<?php else : ?>
								<img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-1@2x.jpg" alt="Blog">
							<?php endif; ?>
						</a>
					</div>
				</div>
			</div>
		</div>

		<?php
		endforeach;
	?>

	</div>
</div>
