<?php
/**
 * Template part for displaying posts content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

		<article>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php the_excerpt(); ?>
		</article>

		<?php
	endwhile;

	the_posts_navigation();
else :
	?>

	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php
endif;
