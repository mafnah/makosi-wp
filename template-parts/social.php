<?php
/**
 * Social Bar
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'options-';

?>
<div class="social-bar">
	<div class="follow">
		<?php if ( is_acf( $prefix . 'instagram', 'option' ) ) : ?>
			<a href="<?php the_acf( $prefix . 'instagram', 'option' ); ?>" target="_blank">
				<?php the_svg( 'images/instagram.svg' ); ?>
			</a>
		<?php endif; ?>
	</div>

	<?php if ( is_acf( $prefix . 'instagram', 'option' ) ) : ?>
		<div class="insta" data-feed="<?php the_acf( $prefix . 'instagram', 'option' ); ?>">
			<p>Loading...</p>
		</div>
	<?php endif; ?>
</div>
