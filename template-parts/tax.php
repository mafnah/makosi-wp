<?php
/**
 * Tax Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'tax-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'tax' );
		?>
		<section class="tax-section" id="tax-1">
			<div class="container">
				<div class="row">
					<div class="col button-section d-flex justify-content-start align-items-start flex-column">
						<div class="tax-wrap align-self-end">
							<?php if ( is_acf( $prefix . 'title' ) ) : ?>
								<?php the_acf( $prefix . 'title' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'content' ) ) : ?>
								<?php the_acf( $prefix . 'content' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'button-1' ) && is_acf( $prefix . 'button-2' ) ) : ?>
								<div class="d-flex flex-row">
									<a class="btn mobile-hide" id="tax-clients"><?php the_acf( $prefix . 'button-1' ); ?></a>
									<a class="btn mobile-hide" id="tax-candidates"><?php the_acf( $prefix . 'button-2' ); ?></a>
									<a class="btn mobile-show" href="<?php bloginfo( 'url' ); ?>/tax/clients"><?php the_acf( $prefix . 'button-1' ); ?></a>
									<a class="btn mobile-show" href="<?php bloginfo( 'url' ); ?>/tax/applicants"><?php the_acf( $prefix . 'button-2' ); ?></a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>

	<div class="mobile-hide">
		<?php get_template_part( 'template-parts/tax', 'clients' ); ?>
		<?php get_template_part( 'template-parts/tax', 'candidates' ); ?>

		<?php if ( ! is_front_page() ) : ?>
			<?php get_template_part( 'template-parts/new-blog' ); ?>
		<?php endif ?>
	</div>
