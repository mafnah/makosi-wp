<?php
/**
 * Contact Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'contact-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		<section class="contact">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<div class="row">
							<div class="col">
								<?php if ( is_acf( $prefix . 'form' ) ) : ?>
									<?php the_field( $prefix . 'form' ); ?>
								<?php endif; ?>

								<?php if ( is_acf( $prefix . 'content' ) ) : ?>
									<div class="form-content">
										<?php the_acf( $prefix . 'content' ); ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
