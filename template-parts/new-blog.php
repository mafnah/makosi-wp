<?php
/**
 * New Blog Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'new-blog-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'blog' );
		?>
		<section class="new-blog">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<section class="blog-single">
							<div class="blog-title">
								<?php if ( is_acf( $prefix . 'title' ) ) : ?>
									<?php the_acf( $prefix . 'title' ); ?>
								<?php endif; ?>

								<?php if ( is_acf( $prefix . 'content' ) ) : ?>
									<?php the_acf( $prefix . 'content' ); ?>
								<?php endif; ?>
							</div>

							<?php get_template_part( 'template-parts/content', 'new-blog' ); ?>
						</section>
					</div>

					<div class="col-md-3">
						<?php get_template_part( 'sidebar' ); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
