<?php
/**
 * Template part for displaying new blog content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

$args      = array(
	'post_type'      => 'post',
	'posts_per_page' => -1,
);
$the_query = new WP_Query( $args );
$count     = 0;

if ( $the_query->have_posts() ) :
	?>
	<div class="row">

		<?php
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			$featured   = ( ! $count ) ? 'featured-post' : '';
			$image_size = ( $featured ) ? 'large' : 'medium';
			$cols       = ( ! $count ) ? 'col' : 'col-sm-6';
			?>

			<?php if ( 0 === ( $count + 1 ) % 2 && 0 !== ( $count + 1 ) ) : ?>
				</div><div class="row">
			<?php endif; ?>

				<div class="<?php echo esc_html( $cols ); ?>">
					<article class="blog-post <?php echo esc_html( $featured ); ?>">
						<a href="<?php the_permalink(); ?>" class="thumb">
							<span class="thumb-wrap">
								<?php if ( has_post_thumbnail() ) : ?>
									<?php the_post_thumbnail( $image_size ); ?>
								<?php else : ?>
									<img src="<?php bloginfo( 'template_directory' ); ?>/images/blog-single-1@2x.jpg" alt="Blog">
								<?php endif; ?>
							</span>
						</a>

						<div class="categories">
							<?php the_category( ' ' ); ?>
						</div>

						<h2>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>

						<div class="meta">
							<a href="<?php echo esc_html( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="author">
								<?php the_author(); ?>
							</a>

							<span>|</span>

							<time datetime="<?php echo esc_html( get_the_date( 'Y-m-d H:i' ) ); ?>" class="date">
								<?php echo esc_html( get_the_date() ); ?>
							</time>
						</div>

						<?php the_excerpt(); ?>

						<a href="<?php the_permalink(); ?>" class="btn btn-sm">
							<?php esc_html_e( 'read more' ); ?>
						</a>
					</article>
				</div>

			<?php
			$count++;
			endwhile;
			wp_reset_postdata();
		?>

	</div>
<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php
endif;
