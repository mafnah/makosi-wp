<?php
/**
 * Blog Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'options-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'blog' );
		?>
		<section class="main-blog">
			<?php if ( is_acf( $prefix . 'blog-title', 'option' ) ) : ?>
				<div class="side-title"><?php the_acf( $prefix . 'blog-title', 'option' ); ?></div>
			<?php endif; ?>

			<div class="container-fluid">
				<div class="swiper-container-wrap">
					<div class="slider blog-slider swiper-container">
						<div class="swiper-wrapper">
							<?php
							$fb_feed    = false;
							$fb_page_id = ( get_option( 'cff_page_id' ) ) ? get_option( 'cff_page_id' ) : 158233690863553;
							$fb_token   = ( get_option( 'cff_access_token' ) ) ? get_option( 'cff_access_token' ) : 'EAAS8LGISx9wBAGnSMVDw4OdG2C1TqBPO9cEZBTfUpCzwN8B5hZAuoHWyz3gtnUAhYtxCccxkk6xGZCIx6D5WmyRg5O2LCZBdDIGVXaEqGn5zWlOZBrG0MuJiFfBHquLpL7lofhZATwUl9eaKjJv3mF4U5oH3NXf72UmHZAONMN802xTgzsUZA1I9QllbZAkHVKJwZD';

							$response = wp_remote_get( 'https://graph.facebook.com/v4.0/' . $fb_page_id . '/posts?fields=id,from{picture,id,name,link},full_picture,permalink_url,message,message_tags,story,story_tags,status_type,created_time,backdated_time,call_to_action,attachments{title,description,media_type,unshimmed_url,target{id},media{source}}&access_token=' . $fb_token . '&limit=100&locale=en_US' );

							if ( is_array( $response ) && ! is_wp_error( $response ) ) {
								$headers = $response['headers'];
								$fb_feed = $response['body'];
							}

							if ( $fb_feed ) {
								include locate_template( 'template-parts/content-fb.php', false, false );
							} else {
								get_template_part( 'template-parts/content', 'blog' );
							}
							?>
						</div>
					</div>
					<div class="slider-nav-wrap">
						<div class="slider-nav prev"></div>
						<div class="slider-nav next"></div>
					</div>
				</div>

				<?php get_template_part( 'template-parts/social' ); ?>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
