<?php
/**
 * Tax Candidates Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'tax-candidates-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'tax/applicants' );
		?>
		<section class="tax-clients tax-candidates" id="tax-candidates-1">
			<div class="container d-flex justify-content-start">
				<div class="row align-items-center">
					<div class="col">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content' ) ) : ?>
							<?php the_acf( $prefix . 'content' ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		<section class="tax-love" id="tax-candidates-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-6">
						<?php if ( is_acf( $prefix . 'subtitle' ) || is_acf( $prefix . 'subtitle-2' ) ) : ?>
							<h1>
								<span id="love"><?php the_acf( $prefix . 'subtitle' ); ?></span>
								<strong id="tax"><?php the_acf( $prefix . 'subtitle-2' ); ?><span class="before"></span></strong>
							</h1>
						<?php endif; ?>
					</div>
					<div class="col-6">
						<div class="content-wrap">
							<?php if ( is_acf( $prefix . 'content-large' ) ) : ?>
								<div class="larger"><?php the_acf( $prefix . 'content-large' ); ?></div>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'content-2' ) ) : ?>
								<?php the_acf( $prefix . 'content-2' ); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="tax-how text-center" id="tax-candidates-3">
			<div class="container-fluid">
				<?php if ( is_acf( $prefix . 'title-2' ) ) : ?>
					<?php the_acf( $prefix . 'title-2' ); ?>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'icons' ) ) : ?>
					<?php $icons = get_field( $prefix . 'icons' ); ?>
					<div class="row icon-cols">
						<?php foreach ( $icons as $key => $icon ) : ?>
							<div class="col-3 icon-col" id="col-<?php echo esc_html( $key ); ?>">
								<?php $mid_arrow = ( 1 === $key ) ? 'id=mid-arrow' : ''; ?>
								<?php if ( is_acf_field( $icon[ $prefix . 'repeater-image' ] ) ) : ?>
									<?php the_acf_field_image( $icon[ $prefix . 'repeater-image' ] ); ?>
								<?php endif; ?>

								<?php if ( is_acf_field( $icon[ $prefix . 'repeater-title' ] ) ) : ?>
									<?php the_acf_field( $icon[ $prefix . 'repeater-title' ] ); ?>
								<?php endif; ?>

								<?php if ( is_acf_field( $icon[ $prefix . 'repeater-content' ] ) ) : ?>
									<?php the_acf_field( $icon[ $prefix . 'repeater-content' ] ); ?>
								<?php endif; ?>

								<div class="arrow" <?php echo esc_html( $mid_arrow ); ?>>
									<?php the_svg( 'images/arrow.svg' ); ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</section>
		<!-- <section class="tax-revolution audit-section-6 audit-section-10 d-flex align-items-center" id="tax-candidates-4">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<?php if ( is_acf( $prefix . 'title-3' ) ) : ?>
							<?php the_acf( $prefix . 'title-3' ); ?>
						<?php endif; ?>

						<div class="call-to-action">
							<div class="d-flex flex-row justify-content-center">
								<?php if ( is_acf( $prefix . 'button' ) && is_acf( $prefix . 'url' ) ) : ?>
									<a class="btn" href="<?php the_acf( $prefix . 'url' ); ?>"><?php the_acf( $prefix . 'button' ); ?></a>
								<?php endif; ?>

								<?php if ( is_acf( $prefix . 'button-2' ) && is_acf( $prefix . 'url-2' ) ) : ?>
									<a class="btn" href="<?php the_acf( $prefix . 'url-2' ); ?>" target="_blank"><?php the_acf( $prefix . 'button-2' ); ?></a>
								<?php endif; ?>
							</div>
							<?php if ( is_acf( $prefix . 'link' ) ) : ?>
								<p><a href="#" data-toggle="modal" data-target="#questions"><?php the_acf( $prefix . 'link' ); ?></a></p>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
