<?php
/**
 * Apply Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'apply-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'apply' );
		?>
		<section class="application" id="application-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content' ) ) : ?>
							<?php the_acf( $prefix . 'content' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'button' ) && is_acf( $prefix . 'url' ) ) : ?>
							<a class="btn btn-light" href="<?php the_acf( $prefix . 'url' ); ?>" target="_blank"><?php the_acf( $prefix . 'button' ); ?></a>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-small' ) ) : ?>
							<div class="p-small"><?php the_acf( $prefix . 'content-small' ); ?></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
