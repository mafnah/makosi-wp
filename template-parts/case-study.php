<?php
/**
 * Case Study Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'case-study-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'case-study' );
		?>
		<section class="audit-testimonial d-flex align-items-center text-center" id="case-study-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_acf( $prefix . 'testimonial' ) ) : ?>
							<?php the_acf( $prefix . 'testimonial' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'button' ) ) : ?>
							<a class="btn btn-light" href="#" data-toggle="modal" data-target="#case-studies"><?php the_acf( $prefix . 'button' ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
