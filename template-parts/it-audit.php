<?php
/**
 * IT Audit Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'it-audit-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'it-audit' );
		?>
		<section class="audit-section it-audit-section  d-flex align-items-end" id="it-audit-1">
			<div class="container">
				<div class="row">
					<div class="col button-section">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content' ) ) : ?>
							<?php the_acf( $prefix . 'content' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'button-1' ) && is_acf( $prefix . 'button-2' ) ) : ?>
							<div class="d-flex flex-row">
								<a class="btn mobile-hide" id="it-audit-clients"><?php the_acf( $prefix . 'button-1' ); ?></a>
								<a class="btn mobile-hide" id="it-audit-candidates"><?php the_acf( $prefix . 'button-2' ); ?></a>
								<a class="btn mobile-show" href="<?php bloginfo( 'url' ); ?>/it-audit/clients"><?php the_acf( $prefix . 'button-1' ); ?></a>
								<a class="btn mobile-show" href="<?php bloginfo( 'url' ); ?>/it-audit/applicants"><?php the_acf( $prefix . 'button-2' ); ?></a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>

	<div class="mobile-hide">
		<?php get_template_part( 'template-parts/it-audit', 'clients' ); ?>
		<?php get_template_part( 'template-parts/it-audit', 'candidates' ); ?>
		<?php get_template_part( 'template-parts/new-blog' ); ?>
	</div>
