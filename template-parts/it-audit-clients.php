<?php
/**
 * IT Audit Clients Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'it-audit-clients-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		set_post( 'it-audit/clients' );
		?>
		<section class="clients-section text-center" id="it-audit-clients-1">
			<div class="container">
				<div class="row">
					<div class="col button-section d-flex flex-column justify-content-center welcome-content">
						<?php if ( is_acf( $prefix . 'title' ) ) : ?>
							<?php the_acf( $prefix . 'title' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content' ) ) : ?>
							<?php the_acf( $prefix . 'content' ); ?>
						<?php endif; ?>
					</div>
					<div class="col why-section text-left d-flex flex-column justify-content-center align-items-center">
						<div class="why-content">
							<?php if ( is_acf( $prefix . 'title-2' ) ) : ?>
								<?php the_acf( $prefix . 'title-2' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'facts' ) ) : ?>
								<?php $facts = get_field( $prefix . 'facts' ); ?>
								<?php foreach ( $facts as $key => $fact ) : ?>
									<div class="audit-fact fact-<?php echo esc_html( $key ); ?>">
										<?php if ( is_acf_field( $fact[ $prefix . 'repeater-title' ] ) ) : ?>
											<?php the_acf_field( $fact[ $prefix . 'repeater-title' ] ); ?>
										<?php endif; ?>

										<?php if ( is_acf_field( $fact[ $prefix . 'repeater-content' ] ) ) : ?>
											<?php the_acf_field( $fact[ $prefix . 'repeater-content' ] ); ?>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="audit-section-4 d-flex align-items-center" id="it-audit-clients-2">
			<?php if ( is_acf( $prefix . 'side-title-left' ) ) : ?>
				<div class="side-title side-title-left mobile-hide"><?php the_acf( $prefix . 'side-title-left' ); ?></div>
			<?php endif; ?>

			<?php if ( is_acf( $prefix . 'side-title-right' ) ) : ?>
				<div class="side-title side-title-right mobile-hide"><?php the_acf( $prefix . 'side-title-right' ); ?></div>
			<?php endif; ?>

			<div class="container">
				<div class="row">
					<div class="col d-flex flex-column justify-content-center align-items-center">
						<?php if ( is_acf( $prefix . 'title-3' ) ) : ?>
							<?php the_acf( $prefix . 'title-3' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'facts-2' ) ) : ?>
							<div class="row audit-facts">
								<?php $facts_2 = get_field( $prefix . 'facts-2' ); ?>
								<?php foreach ( $facts_2 as $key => $fact_2 ) : ?>
									<div class="col-3 fact-<?php echo esc_html( $key ); ?>">
										<?php if ( is_acf_field( $fact_2[ $prefix . 'repeater-title-2' ] ) ) : ?>
											<?php the_acf_field( $fact_2[ $prefix . 'repeater-title-2' ] ); ?>
										<?php endif; ?>

										<?php if ( is_acf_field( $fact_2[ $prefix . 'repeater-content-2' ] ) ) : ?>
											<?php the_acf_field( $fact_2[ $prefix . 'repeater-content-2' ] ); ?>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		<section class="audit-testimonial d-flex align-items-center text-center" id="it-audit-clients-3">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_acf( $prefix . 'testimonial' ) ) : ?>
							<?php the_acf( $prefix . 'testimonial' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'title-4' ) ) : ?>
							<?php the_acf( $prefix . 'title-4' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'button' ) ) : ?>
							<a class="btn btn-light" href="#" data-toggle="modal" data-target="#case-studies"><?php the_acf( $prefix . 'button' ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		<section class="audit-team d-flex align-items-center" id="it-audit-clients-4">
			<div class="lines-single mobile-hide">
				<?php the_svg( 'images/lines-single.svg' ); ?>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-5">
						<?php if ( is_acf( $prefix . 'sub-title' ) || is_acf( $prefix . 'sub-title-2' ) || is_acf( $prefix . 'sub-title-3' ) ) : ?>
							<h1>
								<?php if ( is_acf( $prefix . 'sub-title' ) ) : ?>
									<span class="first-title"><?php the_acf( $prefix . 'sub-title' ); ?></span><br>
								<?php endif; ?>
								<?php if ( is_acf( $prefix . 'sub-title-2' ) ) : ?>
									<strong><span class="second-title"><?php the_acf( $prefix . 'sub-title-2' ); ?></span><br>
								<?php endif; ?>
								<?php if ( is_acf( $prefix . 'sub-title-3' ) ) : ?>
									<span class="third-title"><?php the_acf( $prefix . 'sub-title-3' ); ?></span></strong>
								<?php endif; ?>
							</h1>
						<?php endif; ?>
					</div>
					<div class="col-7">
						<?php if ( is_acf( $prefix . 'content-2' ) ) : ?>
							<?php the_acf( $prefix . 'content-2' ); ?>
						<?php endif; ?>

						<div class="industries">
							<?php if ( is_acf( $prefix . 'title-5' ) ) : ?>
								<?php the_acf( $prefix . 'title-5' ); ?>
							<?php endif; ?>

							<div class="row">
								<?php if ( is_acf( $prefix . 'list-left' ) ) : ?>
									<div class="col-6">
										<?php the_acf( $prefix . 'list-left' ); ?>
									</div>
								<?php endif; ?>
								<?php if ( is_acf( $prefix . 'list-right' ) ) : ?>
									<div class="col-6">
										<?php the_acf( $prefix . 'list-right' ); ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="audit-section-6 d-flex align-items-center" id="it-audit-clients-5">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<?php if ( is_acf( $prefix . 'title-6' ) ) : ?>
							<?php the_acf( $prefix . 'title-6' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-3' ) ) : ?>
							<?php the_acf( $prefix . 'content-3' ); ?>
						<?php endif; ?>

						<?php // if ( is_acf( $prefix . 'button-2' ) && is_acf( $prefix . 'url' ) ) : ?>
							<!-- <a class="btn" href="<?php the_acf( $prefix . 'url' ); ?>"><?php the_acf( $prefix . 'button-2' ); ?></a> -->
						<?php // endif; ?>

						<?php if ( is_acf( $prefix . 'button-2' ) ) : ?>
							<a class="btn" href="#" data-toggle="modal" data-target="#request-case-studies"><?php the_acf( $prefix . 'button-2' ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
