<?php
/**
 * Webinar Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'webinar-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		<section class="webinar" id="webinar-main">
			<div class="container">
				<div class="row">
					<div class="col wow-wrap" id="webinar-1">
						<div class="wow">
							<?php if ( is_acf( $prefix . 'title' ) ) : ?>
								<?php the_acf( $prefix . 'title' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'content' ) ) : ?>
								<?php the_acf( $prefix . 'content' ); ?>
							<?php endif; ?>
						</div>
					</div>
					<div class="col join-wrap" id="webinar-2">
						<div class="join">
							<?php if ( is_acf( $prefix . 'title-2' ) ) : ?>
								<?php the_acf( $prefix . 'title-2' ); ?>
							<?php endif; ?>

							<?php if ( is_acf( $prefix . 'content-2' ) ) : ?>
								<?php the_acf( $prefix . 'content-2' ); ?>
							<?php endif; ?>

							<div class="webinar-wrap">
								<div class="webinar-content" id="webinar-3">
									<?php if ( is_acf( $prefix . 'content-first' ) ) : ?>
										<div class="p-large p-first"><?php the_acf( $prefix . 'content-first' ); ?></div>
									<?php endif; ?>

									<?php if ( is_acf( $prefix . 'content-large' ) ) : ?>
										<div class="p-large"><?php the_acf( $prefix . 'content-large' ); ?></div>
									<?php endif; ?>

									<?php if ( is_acf( $prefix . 'content-last' ) ) : ?>
										<div class="p-last"><?php the_acf( $prefix . 'content-last' ); ?></div>
									<?php endif; ?>
								</div>

								<?php if ( is_acf( $prefix . 'feed' ) ) : ?>
									<div class="webinar-table" id="webinar-4" data-feed="<?php the_acf( $prefix . 'feed' ); ?>">
										<table>
											<tr>
												<td><strong>Loading...</strong></td>
											</tr>
										</table>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- <section class="application" id="application-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_acf( $prefix . 'title-3' ) ) : ?>
							<?php the_acf( $prefix . 'title-3' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-3' ) ) : ?>
							<?php the_acf( $prefix . 'content-3' ); ?>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'button' ) && is_acf( $prefix . 'url' ) ) : ?>
							<a class="btn btn-light" href="<?php the_acf( $prefix . 'url' ); ?>" target="_blank"><?php the_acf( $prefix . 'button' ); ?></a>
						<?php endif; ?>

						<?php if ( is_acf( $prefix . 'content-small' ) ) : ?>
							<div class="p-small"><?php the_acf( $prefix . 'content-small' ); ?></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section> -->
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
