<?php
/**
 * Team Partial
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'team-';

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		<div class="container">
			<?php if ( is_acf( $prefix . 'title-1' ) ) : ?>
				<?php the_acf( $prefix . 'title-1' ); ?>
			<?php endif; ?>

			<?php if ( is_acf( $prefix . 'members' ) ) : ?>
				<?php $members = get_field( $prefix . 'members' ); ?>
				<div class="row">
					<?php foreach ( $members as $key => $member ) : ?>
						<?php
						$row = ( 0 === $key % 3 ) ? '</div><div class=row>' : '';
						echo $row; // phpcs:ignore
						?>
						<div class="col-md-4">
							<div class="team">
								<?php if ( is_acf_field( $member[ $prefix . 'repeater-image' ] ) ) : ?>
									<?php the_acf_field_image( $member[ $prefix . 'repeater-image' ], 'large' ); ?>
								<?php else : ?>
									<img src="<?php bloginfo( 'template_directory' ); ?>/images/team-placeholder-gray.png" alt="Team">
								<?php endif; ?>

								<div class="team-overlay">
									<?php if ( is_acf_field( $member[ $prefix . 'repeater-title' ] ) ) : ?>
										<h2><?php the_acf_field( $member[ $prefix . 'repeater-title' ] ); ?></h2>
									<?php endif; ?>

									<?php if ( is_acf_field( $member[ $prefix . 'repeater-position' ] ) ) : ?>
										<span class="position"><?php the_acf_field( $member[ $prefix . 'repeater-position' ] ); ?></span>
									<?php endif; ?>

									<?php if ( is_acf_field( $member[ $prefix . 'repeater-content' ] ) ) : ?>
										<?php the_acf_field( $member[ $prefix . 'repeater-content' ] ); ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	<?php endwhile; else : ?>
		<div class="container">
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		</div>
	<?php endif; ?>
