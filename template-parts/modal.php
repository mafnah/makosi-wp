<?php
/**
 * Modal
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Makosi
 */

$prefix = 'options-';

?>
<div class="modal fade" id="questions" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<?php if ( is_acf( $prefix . 'title-2', 'option' ) ) : ?>
					<h5 class="modal-title"><?php the_acf( $prefix . 'title-2', 'option' ); ?></h5>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'questions', 'option' ) ) : ?>
					<?php $questions = get_field( $prefix . 'questions', 'option' ); ?>
					<div id="accordion">
						<?php foreach ( $questions as $key => $question ) : ?>
							<?php
							$expanded = ( ! $key ) ? 'aria-expanded=true' : '';
							$show     = ( ! $key ) ? 'show' : '';
							?>

							<div class="toggle">
								<div class="toggle-header" id="heading-<?php echo esc_html( $key ); ?>">
									<button data-toggle="collapse" data-target="#collapse-<?php echo esc_html( $key ); ?>"  aria-controls="collapse-<?php echo esc_html( $key ); ?>" <?php echo esc_html( $expanded ); ?>>
										<?php if ( is_acf_field( $question[ $prefix . 'repeater-title' ] ) ) : ?>
											<?php the_acf_field( $question[ $prefix . 'repeater-title' ] ); ?>
										<?php endif; ?>
									</button>
								</div>
								<div class="collapse <?php echo esc_html( $show ); ?>" id="collapse-<?php echo esc_html( $key ); ?>" aria-labelledby="heading-<?php echo esc_html( $key ); ?>" data-parent="#accordion">
									<div class="toggle-body">
										<?php if ( is_acf_field( $question[ $prefix . 'repeater-content' ] ) ) : ?>
											<?php the_acf_field( $question[ $prefix . 'repeater-content' ] ); ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="case-studies" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<?php if ( is_acf( $prefix . 'title-1', 'option' ) ) : ?>
					<h5 class="modal-title"><?php the_acf( $prefix . 'title-1', 'option' ); ?></h5>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'content', 'option' ) ) : ?>
					<?php the_acf( $prefix . 'content', 'option' ); ?>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'form', 'option' ) ) : ?>
					<?php the_field( $prefix . 'form', 'option' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="request-case-studies" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<?php if ( is_acf( $prefix . 'title-3', 'option' ) ) : ?>
					<h5 class="modal-title"><?php the_acf( $prefix . 'title-3', 'option' ); ?></h5>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'content-2', 'option' ) ) : ?>
					<?php the_acf( $prefix . 'content-2', 'option' ); ?>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'form-2', 'option' ) ) : ?>
					<?php the_field( $prefix . 'form-2', 'option' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="covid-update" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal"><span>&times;</span></button>
			</div>
			<div class="modal-body">
				<?php if ( is_acf( $prefix . 'covid-title', 'option' ) ) : ?>
					<h5 class="modal-title"><?php the_acf( $prefix . 'covid-title', 'option' ); ?></h5>
				<?php endif; ?>

				<?php if ( is_acf( $prefix . 'covid-content', 'option' ) ) : ?>
					<?php the_acf( $prefix . 'covid-content', 'option' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
