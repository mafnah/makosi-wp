<?php
/**
 * The IT audit candidates template file
 *
 * Template Name: IT Audit Candidates
 *
 * @package Makosi
 */

get_header();
?>

<main class="main it-audit-candidates-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/it-audit', 'candidates' ); ?>
</main>

<?php
get_footer();
