<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Makosi
 */

$prefix = 'single-post-';

get_header();
?>

<main class="main main-single">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<section class="blog-single">
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) :
							the_post();
							?>

							<div class="blog-header">
								<div class="blog-title">
									<?php if ( is_acf( 'options-blog-title', 'option' ) ) : ?>
										<a href="<?php bloginfo( 'url' ); ?>/blog/">
											<?php the_acf( 'options-blog-title', 'option' ); ?>
										</a>
									<?php endif; ?>
								</div>

								<div class="blog-navigation">
									<?php previous_post_link( '%link', '<span class="prev nav-arrow"></span> Prev' ); ?>
									<span class="separator"></span>
									<?php next_post_link( '%link', 'Next <span class="next nav-arrow"></span>' ); ?>
								</div>
							</div>

							<?php if ( has_post_thumbnail() ) : ?>
								<div class="featured">
									<?php the_post_thumbnail( 'large' ); ?>
								</div>
							<?php endif; ?>

							<div class="single-content">
								<div class="categories">
									<?php the_category( ' ' ); ?>
								</div>

								<h1 class="single-title"><?php the_title(); ?></h1>

								<div class="meta">
									<a href="<?php echo esc_html( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="author">
										<?php the_author(); ?>
									</a>

									<span>|</span>

									<time datetime="<?php echo esc_html( get_the_date( 'Y-m-d H:i' ) ); ?>" class="date">
										<?php echo esc_html( get_the_date() ); ?>
									</time>
								</div>

								<?php the_content(); ?>

								<div class="share">
									<h3>
										<?php esc_html_e( 'SHARE THIS POST' ); ?>
									</h3>

									<ul class="list-inline list-unstyled">
										<li class="list-inline-item">
											<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li class="list-inline-item">
											<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
										<?php if ( has_post_thumbnail() ) : ?>
											<li class="list-inline-item">
												<a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php the_post_thumbnail_url( 'large' ); ?>&description=<?php the_title(); ?>">
													<i class="fa fa-pinterest"></i>
												</a>
											</li>
										<?php endif; ?>
									</ul>
								</div>

								<div class="related">
									<h3>
										<?php esc_html_e( 'RELATED CONTENT' ); ?>
									</h3>

									<div class="row">
										<?php
										$args = array(
											'post_type' => 'post',
											'posts_per_page' => 3,
										);

										if ( is_acf( $prefix . 'related' ) ) {
											$args['post__in'] = get_field( $prefix . 'related' );
										}

										$related_query = new WP_Query( $args );

										if ( $related_query->have_posts() ) :
											while ( $related_query->have_posts() ) :
												$related_query->the_post();
												?>
													<div class="col-md-4">
														<article class="blog-post related-post">
															<?php if ( has_post_thumbnail() ) : ?>
																<div class="thumb">
																	<a href="<?php the_permalink(); ?>">
																		<?php the_post_thumbnail( 'medium' ); ?>
																	</a>
																</div>
															<?php endif; ?>

															<div class="categories">
																<?php the_category( ' ' ); ?>
															</div>

															<h4>
																<a href="<?php the_permalink(); ?>">
																	<?php the_title(); ?>
																</a>
															</h4>

															<div class="meta">
																<a href="<?php echo esc_html( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="author">
																	<?php the_author(); ?>
																</a>

																<span>|</span>

																<time datetime="<?php echo esc_html( get_the_date( 'Y-m-d H:i' ) ); ?>" class="date">
																	<?php echo esc_html( get_the_date() ); ?>
																</time>
															</div>

															<?php the_excerpt(); ?>
														</article>
													</div>
												<?php
											endwhile;
											wp_reset_postdata();
										else :
											?>
												<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
											<?php
										endif;
										?>
									</div>
								</div>
							</div>
						<?php endwhile; else : ?>
							<div class="container">
								<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
							</div>
						<?php endif; ?>
				</section>
			</div>

			<div class="col-md-3">
				<?php get_template_part( 'sidebar' ); ?>
			</div>
		</div>
	</div>
</main>

<?php
get_footer();
