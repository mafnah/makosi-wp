<?php
/**
 * The apply template file
 *
 * Template Name: Apply
 *
 * @package Makosi
 */

get_header();
?>

<main class="main webinar-main apply-main">
	<?php get_template_part( 'template-parts/apply' ); ?>
</main>

<?php
get_footer();
