<?php
/**
 * The IT audit template file
 *
 * Template Name: IT Audit
 *
 * @package Makosi
 */

get_header();
?>

<main class="main it-audit-main">
	<?php get_template_part( 'template-parts/lines' ); ?>
	<?php get_template_part( 'template-parts/it-audit' ); ?>
</main>

<?php
get_footer();
