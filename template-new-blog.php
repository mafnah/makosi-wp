<?php
/**
 * The new blog template file
 *
 * Template Name: New Blog
 *
 * @package Makosi
 */

get_header();
?>

<main class="main main-new-blog">
	<?php get_template_part( 'template-parts/new-blog' ); ?>
</main>

<?php
get_footer();
