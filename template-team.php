<?php
/**
 * The team template file
 *
 * Template Name: Team
 *
 * @package Makosi
 */

get_header();
?>

<main class="main team-main">
	<?php get_template_part( 'template-parts/team' ); ?>
</main>

<?php
get_footer();
