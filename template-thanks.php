<?php
/**
 * The thank you template file
 *
 * Template Name: Thank you
 *
 * @package Makosi
 */

get_header();
?>

<main class="main thanks-main">
	<?php get_template_part( 'template-parts/thanks' ); ?>
</main>

<?php
get_footer();
