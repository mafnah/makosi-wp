<?php
/**
 * Template part for displaying the sidebar
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Makosi
 */

$prefix = 'options-';

?>

<aside class="sidebar">
	<div class="must-reads">
		<h3>
			<?php esc_html_e( 'MUST READS' ); ?>
		</h3>

		<?php
		$args      = array(
			'post_type'      => 'post',
			'posts_per_page' => 4,
		);
		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) :
				$the_query->the_post();
				?>

					<div class="must-read-post">
						<h4>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h4>

						<div class="meta">
							<?php the_category( ' ' ); ?>

							<span>|</span>

							<time datetime="<?php echo esc_html( get_the_date( 'Y-m-d H:i' ) ); ?>" class="date">
								<?php echo esc_html( get_the_date() ); ?>
							</time>
						</div>
					</div>

				<?php
			endwhile;
			wp_reset_postdata();
		else :
			?>
				<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php
		endif;
		?>
	</div>

	<?php echo get_search_form(); ?>

	<?php get_template_part( 'template-parts/social' ); ?>

	<?php if ( is_acf( $prefix . 'sign-up-form', 'option' ) ) : ?>
		<div class="sign-up">
			<h3>
				<?php esc_html_e( 'SIGN UP to receive updates' ); ?>
			</h3>

			<?php the_field( $prefix . 'sign-up-form', 'option' ); ?>
		</div>
	<?php endif; ?>


	<div class="follow-us">
		<h3>
			<?php esc_html_e( 'FOLLOW US' ); ?>
		</h3>

		<ul class="list-inline list-unstyled">
			<?php if ( is_acf( $prefix . 'instagram', 'option' ) ) : ?>
				<li class="list-inline-item">
					<a href="<?php the_acf( $prefix . 'instagram', 'option' ); ?>">
						<i class="fa fa-instagram"></i>
					</a>
				</li>
			<?php endif; ?>
			<?php if ( is_acf( $prefix . 'facebook', 'option' ) ) : ?>
				<li class="list-inline-item">
					<a href="<?php the_acf( $prefix . 'facebook', 'option' ); ?>">
						<i class="fa fa-facebook"></i>
					</a>
				</li>
			<?php endif; ?>
			<?php if ( is_acf( $prefix . 'twitter', 'option' ) ) : ?>
				<li class="list-inline-item">
					<a href="<?php the_acf( $prefix . 'twitter', 'option' ); ?>">
						<i class="fa fa-twitter"></i>
					</a>
				</li>
			<?php endif; ?>
			<?php if ( is_acf( $prefix . 'linkedin', 'option' ) ) : ?>
				<li class="list-inline-item">
					<a href="<?php the_acf( $prefix . 'linkedin', 'option' ); ?>">
						<i class="fa fa-linkedin"></i>
					</a>
				</li>
			<?php endif; ?>
		</ul>
	</div>
</aside>
